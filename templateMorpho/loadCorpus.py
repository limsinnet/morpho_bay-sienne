import json
from corpus import Alphabet
from random import random

def loadCorpus(PATH="/vol/work/loser/tiger/data/tiger_test.json",
               SAMPLE=1.):
    """
    Loads the corpus at PATH, and returns its lexicon
    The parameter SAMPLE is used to sample some ration of the full corpus. 1 takes the whole corpus, 0.5 half of it, etc...
    """
    corpus = json.load(open(PATH,"rt"))
    corpus = filter(lambda _: random()<=SAMPLE, corpus)
    return map(lambda w: '#'+w.lower()+'#', reduce(lambda x,y:x+y, map(lambda s: s[0],corpus)))

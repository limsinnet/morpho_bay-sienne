"""
Some tools to handle template-stem morphology
"""

import numpy as np

class Template:
    """
    A class representing a template, ie: a string of either characters or variable slots
    """
    def __init__(self, value=[0]):
        """
        Initialize a template. 'value' is a list where each element is either a single character, either the integer '0' to indicate a variable slot.
        Example: geXuXXen is represented by: ['g','e',0,'u',0,0,'e','n']
        """
        self.value = value

    def __repr__(self):
        return reduce(lambda x,y: x+y,
                      map(lambda z: '?' if z==0 else z, self.value))

    def __len__(self):
        return len(self.value)

    def __getitem__(self,i):
        return self.value[i]

    def compatible(self,form):
        """
        Checks whether the template is compatible with 'form' (ie: whether some appropriate assignment to its variable slots can yield the form 'form'
        """
        # Method: filling a boolean matrix C with dynamic programming, where C[i,j] determines whether template[:i] is compatible with form[:j]
        C = np.full((len(self.value)+1,len(form)+1), False, dtype='bool')
        C[1:0] |= (self.value[0]==0)
        C[0,0] = True
        for i in range(1,len(self.value)+1):
            for j in range(1,len(form)+1):
                C[i,j] = ((self.value[i-1]==0) and (C[i,j-1] or C[i-1,j] or C[i-1,j-1])) or ((self.value[i-1]==form[j-1]) and C[i-1,j-1])
        return C[len(self.value), len(form)]

    def exhaustive_parse(self,form):
        """
        Parses 'form' according to the template, and returns a list of ALL possible analyses.
        Example: if template 'T' is ge?u??en then the result of T.parse("getrunken") will be the nested list:
                 [['tr','nk',''],
                  ['tr','n','k'],
                  ['tr','','nk']]
        'None' will be returned if the template and the form are incompatible.
        """
        # Method: dynamic programming again. Fill a matrix P whose entries are lists of possible parses of form[:j] according to template[:i]
        P = map(lambda _: map(lambda _: [], range(len(form)+1)), range(len(self.value)+1))
        if self.value[0]==0:
            P[1][0] = [([''],1)]
        P[0][0] = [([],-1)]
        
        C = np.full((len(self.value)+1,len(form)+1), False, dtype='bool')
        C[1:0] |= (self.value[0]==0)
        C[0,0] = True

        def remove_duplicates(L):
            L1 = list()
            for i in range(len(L)):
                if L[i] in L[i+1:]:
                    pass
                else:
                    L1.append(L[i])
            return L1

        for i in range(1,len(self.value)+1):
            for j in range(1,len(form)+1):
                C[i,j] = ((self.value[i-1]==0) and (C[i,j-1] or C[i-1,j] or C[i-1,j-1])) or ((self.value[i-1]==form[j-1]) and C[i-1,j-1])
                if C[i,j]:
                    if self.value[i-1]==0:
                        for (analysis,lastClosure) in P[i-1][j-1]:
                            P[i][j].append((analysis+[form[j-1]], i))
                        for (analysis,lastClosure) in P[i][j-1]:
                            if lastClosure==i:
                                P[i][j].append((analysis[:-1]+[analysis[-1]+form[j-1]], i))
                        for (analysis,lastClosure) in P[i-1][j]:
                            P[i][j].append((analysis+[''], i))
                    else:
                        for (analysis,lastClosure) in P[i-1][j-1]:
                            P[i][j].append((analysis,lastClosure))
                P[i][j] = remove_duplicates(P[i][j])
        return map(lambda x: x[0], P[len(self.value)][len(form)])

    @staticmethod
    def parse(form,template):
        """
        Returns a parse (ie: list of slot-fillers) of 'form' according to 'template' among all possible parses.
        """
        if len(template)==0:
            return ([] if len(form)==0 else None)
        elif template[0]==0:
            if len(template)==1:
                return [form]
            else:
                i = 1
                j = 1
                while j<len(template) and template[j]!=0:
                    j += 1
                # template[i:j] contains the next template segment
                # Now try to match this segment with a substring of 'form'
                for k in range(len(form)-(j-i)+1):
                    if list(form[k:k+(j-i)])==template[i:j]:
                        next_parse = Template.parse(form[k+(j-i):], template[j:])
                        if next_parse==None:
                            continue
                        else:
                            return [form[:k]]+next_parse
                return None
        else:
            j = 0
            while j<len(template) and template[j]!=0:
                j += 1
            if list(form[:j])==template[:j]:
                next_parse = Template.parse(form[j:], template[j:])
                if next_parse==None:
                    return None
                else:
                    return next_parse
            else:
                return None
                            
                
            




                        
    @staticmethod
    def subsumes(T1,T2):
        """
        Checks whether a template T1 subsumes a template T2, ie: if the set of forms compatible with T2 is included in the set of forms compatible with T1
        """
        # Method: Dynamic programming
        S = np.full((len(T1)+1,len(T2)+1), False, dtype='bool')
        S[0,0] = True
        for i in range(1,len(T1)+1):
            if T1[i-1]==0:
                S[i,0] = True
            else:
                break
        for i in range(1,len(T1)+1):
            for j in range(1,len(T2)+1):
                S[i,j] = (
                    ( S[i-1,j-1] and (T1[i-1]==0 or (T1[i-1]==T2[j-1])) ) or
                    ( ( S[i,j-1] or S[i-1,j] ) and T1[i-1]==0 )
                    )
        return S[len(T1),len(T2)]

    @staticmethod
    def Max(Tlist):
        """
        Returns a maximal template in a list of templates. A maximal template is a template that is subsumed by all others. If None such template exists, returns None.
        """
        if len(Tlist)==0:
            return None
        elif len(Tlist)==1:
            return Tlist[0]
        else:
            res = Tlist[0]
            for i in range(1,len(Tlist)):
                if Template.subsumes(Tlist[i], res):
                    continue
                elif Template.subsumes(res,Tlist[i]):
                    res = Tlist[i]
                else:
                    return None
            return res

    @staticmethod
    def Min(Tlist):
        """
        Returns a minimal template in a list of templates. A maximal template is a template that is subsumed by all others. If None such template exists, returns None.
        """
        if len(Tlist)==0:
            return None
        elif len(Tlist)==1:
            return Tlist[0]
        else:
            res = Tlist[0]
            for i in range(1,len(Tlist)):
                if Template.subsumes(res,Tlist[i]):
                    continue
                elif Template.subsumes(Tlist[i],res):
                    res = Tlist[i]
                else:
                    return None
            return res

    @staticmethod
    def intersection(T1,T2):
        """
        Computes an intersection of templates T1 and T2.
        An intersection of two templates is a maximal subsumer of both templates, ie:
        a template who includes all forms that can be generated by both T1 and T2, and that is maximal (for the subsume relation) among templates which satisfy this property
        """
        X = map(lambda _: map(lambda _: [], range(1+len(T2))), range(1+len(T1)))
        for i in range(1,1+len(T1)):
            X[i][0] = [0]
        for j in range(1,1+len(T2)):
            X[0][j] = [0]

        for i in range(1,1+len(T1)):
            for j in range(1,1+len(T2)):
                
                t1 = X[i-1][j]
                if len(t1)==0 or t1[-1]!=0:
                    t1 = t1+[0]

                t2 = X[i][j-1]
                if len(t2)==0 or t2[-1]!=0:
                    t2 = t2+[0]

                t3 = X[i-1][j-1]
                if T1[i-1]==T2[j-1]:
                    t3 = t3 + [T1[i-1]]
                else:
                    if len(t3)==0 or t3[-1]!=0:
                        t3 = t3+[0]

                M = Template.Max([t1,t2,t3])
                if M==None:
                    # If no maximizing element is found in the proposal set, return a random element
                    # TODO: Improve this part in order to maintain not maximizing elements, but 'maximizing sets' in the dynamic programming matrix
                    M = [t1,t2,t3][np.random.randint(3)]
                X[i][j] = M
        return Template(value = X[len(T1)][len(T2)])


T1 = Template(value = ['g','e',0,'u',0,'e','n'])
T2 = Template(value = [0, 'g', 'e', 0, 'e', 'n'])
T3 = Template(value = [0, 'g', 'e', 0, 0])
T4 = Template(value = [0])



                      







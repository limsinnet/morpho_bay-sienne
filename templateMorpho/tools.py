# coding=utf-8

import random
import numpy as np

def randomU01():
    return random.random()

def randomInt(a,b):
    return random.randint(a,b)

def sample(iterator, P, normalized=True):
    """
    Samples an element from an iterator according to a probability distribution P
    
    Parameters
    ----------
    iterator : The iterator to sample from
    P : The probability function (takes an element from the iterator, and returns a real-valued number)
    normalized : A boolean indicating whether P is normalized on the iterator
    """
    if not normalized:
        Z = 0.
        for el in iterator:
            Z += P(el)
    else:
        Z = 1.
    x = np.random.random()
    y = 0.
    for el in iterator:
        y += P(el)/Z
        if y>=x:
            return el
    raise Exception("Options exhausted during sampling ! Check whether the distribution was normalized.")


def MHcheck(acceptance_logratio):
    """
    Performs a Metropolis-Hastings acceptance check given the difference between the log-probabilities of the source and target states.
    Returns True if the target state has passed the test, else False.
    """
    if acceptance_logratio>0:
        return True
    else:
        acceptance_ratio = np.exp(acceptance_logratio)
        return ( random.random()<=acceptance_ratio )
    

def MCMC(initial, transition, logQ, n_steps=100, n_particles=10,
         VERBOSE = True):
    """
    Performs Monte Carlo Markov Chain sampling.

    Parameters
    ----------
    initial : A function that takes no arguments and returns a random element of some domain X (the search space).
    transition : A function that takes an element of X and returns a random element of X.
    logQ : A function that takes an element of X and returns its unnormalized log-probability
    n_steps, n_particles : Self-explanatory
    """
    particles = map(lambda _: initial(), range(n_particles))
    particles_logQ = map(logQ, particles)
    for iteration in range(n_steps):
        if VERBOSE:
            print iteration, particles
        new_particles_before_MH = map(transition, particles)
        logQ_new_particles_before_MH = map(logQ, new_particles_before_MH)
        for i in range(n_particles):
            if MHcheck(logQ_new_particles_before_MH[i]-particles_logQ[i]):
                particles[i] = new_particles_before_MH[i]
                particles_logQ[i] = logQ_new_particles_before_MH[i]
    return particles[max(range(n_particles),
                         key = lambda i: particles_logQ[i])]


def metropolisSampling(proposal, logQ, steps=200):
    """
    Performs Metropolis sampling.
    
    Parameters
    ----------
    proposal : A function that takes no arguments and returns an element of some domain X.
    logQ : An unnormalized log-density function on the domain X.
    steps : Number of steps to run the algorithm for
    """
    state = proposal()
    state_logQ = logQ(state)
    for i in range(steps):
        newState = proposal()
        newState_logQ = logQ(newState)
        if MHcheck(newState_logQ-state_logQ):
            state = newState
            state_logQ = newState_logQ
    return state


def shuffle(L):
    """
    Shuffles a list. (Careful: overrides the list !)
    """
    for i in range(len(L)):
        k = np.random.randint(i,len(L))
        (L[i], L[k]) = (L[k], L[i])


def pr(text,val):
    print text+": "+str(val)
    return val



class SparseDict:
    """
    A class exposing the same interface as dictionaries, but allowing to assign a default value to previously unseen elements and accessing them as any other element
    """
    def __init__(self,
                 initial = dict(),
                 defaultValue = 0.,
                 erase_when_equal_to_default = True,
                 maintain_sum = False
                 ):
        """
        Parameters
        ----------
        initial : A dictionary to initialize the object
        defaultValue : The default value that will be assigned to keys that have not been previously set.
        erase_when_equal_to_default : A boolean determining whether if a key is assigned the default value, it will be removed from the internal dictionary.
        maintain_sum : Maintains a separate variable containing the sum of all elements (useful for efficient normalization for probability distributions). Works only if defaultValue is 0.
        """
        self._dict = initial
        self.defaultValue = defaultValue
        if maintain_sum:
            self.defaultValue = 0.
        self.erase_when_equal_to_default = erase_when_equal_to_default
        self.maintain_sum = maintain_sum
        if self.maintain_sum:
            self.Z = sum(map(lambda x: x[1], self._dict.items()))
        
    def __getitem__(self,key):
        if key in self._dict:
            return self._dict[key]
        else:
            return self.defaultValue

    def __setitem__(self,key,val):
        if val==self.defaultValue and self.erase_when_equal_to_default:
            if self.maintain_sum and key in self._dict:
                self.Z -= self._dict[key]
            if key in self._dict:
                self._dict.pop(key)
        else:
            if key in self._dict:
                self.Z -= self._dict[key]
            self.Z += val
            self._dict[key] = val

    def __repr__(self):
        return self._dict.__repr__()

    def __iter__(self):
        return self._dict.__iter__()

    def __contains__(self,key):
        return (key in self._dict)

    def Sum(self):
        """
        Returns the sum of elements
        """
        return self.Z

#!/usr/bin/env python
# coding=utf-8

import sys
import unicodedata

SENTINEL = u"#"
UNACCENTED_VOWEL = u"%"

def special_symbols():
    return SENTINEL, UNACCENTED_VOWEL

def is_unaccented_vowel(character):
    return (character in u"aeoiuεω")

def split(character):
    """Return a tuple (base character, accent).
    c is assumed to be a python composed (NFC) unicode character,
    (not a 'str' object)."""
    u = unicodedata.normalize('NFD', character)
    if type(u) == str:
        sys.exit("Bad input ('str' type instead of 'unicode')")
    if len(u) == 1:
        return (u, UNACCENTED_VOWEL if is_unaccented_vowel(u) else SENTINEL)
    elif len(u) == 2:
        return (u[0], u[1])
    else:
        sys.exit("bad encoding")


def merge(c1, c2):
    """Return a merged unicode character."""
    if c2==SENTINEL or c2==UNACCENTED_VOWEL:
        u = c1
    else:  # could check if it's a unicode character of the right class
        u = unicodedata.normalize('NFC', c1 + c2)
    return u


if __name__ == "__main__":
    ### TEST ###
    split(u"á")
    merge(u'a', u'\u0301')
    
    # I think we need to assume we get a 'composed' normalized unicode string
    mbochi_ortho_4 = u"""wa áyεε la midí
    wa áyεε la swέbhέ yá mwésé
    wa adi ó bangí
    ngá idzwé damara
    wa ámidzwa kώ"""
    
    def merge_test(t):
        return merge(*t)
        
    sp = map(split, mbochi_ortho_4)
    s = [it[0] for it in sp]
    t = [it[1] for it in sp]

    print mbochi_ortho_4.encode('utf8')
    print '\n'
    print s
    print t
    print ''.join(map(merge_test, sp)).encode('utf8')
    print ''.join(map(merge_test, sp)) == mbochi_ortho_4

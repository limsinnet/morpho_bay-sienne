import numpy as np
from tools import sample, slice_sample, slice_sample_step, beta, uniform, gamma, exponential, dirac, poisson
from pitmanYor import *

class ClassLanguageModel:
    """
    This class describes a language model with hidden classes
    """
    def __init__(self, N=2, n_classes=0, base_class=InfiniteGram, base_hyperparameters=dict(), class_pyp_hyperparameters=dict(), morph_pyp_hyperparameters=dict(), shared_pyp_hyperparameters=None, one_spelling_model_per_class=False):
        """
        base_class : Class of the string base distribution
        N : Markov order of the class sequence model

        Parses are represented by a couple (class_sequence, morph_sequence)
        """
        self.T = ( lambda pf: pf[-N+1:] ) if N>1 else ( lambda pf: tuple() )

        class ClassBase_Hyperparameters:
            def __init__(self,n_classes=0):
                self.n_classes = n_classes
            def __str__(self):
                return "number of classes: "+("unlimited" if self.n_classes==0 else str(self.n_classes)+"(fixed)")

        class ClassBase:
            def __init__(self, base=None, hyperparameters=None):
                self.used_ids = set([0,1])
                self.next_id = 2
                self.hyperparameters = hyperparameters
            def P(self,x):
                if self.hyperparameters.n_classes==0:
                    return 1. if x==self.next_id else 0.
                else:
                    return 1./self.hyperparameters.n_classes if ((x>=2) and (x<2+self.hyperparameters.n_classes)) else 0.
            def reset(self):
                self.used_ids = set([0,1])
                self.next_id = 2
            def cache(self,x):
                if self.hyperparameters.n_classes!=0:
                    return
                self.used_ids.add(x)
                self.next_id = 1+max(self.next_id, x)
            def uncache(self,x):
                if self.hyperparameters.n_classes!=0:
                    return
                self.used_ids.remove(x)
            def sample(self):
                if self.hyperparameters.n_classes==0:
                    return self.next_id
                else:
                    return np.random.randint(2,2+self.hyperparameters.n_classes)
            def id_iterator(self):
                if self.hyperparameters.n_classes==0:
                    for _id in self.used_ids:
                        if _id!=0 and _id!=1:
                            yield _id
                    yield self.next_id
                else:
                    for _id in range(2,2+self.hyperparameters.n_classes):
                        yield _id

            @staticmethod
            def hyperparameters_class():
                return ClassBase_Hyperparameters
            @staticmethod
            def resample_hyperparameters(initial):
                pass

        self.class_hpyp = CacheHierarchy(
            model_classes = lambda pf: ClassBase if pf==None else PYP,
            backoff = lambda pf: pf[1:] if (pf!=None and len(pf)>=1) else None,
            hyperparameters_hash = lambda pf: len(pf) if pf!=None else None,
            hyperparameters = lambda pf: class_pyp_hyperparameters if pf!=None else {'n_classes': n_classes}
            )

        if shared_pyp_hyperparameters==None:
            shared_pyp_hyperparameters = morph_pyp_hyperparameters

        if one_spelling_model_per_class:
            # Identifiers:
            # ------------
            # n (n>0) : CRP of the n-th class
            # -n (n>0) : Spelling model of the n-th class
            self.morph_hpyp = CacheHierarchy(
                model_classes = lambda cls: base_class if cls<0 else PYP,
                backoff = lambda cls: None if (cls<0) else (-cls),
                hyperparameters_hash = lambda cls: cls,
                hyperparameters = lambda cls: base_hyperparameters if cls<0 else morph_pyp_hyperparameters
                )
        else:
            # Identifiers :
            # -------------
            # n (n>=0) : CRP of a class
            # -1 : CRP that is shared among all classes (each class backs off to this CRP)
            # None : Common spelling model
            self.morph_hpyp = CacheHierarchy(
                model_classes = lambda cls: base_class if cls==None else PYP,
                backoff = lambda cls: None if (cls==None or cls==-1) else (-1), # (-1) indicates the shared morph model
                hyperparameters_hash = lambda cls: cls,
                hyperparameters = lambda cls: base_hyperparameters if cls==None else shared_pyp_hyperparameters if cls==-1 else morph_pyp_hyperparameters
            )
            
        self.N = N

        self.info = {"Markov order of class sequence model" : N,
                     "Base class on strings" : base_class,
                     "Base hyperparameters" : base_hyperparameters,
                     "Class PYP hyperparameters" : class_pyp_hyperparameters,
                     "Morph PYP hyperparameters" : morph_pyp_hyperparameters,
                     "One spelling model per class" : one_spelling_model_per_class}

    def __repr__(self):
        return repr(self.info)

    def reset(self):
        self.class_hpyp.reset()
        self.morph_hpyp.reset()
        
    def resample_hyperparameters(self):
        self.class_hpyp.resample_hyperparameters()
        self.morph_hpyp.resample_hyperparameters()

    def cache(self,parse): # Parses are represented by a couple (class_sequence, morph_sequence)
        pf = (0,)
        for i in range(len(parse[0])):
            cls = parse[0][i]
            self.class_hpyp(self.T(pf)).cache(cls)
            pf = pf+(cls,)
            self.morph_hpyp(cls).cache(parse[1][i])
        self.class_hpyp(self.T(pf)).cache(1)

    def uncache(self,parse):
        pf = (0,)
        for i in range(len(parse[0])):
            cls = parse[0][i]
            self.class_hpyp(self.T(pf)).uncache(cls)
            pf = pf+(cls,)
            self.morph_hpyp(cls).uncache(parse[1][i])
        self.class_hpyp(self.T(pf)).uncache(1)

    def P(self,parse):
        pf = (0,)
        log_p = 0.
        for i in range(len(parse[0])):
            cls,mph = parse[0][i],parse[1][i]
            px = self.class_hpyp(self.T(pf)).P(cls)
            pm = self.morph_hpyp(cls).P(mph)
            if px==0. or pm==0.:
                return 0.
            log_p += np.log(px)+np.log(pm)
            pf = pf+(cls,)
        log_p += np.log(self.class_hpyp(self.T(pf)).P(1))
        return np.exp(log_p)

    def sample(self):
        classes = (0,)
        morphs = tuple()
        cls = 0
        while len(classes)<1000:
            cls = self.class_hpyp(self.T(classes)).sample()
            classes = classes+(cls,)
            if cls!=1:
                mph = self.morph_hpyp(cls).sample()
                morphs = morphs+(mph,)
            else:
                break
        if len(classes)==1000:
            print "WARNING : The generated class sequence was longer than 1000 classes and therefore truncated"
        return (classes[1:-1], morphs)


    def beta(self,S):
        """
        Compute beta-probabilities on S
        """
        B = dict()
        L = len(S)
        Ts = lambda spl: spl[-self.N:]
        Tc = ( lambda cls: cls[-self.N+1:] ) if (self.N>1) else ( lambda cls: tuple() )
        def _beta(spl,cls): # N split points, (N-1) classes
            if (spl,cls) in B:
                return B[spl,cls]
            if spl[-1]==L:
                b = self.class_hpyp(cls).P(1)
                B[spl,cls] = b
                return b
            else:
                b = 0.
                for p in range(1+spl[-1],L+1):
                    for c in self.class_hpyp(None).id_iterator():
                        b += self.class_hpyp(cls).P(c)*self.morph_hpyp(c).P(S[spl[-1]:p])*_beta(
                            Ts(spl+(p,)) , Tc(cls+(c,))
                            )
                B[spl,cls] = b
                return b
        return _beta
                            
                        
    def segment(self,S,sampled=True,for_eval=False,max_segment_length=None,compute_likelihood=False):
        """
        Samples a segmentation for string 'S'
        """
        splits = (0,)
        classes = (0,)
        Ts = lambda spl: spl[-self.N:]
        Tc = ( lambda cls: cls[-self.N+1:] ) if (self.N>1) else ( lambda cls: tuple() )
        B = self.beta(S)
        max_len = len(S) if max_segment_length==None else max_segment_length

        while splits[-1]!=len(S):
            p = dict()
            Z = 0.
            for pos in range(splits[-1]+1, min(splits[-1]+1+max_len,len(S)+1)):
                for c in self.class_hpyp(None).id_iterator():
                    p[pos,c] = self.class_hpyp(Tc(classes)).P(c)*self.morph_hpyp(c).P(S[splits[-1]:pos])*B(Ts(splits+(pos,)), Tc(classes+(c,)))
                    Z += p[pos,c]
            if Z!=0:
                pos,c = sample(p, lambda k: p[k]/Z, normalized=True) if sampled else max(p, key=lambda k:p[k])
            else:
                print "WARNING : a zero probability was encountered during the sampling of a parse. By default, a random segmentation is returned"
                for p_n in range(splits[-1]+1, len(S)+1):
                    pos = p_n
                    if np.random.random()<0.3:
                        break
                class_ids = list(self.class_hpyp(None).id_iterator())
                c = sample(class_ids, lambda k: 1./len(class_ids), normalized=True)
            splits = splits+(pos,)
            classes = classes+(c,)

        segments = tuple()
        i = 0
        for j in splits[1:]:
            segments = segments+(S[i:j],)
            i = j
        return segments if for_eval else (
            ((classes[1:],segments),np.log(self.P((classes[1:],segments)))) if compute_likelihood else (classes[1:],segments)
            )

    def marginal_logP(self,S,max_segment_length=None):
        ana = self.segment(S, sampled=True, max_segment_length=max_segment_length)
        return np.log(self.P(ana))
            
"""
lm = ClassLanguageModel(N=3)
for i in range(10):
    lm.cache(((2,3,4),('un','happi','ly')))
    lm.cache(((3,4),('happi','ness')))
    lm.cache(((3,),('sad',)))
    lm.cache(((2,3,4),('un','sad','ness')))
"""

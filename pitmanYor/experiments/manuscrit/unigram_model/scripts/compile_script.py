import prepare_xp

# Experiences avec modele unigramme de morphemes et modele trigramme simple comme spelling model

prepare_xp.save_parameter = False
prepare_xp.set_xp_name("en_unigram")
prepare_xp.set_command("/vol/work/loser/pitmanYor/segmenter.py")
prepare_xp.add_iter_parameter("-T",[0])
prepare_xp.add_iter_parameter("--corpus_annealing",[0.,0.25,0.5,0.75,1.])
prepare_xp.add_iter_parameter("-N",[1])
prepare_xp.add_iter_parameter("-n",[3])
prepare_xp.add_iter_parameter("-e",[10000])
prepare_xp.add_enum_parameter(["--stop iteration1673770"])
prepare_xp.add_enum_parameter(["--simple_ngram_model"])
prepare_xp.add_iter_parameter("--morph_theta_dirac",[1.,10.,100.,500.,1000.])
prepare_xp.add_iter_parameter("--morph_discount_dirac",[0.1])
prepare_xp.preview()
prepare_xp.save("run_xp.sh")

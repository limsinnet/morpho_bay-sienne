import os

command = None
xp_name = "xp"


def info():
    print "Commands:\n---------\ninfo()\nset_xp_name(name)\nset_command(name)\nadd_iter_parameter(name,iterator)\nadd_option_parameter(name)\nadd_enum_parameter(iterator)\nremove([parameter_index])\nfiltr(lambda) where 'lambda' is a function mapping a dict of parameter assignments to a boolean\nreset_filtr()\npreview()\nsave([path])"

info()

def set_xp_name(name):
    global xp_name
    xp_name = name

def set_command(name):
    global command
    command = name

name_iterators = []
arg_iterators = []
dict_iterators = []
_fltr = lambda v: True

def filtr(_lambda):
    global _fltr
    _fltr_old = _fltr
    _fltr = lambda v: (_fltr_old(v) and _lambda(v))

def reset_filtr():
    global _fltr
    _fltr = lambda v: True

def remove(index=-1):
    global name_iterators, arg_iterators
    if index==-1:
        name_iterators = name_iterators[:-1]
        arg_iterators = arg_iterators[:-1]
        dict_iterators = dict_iterators[:-1]
    else:
        name_iterators = name_iterators[:index]+name_iterators[(1+index):]
        arg_iterators = arg_iterators[:index]+arg_iterators[(1+index):]
        dict_iterators = dict_iterators[:index]+dict_iterators[(1+index):]

def rm_spaces(s):
    return s.replace(" ","").replace(",","")

def hyphen(s):
    if len(s)==0:
        return ""
    else:
        if s[0]=="-":
            return s
        else:
            return "-"+s if len(s)==1 else "--"+s

def add_iter_parameter(name,iterator):
    l2 = []
    l1 = []
    l3 = []
    for x in iterator:
        l1.append(name+rm_spaces(str(x)))
        l2.append(hyphen(name)+" "+str(x))
        l3.append({name: x})
    name_iterators.append(l1)
    arg_iterators.append(l2)
    dict_iterators.append(l3)

def add_option_parameter(name):
    name_iterators.append(["NO"+name,name])
    arg_iterators.append(["",hyphen(name)])
    dict_iterators.append([{name:False},{name:True}])

def add_enum_parameter(iterator):
    name_iterators.append(list(iterator))
    arg_iterators.append(map(hyphen,iterator))
    l3 = []
    iter_list = list(iterator)
    for x in iter_list:
        if x=="":
            l3.append(dict(map(lambda y: (y,False), iter_list)))
        else:
            l3.append(dict(map(lambda y: (y,y==x), iter_list)))
    dict_iterators.append(l3)

def compile():
    if command==None:
        print "Error: Command not specified"
        return None
    # Build commands
    l = [command]
    for i in range(len(arg_iterators)):
        l_next = list()
        for x in l:
            for y in arg_iterators[i]:
                l_next.append(x+" "+y)
        l = l_next
    command_list = l

    # Build xp names
    l = [[xp_name]]
    for i in range(len(name_iterators)):
        l_next = list()
        for x in l:
            for y in name_iterators[i]:
                l_next.append(x+[y])
        l = l_next
    l = map(lambda xp: filter(lambda entry: len(entry)>0, xp), l)
    name_list = map(lambda xp: reduce(lambda a,b: a+"_"+b, xp), l)
    
    # Build var assignment dictionaries
    l = [dict()]
    for i in range(len(dict_iterators)):
        l_next = list()
        for x in l:
            for y in dict_iterators[i]:
                l_next.append(dict(x.items()+y.items()))
        l = l_next
    dict_list = l

    # Build script
    res = "#!/bin/bash\n"
    for i in range(len(command_list)):
        # Test filter
        if _fltr(dict_list[i]):
            res += "/people/loser/clusters/cluster_rock2.sh "+name_list[i]+" /people/allauzen/venv/kevin/bin/python "+command_list[i]+"\n"
    return res


def preview():
    s = compile()
    if s==None:
        return
    else:
        print s

def save(path="run.sh"):
    f = open(path,"w")
    s = compile()
    if s==None:
        return
    f.write(s)
    f.close()
    os.system("chmod +x "+path)

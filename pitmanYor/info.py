from pitmanYor import *
from pitmanYor2 import *

def head_of_distribution(pyp,ratio=0.8):
    """
    Returns all events whose cumulative probability occupies the top 'ratio' of the probability mass
    """
    event_probabilities = dict()
    Z = 0.
    for ev in pyp.label_tables:
        event_probabilities[ev] = 0.
        tables = pyp.label_tables[ev]
        for t in tables:
            n = pyp.customers_per_table[t]
            Z += n
            event_probabilities[ev] += n
    sorted_events = sorted(event_probabilities.items(), key = lambda x: x[1], reverse=True)
    S = 0.
    end_at = 0
    for i in range(len(sorted_events)):
        if S>=(Z*ratio):
            end_at = i
            break
        else:
            S += sorted_events[i][1]
    return sorted_events[:end_at]


def class_model_info(classModel,ratio=0.8):
    s = "Transitions (top "+str(100.*ratio)+"%)"
    print s
    print "-"*len(s)
    for c in classModel.class_hpyp.models:
        if not hasattr(classModel.class_hpyp.models[c],"label_tables"):
            continue
        h = map(lambda x: x[0], head_of_distribution(classModel.class_hpyp.models[c],ratio=ratio))
        print str(c)+" -> "+(reduce(lambda a,b : a+","+b,map(str,h)) if len(h)>0 else "/")
    print " "
    s = "Emissions (top "+str(100.*ratio)+"%)"
    print s
    print "-"*len(s)
    for c in classModel.morph_hpyp.models:
        if not hasattr(classModel.morph_hpyp.models[c],"label_tables"):
            continue
        h = head_of_distribution(classModel.morph_hpyp.models[c],ratio=ratio)
        L = sum(map(lambda x: len(x[0])*x[1], h))
        LS = sum(map(lambda x: x[1],h))
        avg_len = (L/LS) if (LS>0.) else None
        h = map(lambda x: x[0], h)
        try:
            print str(c)+u" -> (theta="+str(classModel.morph_hpyp.models[c].hyperparameters.theta)+u", discount="+str(classModel.morph_hpyp.models[c].hyperparameters.discount)+u", average_length="+str(avg_len)+u", "+str(len(h))+u" items) "+(reduce(lambda a,b : a+u", "+b,h) if len(h)>0 else "/")
        except:
            print "Probably a unicode error :("
            pass
        



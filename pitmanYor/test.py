"""
An example: infinite N-gram model
"""

from pitmanYor import *
import numpy as np

charset = list('azertyuiopqsdfghjklmwxcvbn')


hpyp = CacheHierarchy(model_classes = lambda pf: ( PDF(_P = lambda _ : 1./len(charset),
                                                       _sample = lambda : charset[np.random.randint(0,len(charset))])
                                                   ) if pf==None else PYP,
                      backoff = lambda pf: pf[1:] if (pf!=None and len(pf)>=1) else None,
                      hyperparameters_hash = lambda pf: len(pf) if pf!=None else None,
                      hyperparameters = lambda pf: dict() if pf==None else {'lock_discount' : True, 'theta' : 10., 'discount' : 0.001 } )

text = tuple("ceci est un exemple permettant de tester un modele infinite gram de mots chaque mot de ce texte sera introduit dans le cache du modele infinite gram".split(' '))
for k in range(len(text)):
    hpyp(text[:k]).cache(text[k])
    

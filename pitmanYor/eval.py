"""
A module to evaluate morphological models
"""

from random import random

def lexicon_augmentation(sample_procedure,
                         train_corpus,
                         test_corpus=[],
                         PRINT_EVERY=100):
    """
    Repeatedly calls function 'sample_procedure' to generate word forms.
    Generates a set of word forms, pruning forms that were either already generated or present in the train_corpus, and computes precision/recall against word forms in the test_corpus minus train_corpus
    """
    generated = set()
    generated_outside_train = set()
    train = set(train_corpus)
    test = set(test_corpus)
    test = test.difference(train)
    it = 1
    while True:
        form = sample_procedure()
        if (form not in generated):
            generated.add(form)
            if (form not in train):
                generated_outside_train.add(form)
        it += 1
        if it%PRINT_EVERY==0:
            print str(it)+" samples"
            trainIg = len(train.intersection(generated))
            testIgot = len(test.intersection(generated_outside_train))
            totalIg = len((train.union(test)).intersection(generated))
            print "Precision over train corpus: "+str(trainIg)+"/"+str(len(generated))+" = "+str(float(trainIg)/len(generated))
            print "Recall over train corpus:"+str(trainIg)+"/"+str(len(train))+" = "+str(float(trainIg)/len(train))
            print "Precision over test corpus: "+str(testIgot)+"/"+str(len(generated_outside_train))+" = "+str(float(testIgot)/len(generated_outside_train))
            print "Recall over test corpus:"+str(testIgot)+"/"+str(len(test))+" = "+str(float(testIgot)/len(test))
            print "Total precision: "+str(totalIg)+"/"+str(len(generated))+" = "+str(float(totalIg)/(len(generated)))
            print "Total recall: "+str(totalIg)+"/"+str(len(train)+len(test))+" = "+str(float(totalIg)/(len(train)+len(test)))
            print " "


def compare_segmentations(segmentation, gold):
    """
    Computes a proposed segmentation 'segmentation' against a gold standard 'gold'
    Returns a triple: hits, insertions, deletions
    """
    seg_boundaries = set()
    gold_boundaries = set()
    i = 0
    for morph in segmentation[0:len(segmentation)-1]:
        i += len(morph)
        seg_boundaries.add(i)
    i = 0
    for morph in gold[0:len(gold)-1]:
        i += len(morph)
        gold_boundaries.add(i)
    H = len(seg_boundaries.intersection(gold_boundaries))
    I = len(seg_boundaries.difference(gold_boundaries))
    D = len(gold_boundaries.difference(seg_boundaries))
    return H, I, D
        

def against_gold_segmentations(segmentation_procedure,
                               test_corpus,
                               VERBOSE = False,
                               print_seg = False):
    """
    Segments the words in test_corpus and compares the obtained segmentations to the gold segmentations

    Parameters
    ----------
    segmentation_procedure : A function mapping a string to a string list
    test_corpus : A list of [unsegmented_string, [segmentation1, segmentation2, ...]] structures, where each segmentation_ is itself a list of morphemes
                  example: ["abgebrochen", [["ab","ge","broch","en"],["ab","ge","brochen"]]]
    
    Returns: a triple (precision, recall, F-measure)
    """
    if VERBOSE:
        print "EVALUATION"
    H = 0
    I = 0
    D = 0
    it = 0
    for item in test_corpus:
        unseg = item[0]
        seg_options = item[1]
        proposal = segmentation_procedure(unseg)
        if VERBOSE:
            print reduce(lambda a,b: a+' '+b, proposal)[:-1]
        results = map(lambda option: compare_segmentations(proposal,option),
                      seg_options)
        best = max(range(len(results)), key=(lambda i: results[i][0]))
        _h,_i,_d = results[best]
        model_seg = reduce(lambda a,b:a+" "+b, proposal)
        if VERBOSE:
            print "Model: "+model_seg+" / Ref: "+reduce(lambda a,b:a+" "+b, seg_options[best])+" / "+str(_h)+" hits "+str(_i)+" inserts "+str(_d)+" deletions"
        if print_seg:
            print model_seg
        H += _h
        I += _i
        D += _d
        it += 1
        
    return 100*float(H)/(H+I), 100*float(H)/(H+D), 100*2.*float(H)/(2*H+I+D)




class random_baseline:
    """
    A trivial segmentation procedure that randomly splits words.
    At each character boundary, a split randomly introduced according to the frequency of splits in the corpus
    """

    def __init__(self,corpus):
        n_chars = 0
        n_splits = 0
        for item in corpus:
            for seg in item[1]:
                n_splits += (len(seg)-2)
                n_chars += len(reduce(lambda a,b:a+b, seg))
        self.ratio = float(n_splits)/n_chars
        
    def __call__(self,word_):
        word = word_[1:-1]
        seg = ['#']
        j = 0
        for i in range(1,len(word)):
            if random()<self.ratio:
                seg += [word[j:i]]
                j = i
        seg += [word[j:]]
        seg += ['#']
        return seg
        
        
                











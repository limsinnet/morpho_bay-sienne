import json
from corpus import Alphabet
from random import random
from tools import sample, cumulative_sample
import numpy as np
import codecs

def wrap_trainSet(trainset,keep_order=False,annealing=0.):
    class TrainSet:
        def __init__(self,trainset,keep_order=False):
            self.keep_order = keep_order
            Z = 0.
            items = list()
            freqs = list()
            for item in trainset:
                freq, word = int(item[0]), item[1]
                items.append(word)
                freqs.append(Z)
                Z += np.power(freq,annealing)
            self.Z = Z
            self.items = items
            self.freqs = freqs

        def __len__(self):
            return int(self.Z)

        def token_length(self):
            return int(self.Z)

        def type_length(self):
            return len(self.items)

        def __call__(self):
            if keep_order:
                while True:
                    n = 0
                    for i in range(len(self.items)):
                        for j in range(int(
                                (self.Z if i==(len(self.items)-1) else self.freqs[i+1]) - self.freqs[i]
                                )):
                            yield self.items[i],n
                            n += 1
            else:
                while True:
                    n = cumulative_sample(list(range(len(self.items))), lambda i: float(self.freqs[i])/self.Z)
                    yield self.items[n], (n, np.random.randint(0,
                                                               ( self.Z if n==(len(self.items)-1) else self.freqs[n+1] )-self.freqs[n]
                                                           ))
    return TrainSet(trainset,keep_order=keep_order)
                


def loadCorpus_MBO_train(PATH="/vol/work/loser/data/bulb/mbochi.txt"):
    """
    Mbochi data without the segmentations
    """
    f = codecs.open(PATH,"r",encoding="utf-8")
    lines = f.read().splitlines()
    f.close()
    dataset = map(lambda line: line.replace(' ',''), lines)
    return dataset

def loadCorpus_MBO_test(PATH="/vol/work/loser/data/bulb/mbochi.txt"):
    """
    Mbochi data with segmentations
    """
    f = codecs.open(PATH,"r",encoding="utf-8")
    lines = f.read().splitlines()
    f.close()
    unsplit = map(lambda line: line.replace(' ',''), lines)
    split = map(lambda line: [filter(lambda morph: len(morph)>0, line.split(' '))], lines)
    return zip(unsplit, split)
                         

def loadCorpus_FIN_test(PATH="/vol/work/loser/data/morphoChallenge/goldstdsample.fin",
                         SAMPLE=1.):
    """
    Finnish MorphoChallenge 2005 test set
    """
    f = codecs.open(PATH,"r",encoding="utf-8")
    lines = f.read().splitlines()
    f.close()
    lines_split_between_unsegmented_and_segmented = map(lambda x: x.split('\t'),
                                                        lines)
    lines_with_split_options = map(lambda x: [x[0], x[1].split(', ')],
                                   lines_split_between_unsegmented_and_segmented)
    lines_with_split_segmentation = map(lambda x: [x[0],
                                                   map(lambda opt: opt.split(' '),
                                                       x[1])],
                                        lines_with_split_options)
    sampled_dataset = filter(lambda _: random()<SAMPLE,
                             lines_with_split_segmentation)
    return sampled_dataset

def loadCorpus_FIN_train(PATH="/vol/work/loser/data/morphoChallenge/wordlist.fin",annealing=1.,
                         SAMPLE=1.):
    """
    Turkish MorphoChallenge 2005 train set
    """
    f = codecs.open(PATH,"r",encoding="utf-8")
    lines = f.read().splitlines()
    f.close()
    dataset = map(lambda line: line.split(' '),
                  lines)
    sampled_dataset = filter(lambda _: random()<SAMPLE, dataset)
    return wrap_trainSet(sampled_dataset,annealing=annealing)

def loadCorpus_TUR_test(PATH="/vol/work/loser/data/morphoChallenge/goldstdsample2005.tur",
                         SAMPLE=1.):
    """
    Turkish MorphoChallenge 2005 test set
    """
    f = codecs.open(PATH,"r",encoding="utf-8")
    lines = f.read().splitlines()
    f.close()
    lines_split_between_unsegmented_and_segmented = map(lambda x: x.split('\t'),
                                                        lines)
    lines_with_split_options = map(lambda x: [x[0], x[1].split(', ')],
                                   lines_split_between_unsegmented_and_segmented)
    lines_with_split_segmentation = map(lambda x: [x[0],
                                                   map(lambda opt: opt.split(' '),
                                                       x[1])],
                                        lines_with_split_options)
    sampled_dataset = filter(lambda _: random()<SAMPLE,
                             lines_with_split_segmentation)
    return sampled_dataset

def loadCorpus_TUR_train(PATH="/vol/work/loser/data/morphoChallenge/wordlist2005.tur",annealing=1.,
                         SAMPLE=1.):
    """
    Turkish MorphoChallenge 2005 train set
    """
    f = codecs.open(PATH,"r",encoding="utf-8")
    lines = f.read().splitlines()
    f.close()
    dataset = map(lambda line: line.split(' '),
                  lines)
    sampled_dataset = filter(lambda _: random()<SAMPLE, dataset)
    return wrap_trainSet(sampled_dataset,annealing=annealing)

def loadCorpus_EN_test(PATH="/vol/work/loser/data/morphoChallenge/goldstdsample2005.eng",
                         SAMPLE=1.):
    """
    English MorphoChallenge 2005 test set
    """
    f = codecs.open(PATH,"r",encoding="utf-8")
    lines = f.read().splitlines()
    f.close()
    lines_split_between_unsegmented_and_segmented = map(lambda x: x.split('\t'),
                                                        lines)
    lines_with_split_options = map(lambda x: [x[0], x[1].split(', ')],
                                   lines_split_between_unsegmented_and_segmented)
    lines_with_split_segmentation = map(lambda x: [x[0],
                                                   map(lambda opt: opt.split(' '),
                                                       x[1])],
                                        lines_with_split_options)
    sampled_dataset = filter(lambda _: random()<SAMPLE,
                             lines_with_split_segmentation)
    return sampled_dataset


def loadCorpus_EN_train(PATH="/vol/work/loser/data/morphoChallenge/wordlist2005.eng",annealing=1.,
                         SAMPLE=1.):
    """
    Turkish MorphoChallenge 2005 train set
    """
    f = codecs.open(PATH,"r",encoding="utf-8")
    lines = f.read().splitlines()
    f.close()
    dataset = map(lambda line: line.split(' '),
                  lines)
    sampled_dataset = filter(lambda _: random()<SAMPLE, dataset)
    return wrap_trainSet(sampled_dataset,annealing=annealing)



def loadCorpus_DE_small(PATH="/vol/work/loser/tiger/data/tiger_test.json",
               SAMPLE=1.):
    """
    Loads the corpus at PATH, and returns its lexicon
    The parameter SAMPLE is used to sample some ration of the full corpus. 1 takes the whole corpus, 0.5 half of it, etc...
    """
    corpus = json.load(open(PATH,"rt"))
    corpus = filter(lambda _: random()<=SAMPLE, corpus)
    corpus = map(lambda w: w.lower(), reduce(lambda x,y:x+y, map(lambda s: s[0],corpus)))
    corpus = filter(lambda x: x.isalpha(), corpus)
    corpus = list(set(corpus))
    lexicon = map(lambda x: x, corpus)
    lexicon = sorted(lexicon,
                     key = lambda w: len(w))
    return lexicon


def loadCorpus_DE_large(PATH="/vol/work/loser/tiger/data/tiger_train.json",
               SAMPLE=1.):
    """
    Loads the corpus at PATH, and returns its lexicon
    The parameter SAMPLE is used to sample some ration of the full corpus. 1 takes the whole corpus, 0.5 half of it, etc...
    """
    corpus = json.load(open(PATH,"rt"))
    corpus = filter(lambda _: random()<=SAMPLE, corpus)
    corpus = map(lambda w: w.lower(), reduce(lambda x,y:x+y, map(lambda s: s[0],corpus)))
    corpus = filter(lambda x: x.isalpha(), corpus)
    corpus = list(set(corpus))
    lexicon = map(lambda x: x, corpus)
    lexicon = sorted(lexicon,
                     key = lambda w: len(w))
    return lexicon


def loadCorpus_DE_sentences_small(PATH="/vol/work/loser/tiger/data/tiger_test.json",
               SAMPLE=1.):
    """
    Loads the corpus at PATH, and returns its lexicon
    The parameter SAMPLE is used to sample some ration of the full corpus. 1 takes the whole corpus, 0.5 half of it, etc...
    """
    corpus = json.load(open(PATH,"rt"))
    corpus = filter(lambda _: random()<=SAMPLE, corpus)
    corpus = map(lambda sentence: sentence[0], corpus) # Leave out POS information
    corpus = filter(lambda sentence: len(sentence)>7, corpus) # Only keep sentences with at least 7 words
    corpus = map(lambda sentence: map(lambda word: word.lower(), sentence), corpus) # Convert to lower-case
    corpus = map(lambda sentence: reduce(lambda x,y: x+y, sentence), corpus) # Forget word boundaries
    corpus = sorted(corpus,
                    key = lambda sentence: len(sentence)) # Sort in increasing sentence length
    corpus = filter(lambda sentence: '#' not in sentence, corpus) # Remove sentences with padding symbols
    corpus = map(lambda sentence: '#'+sentence+'#', corpus) # Add padding symbols
    return corpus


def loadCorpus_DE_sentences_large(PATH="/vol/work/loser/tiger/data/tiger_train.json",
               SAMPLE=1.):
    """
    Loads the corpus at PATH, and returns its lexicon
    The parameter SAMPLE is used to sample some ration of the full corpus. 1 takes the whole corpus, 0.5 half of it, etc...
    """
    corpus = json.load(open(PATH,"rt"))
    corpus = filter(lambda _: random()<=SAMPLE, corpus)
    corpus = map(lambda sentence: sentence[0], corpus) # Leave out POS information
    corpus = filter(lambda sentence: len(sentence)>7, corpus) # Only keep sentences with at least 7 words
    corpus = map(lambda sentence: map(lambda word: word.lower(), sentence), corpus) # Convert to lower-case
    corpus = map(lambda sentence: reduce(lambda x,y: x+y, sentence), corpus) # Forget word boundaries
    corpus = sorted(corpus,
                    key = lambda sentence: len(sentence)) # Sort in increasing sentence length
    corpus = filter(lambda sentence: '#' not in sentence, corpus) # Remove sentences with padding symbols
    corpus = map(lambda sentence: '#'+sentence+'#', corpus) # Add padding symbols
    return corpus

import numpy as np
from tools import sample, slice_sample, slice_sample_step, beta, uniform, gamma, exponential, dirac, poisson
from pitmanYor import *
from split_merge import split, merge, special_symbols

SENTINEL, UNACCENTED_VOWEL = special_symbols()
                   

class ToneCharacterModel_Hyperparameters:
    def __init__(self, share_across_lengths = True, maximum_markov_order = None, theta_prior=exponential(10.), discount_prior = beta(1.,5.), length_constraint_gamma_prior=(30.,3.), no_chars=False, only_remember_last_tone_seen=False):
        """
        theta_prior, discount_prior : Hyperparameter priors on the PYPs modeling next-character distributions
        share_across_lengths : Use the same hyperparameter set for all prefix lengths
        maximum_markov_order : Stop PYP hierarchy after prefixes of length 'maximum_markov_order' (if 'None', yields an infinite N-gram model
        length_constraint_gamma_prior : (A tuple with two elements) Parameters on the gamma distribution that acts as a prior for the length constraint of strings (if None: no length constraint)
        no_chars : Keep only tones
        """
        self.share_across_lengths = share_across_lengths
        self.only_remember_last_tone_seen = only_remember_last_tone_seen
        self.no_chars = no_chars
        self.length_constraint = (length_constraint_gamma_prior!=None)
        self.length_constraint_gamma_prior = length_constraint_gamma_prior
        if self.length_constraint:
            self.length = gamma(length_constraint_gamma_prior[0], 1./length_constraint_gamma_prior[1]).sample()
        self.maximum_markov_order = maximum_markov_order
        if share_across_lengths:
            self.hp = {0 : PYP_Hyperparameters(theta_prior=theta_prior, discount_prior=discount_prior), None : PDF_Hyperparameters(P = lambda _ : 0., sample = lambda : '?')}
        else:
            self.hp = { None : PDF_Hyperparameters(P = lambda _ : 0., sample = lambda : '?')}
        if self.length_constraint:
            self.gamma_a, self.gamma_b = self.length_constraint_gamma_prior

    """
    TO REMOVE
    def hyperparameters_by_length(l):
        if self.share_across_lengths:
            return self.hp[0]
        else:
            if l in self.hp:
                return self.hp[l]
            else:
                self.hp[l] = PYP_Hyperparameters(theta=theta, discount=discount)
                return self.hp[l]
    """

    def reset(self):
        for hp_set in self.hp.values():
            hp_set.reset()

    def resample_length(self):
        self.length = gamma(self.gamma_a, 1./self.gamma_b).sample()
        print "length hyperparameter = "+str(self.length)

    def cache_length(self,l):
        """
        Caches a length 'l' for resampling the length parameter
        """
        self.gamma_a += l
        self.gamma_b += 1

    def uncache_length(self,l):
        """
        Uncaches a length 'l' for resampling the length parameter
        """
        self.gamma_a -= l
        self.gamma_b += 1








class ToneCharacterModel:
    """
    A class describing a character model with tones
    """
    def __init__(self,base,hyperparameters):
        """
        base : irrelevant
        hyperparameters : a InfiniteGram_Hyperparameters object
        """
        self.initial = lambda : 0
        self.final = lambda : 1
        self.hyperparameters = hyperparameters

        def backoff_scheme(ID):
            if not self.hyperparameters.only_remember_last_tone_seen:
                if ID!=None:
                    chars, tones = ID
                    if len(chars)>0:
                        if chars[0]==self.initial:
                            return (chars[1:],tones)
                        else:
                            char,tone = split(chars[0])
                            return (chars[1:], 
                                    tones+(tone,))
                    elif len(tones)>0:
                        return (tuple(),tones[1:])
                    else:
                        return None
                else: # Irrelevant
                    return None
            else:
                if ID!=None:
                    chars,tones = ID
                    if len(chars)==0:
                        if len(tones)==1:
                            return (chars, tuple())
                        else:
                            return None
                    elif chars[0]==self.initial:
                        return (chars[1:], tones)
                    else:
                        char0,tone0 = split(chars[0])
                        """
                        if tone0==SENTINEL:
                            return (chars[1:], tones)
                        else:
                            if len(tones)==0:
                                return (chars[1:], (tone0,))
                            else:
                                return (chars, tuple())
                        """
                        return (chars[1:], tone0)
                else:
                    return None

        def backoff_scheme_nochars(ID):
            if ID!=None:
                if len(ID)>0:
                    return ID[1:]
                else:
                    return None
            else:
                return None

        self.hpyp = CacheHierarchy(model_classes = lambda pf: PDF if pf==None else PYP,
                                   backoff = backoff_scheme_nochars if self.hyperparameters.no_chars else backoff_scheme,
                                   hyperparameters_hash = (lambda ID: None if ID==None else len(ID)) if self.hyperparameters.no_chars else (lambda ID: (len(ID[0]),ID[1]!=None) if ID!=None else None),
                                   hyperparameters = lambda ID: dict() if ID!=None else {'P' : (lambda _ : 0.),  'sample' : (lambda : '?')}
                                   )
        self.hpyp.hyperparameters_table = self.hyperparameters.hp
        self.T = ( lambda pf : pf ) if self.hyperparameters.maximum_markov_order==None else (lambda pf : pf[-(-1+self.hyperparameters.maximum_markov_order):])

    def reset(self):
        self.hyperparameters.reset()
        self.hpyp.reset()

    @staticmethod
    def hyperparameters_class():
        return ToneCharacterModel_Hyperparameters

    @staticmethod
    def resample_hyperparameters(initial,
                                 VERBOSE=True):
        for H in initial.hp:
            if H!=None:
                PYP.resample_hyperparameters(initial.hp[H], VERBOSE=VERBOSE)
        if initial.length_constraint:
                initial.resample_length()
        
    def cache(self,seq,dont_cache_length=False):
        """ Caches a sequence 'seq' """
        pf = (self.initial,)
        for x in seq:
            self.hpyp(self.T(pf) if self.hyperparameters.no_chars else (self.T(pf),tuple())).cache(x)
            pf = pf+((split(x)[1],) if self.hyperparameters.no_chars else (x,))
        if self.hyperparameters.length_constraint and (not dont_cache_length):
            self.hyperparameters.cache_length(len(seq))
        else:
            self.hpyp((self.T(pf),tuple())).cache(self.final)

    def uncache(self,seq):
        """ Uncaches a sequence 'seq' """
        pf = (self.initial,)
        for x in seq:
            self.hpyp(self.T(pf) if self.hyperparameters.no_chars else (self.T(pf),tuple())).uncache(x)
            pf = pf+((split(x)[1],) if self.hyperparameters.no_chars else (x,))
        if self.hyperparameters.length_constraint:
            self.hyperparameters.uncache_length(len(seq))
        else:
            self.hpyp((self.T(pf),tuple())).uncache(self.final)

    def P(self,seq):
        pf = (self.initial,)
        log_p = 0.
        for x in seq:
            px = self.hpyp(self.T(pf) if self.hyperparameters.no_chars else (self.T(pf),tuple())).P(x)
            if px==0.:
                return 0.
            log_p += np.log(px)
            pf = pf+((split(x)[1],) if self.hyperparameters.no_chars else (x,))
        if self.hyperparameters.length_constraint:
            log_p += (-self.hyperparameters.length + (len(pf)-1)*np.log(self.hyperparameters.length) - sum(map(np.log, range(2,len(pf)))))
        else:
            log_p += np.log(self.hpyp((self.T(pf),tuple())).P(self.final))
        return np.exp(log_p)

    def sample(self): # TODO
        seq = (self.initial,)
        x = 0
        if self.hyperparameters.length_constraint:
            l = poisson(self.hyperparameters.length).sample()
        while (self.hyperparameters.length_constraint and (len(seq)<(l+1))) or ((not self.hyperparameters.length_constraint) and x!=self.final):
            x = self.hpyp((self.T(seq),tuple())).sample()
            seq = seq+(x,)
        if self.hyperparameters.length_constraint:
            return reduce(lambda a,b : a+b, seq[1:]) if len(seq)>1 else ""
        else:
            return reduce(lambda a,b : a+b, seq[1:-1]) if len(seq)>2 else ""



def test(no_chars= False):
    import codecs
    f = codecs.open("/vol/work/godard/dat/bulb.5/mbochi/REFS/mbochi.fullsup.words.ortho.4",encoding="utf-8")
    lines = f.read().splitlines()
    f.close()
    tm = ToneCharacterModel(None, ToneCharacterModel_Hyperparameters(no_chars = no_chars))
    lines_it = iter(lines)
    for i in range(100):
        tm.cache(next(lines_it))
    return tm


def words():
    import codecs
    res = set()
    f = codecs.open("/vol/work/godard/dat/bulb.5/mbochi/REFS/mbochi.fullsup.words.ortho.4",encoding="utf-8")
    lines = f.read().splitlines()
    f.close()
    for l in lines:
        for word in l.split():
            res.add(word)
    return res

def backoff_test(no_chars=False):
    tm = test(no_chars=no_chars)
    import numpy as np
    w = words()
    word = list(w)[np.random.randint(len(w))]
    bo = tm.hpyp.backoff
    print word
    if no_chars:
        context = (tm.initial,)
        for char in word:
            context = context + ((split(char)[1],) if split(char)[1]!=SENTINEL else tuple())
    else:
        context = ((tm.initial,)+tuple(word), tuple())
    while context!=None:
        if no_chars:
            print ("empty" if len(context)==0 else reduce(lambda a,b: a+' '+b, map(lambda c: 'I' if c==tm.initial else c, context)))
        else:
            print ("empty" if len(context[0])==0 else reduce(lambda a,b:a+b, map(lambda c: 'I' if c==tm.initial else c,context[0])))+" / "+(reduce(lambda a,b:a+' '+b, map(lambda c: "I" if c==tm.initial else c, context[1])) if len(context[1])>0 else "empty")
        context = bo(context)
                   

    

import globalz
import numpy as np
from tools import sample, slice_sample, slice_sample_step, beta, uniform, gamma, exponential, dirac, poisson
import types
import math

class CharNGram_Hyperparameters:
    def __init__(self,order=3,avg_length = 5.,smoothing=0.05):
        """
        order : Order of the character n-gram model
        smoothing : back-off with a probability 0.05 to the lower order model
        avg_length : Average segment length (expectancy of the geometric law used to generate segment lengths)
        """
        self.order = order
        self.smoothing = smoothing
        self.pstop = 1./avg_length
        self.counts = dict()
        self.countsZ = dict()
        self.T = lambda context: (tuple(context[-(order-1):]) if order>1 else tuple())

    def _cache_char_after_context(self,context,char):
        if context in self.counts:
            if char in self.counts[context]:
                self.counts[context][char] += 1.
            else:
                self.counts[context][char] = 1.
            self.countsZ[context] += 1.
        else:
            self.counts[context] = {context : {char: 1.}}
            self.countsZ[context] = 1.

    def _uncache_char_after_context(self,context,char):
        if context in self.counts:
            if char in self.counts[context]:
                self.counts[context][char] -= 1.
                if self.counts[context][char]==0.:
                    self.counts[context].pop(char)
            self.countsZ[context] -= 1.
            if self.countsZ[context]==0.:
                self.countsZ.pop(context)
                self.counts.pop(context)
        else:
            print "WARNING in "+self.__repr__()+": Char uncached in CharNGram when it was already absent before"

    def cache_ngram(self,context_,char):
        context = self.T(context_)
        for k in range(0,len(context)+1):
            self._cache_char_after_context(context[k:],char)

    def uncache_ngram(self,context_,char):
        context = self.T(context_)
        for k in range(0,len(context)+1):
            self._uncache_char_after_context(context[k:],char)
            
    def _P_char_after_context(self,context,char):
        if context in self.counts:
            if char in self.counts[context]:
                return self.counts[context][char]/self.countsZ[context]
        return 0.

    def P_ngram(self,context,char): # Probability of ngram including backoff
        if len(context)==0:
            return (1-self.smoothing)*(self._P_char_after_context(context,char)) + self.smoothing
        else:
            return (1-self.smoothing)*(self._P_char_after_context(context,char)) + self.smoothing*(self.P_ngram(context[1:],char))
        
    def reset(self):
        pass


class CharNGram:
    """
    A simple character n-gram model to be used as a base distribution of morph cache models
    """
    def __init__(self,base,hyperparameters):
        """
        base : irrelevant
        hyperparameters : a CharNGram_Hyperparameters object
        """
        self.initial = 0 # sentinel object for beginning of string
        self.final = 1 # sentinel object for end of string
        self.hyperparameters = hyperparameters
        
    def reset(self):
        self.hyperparameters.reset()

    @staticmethod
    def hyperparameters_class():
        return CharNGram_Hyperparameters

    @staticmethod
    def resample_hyperparameters(initial,VERBOSE=True):
        pass

    def cache(self,seq):
        pf = (self.initial,)
        for x in seq:
            self.hyperparameters.cache_ngram(pf,x)
            pf = pf+(x,)

    def uncache(self,seq):
        pf = (self.initial,)
        for x in seq:
            self.hyperparameters.uncache_ngram(pf,x)
            pf = pf+(x,)

    def P(self,seq):
        pf = (self.initial,)
        log_p = 0.
        for x in seq:
            log_p += (np.log(self.hyperparameters.P_ngram(self.hyperparameters.T(pf),x)) + np.log(1.-self.hyperparameters.pstop))
            pf += (x,)
        log_p -= np.log(1.-self.hyperparameters.pstop) + np.log(self.hyperparameters.pstop)
        return np.exp(log_p)

    def sample(self):
        return "?"

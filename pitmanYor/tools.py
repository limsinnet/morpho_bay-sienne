# coding=utf-8

import random
import numpy as np

def randomU01():
    return random.random()

def randomInt(a,b):
    return random.randint(a,b)

loggamma = lambda z: ( 0.5*(np.log(2*np.pi) - np.log(z)) + z*(np.log(    z+(1./((12.*z)-(1./(10.*z))) )  ) - 1.) )  # (approximation de Gergo Nemes)

class print_once:
    printed = set()
    def __init__(self,msg):
        if msg not in print_once.printed:
            print_once.printed.add(msg)
            print msg

class mixture:
    """ Mix several distributions """
    def __init__(self, distributions, weights=None):
        """
        distributions : A list of distributions
        weights : weights[i] is the weight of distributions[i] in the mixture (must sum to one). If left empty, by default the mixture will be uniform
        """
        self.distributions = distributions
        self.weights = [1./float(len(distributions))]*len(distributions) if weights==None else weights
        self.n_distributions = len(self.distributions)
    
    def sample(self):
        # First, choose a distribution
        x = np.random.random()
        s = 0.
        for i in range(self.n_distributions):
            s += self.weights[i]
            if s>=x:
                # Then return a sample from that distribution
                return self.distributions[i].sample()

    def unnormalized_logP(self,x):
        return np.log(sum(map(lambda i: self.weights[i]*np.exp(self.distributions[i].logP(x)), range(self.n_distributions))))

    def logP(self,x):
        return self.unnormalized_logP(x)

    def __repr__(self):
        res = "Mixture( "
        for i in range(self.n_distributions):
            res += (str(self.weights[i])+"*"+str(self.distributions[i])+" + ")
        return res[:-2]+")"
                            

class beta:
    """ The beta distribution """
    def __init__(self,alpha=1.,beta=1.):
        self.alpha, self.beta = alpha, beta

    def sample(self):
        return np.random.beta(self.alpha, self.beta)

    def unnormalized_logP(self,x):
        return (self.alpha-1.)*np.log(x) + (self.beta-1.)*np.log(1.-x)

    def __repr__(self):
        return "Beta( alpha="+str(self.alpha)+", beta="+str(self.beta)+" )"

class uniform:
    """ The uniform distribution over an interval """
    def __init__(self,A=0., B=1.):
        self.A,self.B = A,B

    def sample(self):
        return self.A + np.random.random()*(self.B-self.A)

    def unnormalized_logP(self,x):
        return 0.

    def __repr__(self):
        return "Uniform("+str(self.A)+", "+str(self.B)+")"

class poisson:
    """ The Poisson distribution """
    def __init__(self,l=1.):
        self.l = l

    def sample(self):
        L = np.exp(-self.l)
        k = 0
        p = 1
        while p>L:
            k += 1
            p *= np.random.random()
        return k-1

    def unnormalized_logP(self,k):
        return k*np.log(self.l)-self.l-sum(map(np.log,range(2,k+1)))

    def P(self,k):
        return np.exp(-self.l)*reduce(lambda x,y:x*y, map(lambda z: float(self.l)/float(z),range(1,k+1)))

class gamma:
    """ The gamma distribution """
    def __init__(self,shape=1.,scale=1.):
        self.shape, self.scale = shape, scale

    def sample(self):
        return np.random.gamma(shape=self.shape, scale=self.scale)

    def unnormalized_logP(self,x):
        return (self.shape-1.)*np.log(x) - (x/self.scale)

    def logP(self,x):
        return (self.shape-1.)*np.log(x) - (x/self.scale) - loggamma(self.shape) - (self.shape)*np.log(self.scale)

    def __repr__(self):
        return "Gamma( shape="+str(self.shape)+", scale="+str(self.scale)+" )"


class exponential:
    """ The exponential distribution """
    def __init__(self,scale=1.0):
        self.scale = scale

    def sample(self):
        return np.random.exponential(scale=self.scale)

    def unnormalized_logP(self,x):
        return -np.log(self.scale)-(x/self.scale)

    def logP(self,x):
        return self.unnormalized_logP(x)

    def __repr__(self):
        return "Exponential( scale="+str(self.scale)+" )"

class dirac:
    """ The Dirac distribution centered on an object """
    def __init__(self,center):
        self.center = center

    def sample(self):
        return self.center

    def unnormalized_logP(self,x):
        return (0. if x==self.center else (-np.inf))

    def __repr__(self):
        return "Dirac("+str(self.center)+")"


def test_convergence(records, key=(lambda entry: entry),
                     min_time_window = 3000,
                     time_window_ratio = 0.2,
                     value_window_ratio = 0.01
                     ):
    """
    Used to test whether some function of time described in 'records' has converged.
    records : A dict mapping integer times to entries
    key : Some function of an entry returning a number (int or float)
    min_time_window : Minimum time window for convergence
    time_window_ratio : Ratio of total recording time to test convergence
    value_window_ratio : Convergence will happen when all values in the time window will be within an interval of value_window_ratio times the average absolute value during the time window
    """
    if len(records)==0:
        return False

    most_recent_time = max(records.keys())
    most_ancient_time = min(records.keys())
    if(most_ancient_time+min_time_window>=most_recent_time):
        return False
    
    time_window = max(min_time_window, (most_recent_time-most_ancient_time)*time_window_ratio)

    tw_recent_values = map(lambda y: key(y[1]),filter(lambda x: x[0]+time_window>=most_recent_time, records.items()))
    if(len(tw_recent_values)<2):
        return False
    avg_absolute_value = sum(map(float,map(np.abs,tw_recent_values)))/len(tw_recent_values)
    return np.abs(max(tw_recent_values)-min(tw_recent_values))<(avg_absolute_value*value_window_ratio)
    


def cumulative_sample(lst, F):
    """
    Fast sampling given a list and the cumulative function of a probability distribution
    """
    x = np.random.random()
    L = 0
    R = len(lst)
    while (R-L)>1:
        M = (L+R)//2
        if F(lst[M])<=x:
            L = M
        else:
            R = M
    return lst[L]



def sample(iterator, P, normalized=True, annealing=1.):
    """
    Samples an element from an iterator according to a probability distribution P
    
    Parameters
    ----------
    iterator : The iterator to sample from
    P : The probability function (takes an element from the iterator, and returns a real-valued number)
    normalized : A boolean indicating whether P is normalized on the iterator
    annealing : (If different from 1., overrides the 'normalized' argument).
                annealing==1. -> sample from the true distribution
                annealing==0. -> sample from the uniform distribution
                0. <= annealing <= 1.
    """
    if not normalized or annealing!=1.:
        Z = 0.
        for el in iterator:
            Z += np.power(P(el), annealing)
    else:
        Z = 1.
    x = np.random.random()
    y = 0.
    for el in iterator:
        y += np.power(P(el),annealing)/Z
        if y>=x:
            return el
    raise Exception("Options exhausted during sampling ! Check whether the distribution was normalized.")


def MHcheck(acceptance_logratio):
    """
    Performs a Metropolis-Hastings acceptance check given the difference between the log-probabilities of the source and target states.
    Returns True if the target state has passed the test, else False.
    """
    if acceptance_logratio>0:
        return True
    else:
        acceptance_ratio = np.exp(acceptance_logratio)
        return ( random.random()<=acceptance_ratio )
    

def slice_sample(logQ,
                 w = 10.,
                 initial = exponential(1.),
                 n = 10,
                 policy = "stepping_out",
                 VERBOSE = False):
    """
    Performs slice-sampling from an unnormalized log-density function over the real numbers

    Parameters
    ----------
    logQ : the log-density function
    w : estimate of the typical size of a slice
    initial : a distribution to initialize sampling (must be nonzero only where Q is nonzero
    n :number of iterations
    policy : can be set either to 'stepping_out' or 'doubling'

    Returns a sample from the distribution
    """
    if policy != 'stepping_out' and policy != 'doubling':
        print "Error: unrecognized policy for slice sampling: "+str(policy)
        return

    x = initial.sample()
    for it in range(0,n):
        y = logQ(x) - np.random.exponential(scale=1.)
        L = x - w*np.random.random()
        R = L + w
        # Find an interval containing a large portion of the slice {z | logQ(z)<y}
        if policy=='stepping_out':
            while logQ(L)>y:
                L -= w
            while logQ(R)>y:
                R += w
        elif policy=='doubling':
            while (logQ(L)>y or logQ(R)>y):
                if np.random.randint(0,2)==0:
                    L -= (R-L)
                else:
                    R += (R-L)
        if VERBOSE:
            print "Current unnormalized log-probability: "+str(logQ(x))
            print "Current x: "+str(x)+" - Sampling interval: ("+str(L)+", "+str(R)+")"
        # Sample from (L,R), excluding samples which are not part of the subgraph, and in that case shrinking the interval (L,R)
        while True:
            x1 = L + (R-L)*np.random.random()
            if logQ(x1)>y:
                x = x1
                break
            else:
                if x1<x:
                    L = x1
                else:
                    R = x1
                if VERBOSE:
                    print "Sample rejected, interval shrunk to ("+str(L)+", "+str(R)+")"
    return x

def slice_sample_step(logQ,
                      x_,
                      w = 10,
                      policy = "stepping_out",
                      VERBOSE = False,
                      timeout = 100):
    if policy != 'stepping_out' and policy != 'doubling':
        print "Error: unrecognized policy for slice sampling: "+str(policy)
        return

    x = x_

    y = logQ(x) - np.random.exponential(scale=1.)
    L = x - w*np.random.random()
    R = L + w
    # Find an interval containing a large portion of the slice {z | logQ(z)<y}
    if policy=='stepping_out':
        while logQ(L)>y:
            L -= w
        while logQ(R)>y:
            R += w
    elif policy=='doubling':
        while (logQ(L)>y or logQ(R)>y):
            if np.random.randint(0,2)==0:
                L -= (R-L)
            else:
                R += (R-L)
    if VERBOSE:
        print "Current unnormalized log-probability: "+str(logQ(x))
        print "Current x: "+str(x)+" - Sampling interval: ("+str(L)+", "+str(R)+")"
    # Sample from (L,R), excluding samples which are not part of the subgraph, and in that case shrinking the interval (L,R)
    iteration = 0
    while True:
        if timeout!=None and iteration>timeout:
            print "WARNING : Timeout during slice sampling"
            return x_
        else:
            iteration += 1
        x1 = L + (R-L)*np.random.random()
        if logQ(x1)>y:
            x = x1
            break
        else:
            if x1<x:
                L = x1
            else:
                R = x1
            if VERBOSE:
                print "Sample rejected, interval shrunk to ("+str(L)+", "+str(R)+")"
    return x

                       


def MCMC(initial, transition, logQ, n_steps=100, n_particles=10,
         VERBOSE = False):
    """
    Performs Monte Carlo Markov Chain sampling.

    Parameters
    ----------
    initial : A function that takes no arguments and returns a random element of some domain X (the search space).
    transition: A function that takes an element of X and stochastically returns another element of X. The proposal distribution must be symmetric.
    logQ : A function that takes an element of X and returns its unnormalized log-probability
    n_steps, n_particles : Self-explanatory
    """

    particles = map(lambda _: initial(), range(n_particles))
    particles_logQ = map(logQ, particles)
    for iteration in range(n_steps):
        if VERBOSE:
            print iteration, particles
        new_particles_before_MH = map(transition, particles)
        logQ_new_particles_before_MH = map(logQ, new_particles_before_MH)
        for i in range(n_particles):
            if MHcheck(logQ_new_particles_before_MH[i]-particles_logQ[i]):
                particles[i] = new_particles_before_MH[i]
                particles_logQ[i] = logQ_new_particles_before_MH[i]
    return particles[max(range(n_particles),
                         key = lambda i: particles_logQ[i])]

def asymmetric_MCMC(initial, transition, logQ, transition_logQ, n_steps=20, n_particles=5,
                    VERBOSE = False):
    """
    Performs Monte Carlo Markov Chain sampling with an asymmetric proposal distribution

    Parameters
    ----------
    initial : A function that takes no arguments and returns a random element of some domain X (the search space).
    transition: A function that takes an element of X and stochastically returns another element of X.
    transition_logQ : A function that takes two elements (x,y) of X and computes the (unnormalized) log-probability of transiting from x to y.
    logQ : A function that takes an element of X and returns its unnormalized log-probability.
    n_steps, n_particles : Self-explanatory.
    """

    particles = map(lambda _: initial(), range(n_particles))
    particles_logQ = map(logQ, particles)
    for iteration in range(n_steps):
        if VERBOSE:
            print iteration, particles
        new_particles_before_MH = map(transition, particles)
        logQ_new_particles_before_MH = map(logQ, new_particles_before_MH)
        TlogQ_xy = map(lambda xy: transition_logQ(*xy), zip(particles, new_particles_before_MH))
        TlogQ_yx = map(lambda yx: transition_logQ(*yx), zip(new_particles_before_MH, particles))
        for i in range(n_particles):
            if MHcheck((TlogQ_yx[i]+logQ_new_particles_before_MH[i])-(TlogQ_xy[i]+particles_logQ[i])):
                particles[i] = new_particles_before_MH[i]
                particles_logQ[i] = logQ_new_particles_before_MH[i]
    return particles[max(range(n_particles),
                         key = lambda i: particles_logQ[i])]


def metropolisSampling(proposal, logQ, steps=200):
    """
    Performs Metropolis sampling.
    
    Parameters
    ----------
    proposal : A function that takes no arguments and returns an element of some domain X.
    logQ : An unnormalized log-density function on the domain X.
    steps : Number of steps to run the algorithm for
    """
    state = proposal()
    state_logQ = logQ(state)
    for i in range(steps):
        newState = proposal()
        newState_logQ = logQ(newState)
        if MHcheck(newState_logQ-state_logQ):
            state = newState
            state_logQ = newState_logQ
    return state


def shuffle(L):
    """
    Shuffles a list. (Careful: overrides the list !)
    """
    for i in range(len(L)):
        k = np.random.randint(i,len(L))
        (L[i], L[k]) = (L[k], L[i])


def pr(text,val):
    print text+": "+str(val)
    return val



class SparseDict:
    """
    A class exposing the same interface as dictionaries, but allowing to assign a default value to previously unseen elements and accessing them as any other element
    """
    def __init__(self,
                 initial = None,
                 defaultValue = 0.,
                 erase_when_equal_to_default = True,
                 maintain_sum = False
                 ):
        """
        Parameters
        ----------
        initial : A dictionary to initialize the object. If None, initialized as a new dictionary.
        defaultValue : The default value that will be assigned to keys that have not been previously set.
        erase_when_equal_to_default : A boolean determining whether if a key is assigned the default value, it will be removed from the internal dictionary.
        maintain_sum : Maintains a separate variable containing the sum of all elements (useful for efficient normalization for probability distributions). Works only if defaultValue is 0.
        """
        if initial==None:
            self._dict = dict()
        else:
            self._dict = initial

        self.defaultValue = defaultValue
        if maintain_sum:
            self.defaultValue = 0.
        self.erase_when_equal_to_default = erase_when_equal_to_default
        self.maintain_sum = maintain_sum
        if self.maintain_sum:
            self.Z = sum(map(lambda x: x[1], self._dict.items()))
        
    def __getitem__(self,key):
        if key in self._dict:
            return self._dict[key]
        else:
            return self.defaultValue

    def __setitem__(self,key,val):
        if val==self.defaultValue and self.erase_when_equal_to_default:
            if self.maintain_sum and key in self._dict:
                self.Z -= self._dict[key]
            if key in self._dict:
                self._dict.pop(key)
        else:
            if key in self._dict and self.maintain_sum:
                self.Z -= self._dict[key]
            if self.maintain_sum:
                self.Z += val
            self._dict[key] = val

    def __repr__(self):
        return self._dict.__repr__()

    def __iter__(self):
        return self._dict.__iter__()

    def __contains__(self,key):
        return (key in self._dict)

    def clone(self):
        res = SparseDict(defaultValue = self.defaultValue,
                         erase_when_equal_to_default = self.erase_when_equal_to_default,
                         maintain_sum = self.maintain_sum)
        for x in self._dict:
            res[x] = self._dict[x]

        return res


    def Sum(self):
        """
        Returns the sum of elements
        """
        return self.Z

"""
An example: Chinese Restaurant Process
"""

from pitmanYor import *
import numpy as np

def crp(N=1000):
    pyp = PYP(base = PDF(_P= lambda _ : 1./100, _sample = lambda : np.random.randint(0,100)),
              hyperparameters = PYP.hyperparameters_class()(theta=10., discount=0.001))
    for i in range(N):
        pyp.cache(pyp.sample())
    return pyp

"""
An example: infinite N-gram model using the defined class
"""

from pitmanYor import *
import numpy as np

text = "bonjour ceci est un test du infinite gram model j'espere que ca va bien fonctionner la procedure igram permet d'apprendre un infinite gram model sur une chaine de caracteres et la procedure open vocabulary language model permet d apprendre un modele de langue d ordre N sur une liste de mots en faisant un backoff sur un infinite gram model pour conserver un vocabulaire ouvert"

def igram(N):
    model = InfiniteGram(None, InfiniteGram_Hyperparameters(maximum_markov_order=N))
    for i in range(100):
        model.cache(text)
    return model


def open_vocabulary_LM(N):
    model = CacheHierarchy(
        model_classes = lambda pf: ( InfiniteGram if pf==None else PYP ),
        backoff = lambda pf: (pf[1:] if (pf!=None and len(pf)>=1) else None),
        hyperparameters_hash = lambda pf: len(pf) if pf!=None else None,
        hyperparameters = lambda pf: dict()
        )
    words = text.split(' ')
    for i in range(len(words)):
        model(tuple(words[i-N+1:i])).cache(words[i])
    return model

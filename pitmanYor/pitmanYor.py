import globalz
import numpy as np
from tools import sample, slice_sample, slice_sample_step, beta, uniform, gamma, exponential, dirac, poisson
import types
import math


class CacheHierarchy:
    """
    A class describing a hierarchy of cache models (for example: Hierarchical Pitman-Yor process...)
    """
    def __init__(self, model_classes, backoff, hyperparameters = lambda ID : dict(), hyperparameters_hash = lambda ID : None):
        """
        model_classes : A function mapping an ID to the class the model with that ID is supposed to be an instance of
                 (for example: for an infinite-gram model, map every prefix string to the class 'PYP', except the empty string to a wrapper class 'PDF' whose instances are a probability distributions over the charset)
        hyperparameters : A function mapping an ID to 'None' or a dictionary of keyword arguments used to initialize the hyperparameters of the model 'ID'
        backoff : A function mapping the ID of a cache model to the ID of its backoff model
                  (for example: for an infinite-gram model, map the prefix string 'pf' to the truncated prefix string 'pf[1:]')
        hyperparameter_hash : A function mapping the ID of a cache model to a hash-code such that cache models sharing the same hash-code share the same set of hyperparameters
        """
        self.model_classes = model_classes
        self.hyperparameters = hyperparameters
        self.backoff = backoff
        self.hyperparameters_hash = hyperparameters_hash
        self.models = dict()
        self.hyperparameters_table = dict()

    def __call__(self,ID):
        if ID in self.models:
            return self.models[ID]
        else:
            if self.hyperparameters_hash(ID) not in self.hyperparameters_table:
                self.hyperparameters_table[self.hyperparameters_hash(ID)] = self.model_classes(ID).hyperparameters_class()(**self.hyperparameters(ID))
            self.models[ID] = self.model_classes(ID)(base = lambda : self(self.backoff(ID)),
                                                     hyperparameters = self.hyperparameters_table[self.hyperparameters_hash(ID)])
            return self.models[ID]

    def resample_hyperparameters(self, VERBOSE=True):
        HPhash_to_classes = dict()
        for ID in self.models:
            H = self.hyperparameters_hash(ID)
            if H not in HPhash_to_classes:
                HPhash_to_classes[H] = self.model_classes(ID)
        for H in HPhash_to_classes:
            if VERBOSE:
                print " "
                print "Resampling hyperparameters for hash: "+str(H)+" (model class: "+str(HPhash_to_classes[H])+" )"
            HPhash_to_classes[H].resample_hyperparameters(self.hyperparameters_table[H])

    def reset(self):
        """
        Empties all cache models in the hierarchy
        """
        for ID in self.models:
            self.models[ID].reset()
        

class PDF_Hyperparameters:
    def __init__(self, P=None, sample=None):
        self.P = P
        self.sample = sample

    def reset(self):
        pass


class PDF:
    def __init__(self, base, hyperparameters):
        self.hyperparameters = hyperparameters
    def P(self,x):
        return self.hyperparameters.P(x)
    def cache(self,x):
        return
    def uncache(self,x):
        return
    def sample(self):
        return self.hyperparameters.sample()
    def reset(self):
        pass

    @staticmethod
    def hyperparameters_class():
        print "hyperparmeters_class"
        return PDF_Hyperparameters

    @staticmethod
    def resample_hyperparameters(initial):
        pass




class Bins:
    """
    A class describing a sparse mapping for keeping track of table/customer counts. ie: a table is a "bin" whose items are customers, or a restaurant is a "bin" whose items are tables (or customers). Bins with no items are thrown away
    """
    def __init__(self):
        self.nb_bins_with_k_items = dict()

    def is_empty(self):
        return (len(self.nb_bins_with_k_items)==0)

    def add(self,k): # Take a bin with k items and add one more item to it 
        if k!=0:
            self.nb_bins_with_k_items[k] -= 1
        if k!=0 and self.nb_bins_with_k_items[k]==0:
            self.nb_bins_with_k_items.pop(k)
        if (k+1) in self.nb_bins_with_k_items:
            self.nb_bins_with_k_items[(k+1)] += 1
        else:
            self.nb_bins_with_k_items[(k+1)] = 1

    def remove(self,k): # Take a bin with k items and remove one item from it
        self.nb_bins_with_k_items[k] -= 1
        if self.nb_bins_with_k_items[k]==0:
            self.nb_bins_with_k_items.pop(k)
        if k==1:
            return
        if (k-1) in self.nb_bins_with_k_items:
            self.nb_bins_with_k_items[(k-1)] += 1
        else:
            self.nb_bins_with_k_items[(k-1)] = 1

    def with_k_items(self,k):
        return self.nb_bins_with_k_items[k]

    def enumerate_item_counts(self):
        for k in self.nb_bins_with_k_items:
            yield k

    def nb_of_items(self):
        N = 0
        for k in self.nb_bins_with_k_items:
            N += k*self.nb_bins_with_k_items[k]
        return N

    def nb_of_bins(self):
        N = 0
        for k in self.nb_bins_with_k_items:
            N += self.nb_bins_with_k_items[k]
        return N

    def reset(self):
        self.nb_bins_with_k_items = dict()

    def __str__(self):
        Ks = sorted(self.nb_bins_with_k_items, reverse=True)
        s = ""
        for k in Ks:
            s += str(self.nb_bins_with_k_items[k])+" bins with "+str(k)+" items\n"
        if s=="":
            return "Empty collection"
        else:
            return s[:-1]


class PYP_Hyperparameters:
    """ A class describing a set of Pitman-Yor process hyperparameters, that can possibly be shared across several PYPs
    """
    def __init__(self, theta = None, discount = None, theta_prior = exponential(scale=10.) , discount_prior = beta(alpha=1., beta=5.), lock_theta = False, lock_discount = False):
        self.theta_prior = theta_prior
        self.discount_prior = discount_prior
        self.theta = theta_prior.sample() if theta==None else theta
        self.discount = discount_prior.sample() if discount==None else discount
        self.lock_theta, self.lock_discount = lock_theta, lock_discount
        self.tables_with_n_customers = Bins()
        self.restaurants_with_n_customers = Bins()
        self.restaurants_with_k_tables = Bins()

    def add_customer(self,_total_customers, _customers_at_table, _nb_tables,check=False):
        arguments = map(int,(_total_customers,_customers_at_table,_nb_tables))
        total_customers, customers_at_table, nb_tables = arguments
        if check:
            self.check_bin_consistency(comment=("before add_customer"+str(arguments)))
        self.tables_with_n_customers.add(customers_at_table) # "on one of the tables that previously had 'customers_at_table" customers, a customer has been added"
        self.restaurants_with_n_customers.add(total_customers) # "in one of the restaurants that previously had 'total_customers' customers, a customer has entered"
        if customers_at_table==0: # create new table
            self.restaurants_with_k_tables.add(nb_tables) # "in one of the restaurants that previously had 'nb_tables' tables, a table has been added"
        if check:
            self.check_bin_consistency(comment=("after add_customer"+str(arguments)))


    def remove_customer(self,_total_customers, _customers_at_table, _nb_tables,check=False):
        arguments = map(int,(_total_customers,_customers_at_table,_nb_tables))
        total_customers, customers_at_table, nb_tables = arguments
        if check:
            self.check_bin_consistency(comment=("before remove_customer"+str(arguments)))
        self.tables_with_n_customers.remove(customers_at_table) # "on one of the tables that previously had 'customers_at_table" customers, a customer has been removed"
        self.restaurants_with_n_customers.remove(total_customers) # "in one of the restaurants that previously had 'total_customers' customers, a customer has exited"
        if customers_at_table==1: # remove table
            self.restaurants_with_k_tables.remove(nb_tables) # "in one of the restaurants that previously had 'nb_tables' tables, a table has been removed"
        if check:
            self.check_bin_consistency(comment=("after remove_customer"+str(arguments)))


    def check_bin_consistency(self,comment=""):
        # Customer count
        customers_according_to_tables = self.tables_with_n_customers.nb_of_items()
        customers_according_to_restaurants = self.restaurants_with_n_customers.nb_of_items()
        # Table count
        tables_according_to_tables = self.tables_with_n_customers.nb_of_bins()
        tables_according_to_restaurants = self.restaurants_with_k_tables.nb_of_items()
        # Restaurant count
        restaurants_according_to_customers = self.restaurants_with_n_customers.nb_of_bins()
        restaurants_according_to_tables = self.restaurants_with_k_tables.nb_of_bins()
        # Check
        check = (customers_according_to_tables==customers_according_to_restaurants) and (tables_according_to_tables==tables_according_to_restaurants) and (restaurants_according_to_customers==restaurants_according_to_tables)
        if not check:
            print "BIN INCONSISTENCY: "+comment
            print "Customers: "+str(customers_according_to_tables)+" "+str(customers_according_to_restaurants)
            print "Tables: "+str(tables_according_to_tables)+" "+str(tables_according_to_restaurants)
            print "Restaurants: "+str(restaurants_according_to_customers)+" "+str(restaurants_according_to_tables)
            print "Tables with n customers: "+str(self.tables_with_n_customers)
            print "Restaurants with n customers: "+str(self.restaurants_with_n_customers)
            print "Restaurants with k tables: "+str(self.restaurants_with_k_tables)

    def reset(self):
        self.tables_with_n_customers.reset()
        self.restaurants_with_n_customers.reset()
        self.restaurants_with_k_tables.reset()

    def __str__(self):
        return "theta = "+str(self.theta)+(" (locked)" if self.lock_theta else "")+" / discount = "+str(self.discount)+(" (locked)" if self.lock_discount else "")



class PYP:
    def __init__(self, base, hyperparameters):
        """
        base : A function taking no arguments and returning the base distribution of the PYP (in order to postpone the construction of the base distribution until when it is needed
        hyperparameters : The hyperparameter set of the PYP
        """
        self.base = base
        self.hyperparameters = hyperparameters
        self.customers_per_table = dict()
        self.total_customers = 0.
        self.table_labels = dict()
        self.label_tables = dict()
        self.filter_set = set() # A set collecting all items that will not undergo backoff

    def reset(self):
        self.hyperparameters.reset()
        self.customers_per_table = dict()
        self.total_customers = 0.
        self.table_labels = dict()
        self.label_tables = dict()

    def add_to_filter(self,x):
        self.filter_set.add(x)

    @staticmethod
    def hyperparameters_class():
        return PYP_Hyperparameters

    @staticmethod
    def resample_hyperparameters(initial,
                                 chain_length=10,
                                 VERBOSE=True):
        """
        Resamples hyperparameters according to the setup of all chinese restaurants bound to the same hyperparameter set 'initial'
        initial : A 'PYP_Hyperparameters' object (the hyperparameters to resample from)
        """
        if VERBOSE:
            print str(initial)
        
        theta, discount = initial.theta, initial.discount

        if initial.restaurants_with_n_customers.is_empty():
            if VERBOSE:
                print "This hyperparameter set has only empty restaurants attached to it ! Hyperparameters will be resampled by merely resampling the prior"
            initial.theta, initial.discount = (initial.theta if initial.lock_theta else initial.theta_prior.sample()) , (initial.discount if initial.lock_discount else initial.discount_prior.sample())
            return


        def logQ(T,D):
            loggamma = lambda z: ( 0.5*(np.log(2*np.pi) - np.log(z)) + z*(np.log(    z+(1./((12.*z)-(1./(10.*z))) )  ) - 1.) )  # (approximation de Gergo Nemes)
            _logQ = 0.
            for n_c in initial.tables_with_n_customers.enumerate_item_counts():
                _logQ += initial.tables_with_n_customers.with_k_items(n_c)*( loggamma(n_c-D) - loggamma(1.-D) )
            for N in initial.restaurants_with_n_customers.enumerate_item_counts():
                _logQ += initial.restaurants_with_n_customers.with_k_items(N)*( loggamma(T) - loggamma(T+N) )
            for K in initial.restaurants_with_k_tables.enumerate_item_counts():
                _logQ += initial.restaurants_with_k_tables.with_k_items(K)*( K*np.log(D) + loggamma(K + T/D) - loggamma(T/D) )
                _logQ += initial.restaurants_with_k_tables.with_k_items(K)*(
                    initial.theta_prior.unnormalized_logP(T) + initial.discount_prior.unnormalized_logP(D)
                    )
            return _logQ

        for it in range(chain_length):
            """
            logQ = lambda T,D : sum(map(lambda pyp : pyp.hyperparameter_posterior_logP(PYP_Hyperparameters(theta = T,
                                                                                                           discount = D,
                                                                                                           theta_prior = initial.theta_prior,
                                                                                                           discount_prior = initial.discount_prior)),
                                        pyp_set))
            """
            if not initial.lock_theta:
                new_theta = slice_sample_step(logQ = lambda T: logQ(T, discount),
                                              x_= theta,
                                              w = 1.,
                                              policy = "doubling",
                                              timeout = 100)
            else:
                new_theta = theta

            if not initial.lock_discount:
                new_discount = slice_sample_step(logQ = lambda D: logQ(new_theta, D),
                                                 x_ = discount,
                                                 w = 0.1,
                                                 policy = "stepping_out",
                                                 timeout = 100)
            else:
                new_discount = discount

            theta,discount = new_theta, new_discount
            if VERBOSE:
                print str(PYP_Hyperparameters(theta = theta,
                                              discount = discount,
                                              theta_prior = initial.theta_prior,
                                              discount_prior = initial.discount_prior,
                                              lock_theta = initial.lock_theta,
                                              lock_discount = initial.lock_discount))                                            
        initial.theta, initial.discount = theta, discount
        
        
        

    def Print(self, N=200):
        top_tables = sorted(self.customers_per_table, key = lambda t: self.customers_per_table[t],reverse=True)[:N]
        for table in top_tables:
            print str(self.table_labels[table])+" : "+str(self.customers_per_table[table])

        
    def P(self,x):
        """
        Returns the probability of observation 'x' conditioned on the cache
        """
        p = 0.
        T = self.hyperparameters.theta
        D = self.hyperparameters.discount
        N = self.total_customers
        K = len(self.customers_per_table)
        if (x in self.label_tables):
            for k in self.label_tables[x]:
                p += (self.customers_per_table[k] - D)/(T+N)
        if not (x in self.filter_set):
            p += ((T+K*D)/(T+N))*self.base().P(x)
        return p

    def cache(self,x):
        p = dict()
        Z = 0.
        T,D = self.hyperparameters.theta, self.hyperparameters.discount
        K = len(self.customers_per_table)
        N = self.total_customers
        if (x in self.label_tables):
            for k in self.label_tables[x]:
                p_ = (self.customers_per_table[k]-D)/(T+N)
                p[k] = p_
                Z += p_
        p_ = 0. if (x in self.filter_set) else ((T+K*D)/(T+N))*self.base().P(x)
        p['new'] = p_
        Z += p_
        if Z>0:
            table = sample(p, lambda k: p[k]/Z, normalized=True)
        else:
            table = 'new'

        if table=='new':
            if not (x in self.filter_set):
                self.base().cache(x)
            self.hyperparameters.add_customer(N, 0, K) # Meaning: "in a restaurant that previously had N customers and K tables, a customer has been added to a table that was previously empty"
            k = max(self.customers_per_table.keys())+1 if len(self.customers_per_table)>0 else 1
            self.customers_per_table[k] = 1.
            self.total_customers += 1.
            self.table_labels[k] = x
            if x in self.label_tables:
                self.label_tables[x].add(k)
            else:
                self.label_tables[x] = set([k])
            
        else:
            Nk = self.customers_per_table[k]
            self.hyperparameters.add_customer(N, Nk, K) # Meaning: "in a restaurant that previously had N customers and K tables, a customer has been added to a table that previously had Nk customers"
            self.customers_per_table[k] += 1.
            self.total_customers += 1.


    def uncache(self,x):
        tables = self.label_tables[x]
        p = dict()
        Z = 0.
        for t in tables:
            p[t] = self.customers_per_table[t]
            Z += p[t]
        t = sample(p, lambda k: p[k]/Z, normalized=True)
        
        n = self.customers_per_table[t]
        self.hyperparameters.remove_customer(self.total_customers,n,len(self.customers_per_table)) # Meaning: "in a restaurant that previously had N customers and K tables, a customer has been removed from a table that previously had n customers"
        self.customers_per_table[t] -= 1.
        self.total_customers -= 1.

        if self.customers_per_table[t]<=0.: # delete table if emptied, and uncache from base distribution
            self.customers_per_table.pop(t)
            self.table_labels.pop(t)
            self.label_tables[x].remove(t)
            if len(self.label_tables[x])==0:
                self.label_tables.pop(x)
            if not (x in self.filter_set):
                self.base().uncache(x)
            
    def sample(self):
        sorted_tables = sorted(self.customers_per_table.keys(),
                               key = lambda t: self.customers_per_table[t],
                               reverse = True)
        y = (self.total_customers + self.hyperparameters.theta)*np.random.random()
        s = 0.
        for t in sorted_tables:
            s += (self.customers_per_table[t]-self.hyperparameters.discount)
            if s>y:
                return self.table_labels[t]
        return self.base().sample()



class InfiniteGram_Hyperparameters:
    def __init__(self, share_across_lengths = True, maximum_markov_order = None, theta_prior=exponential(10.), discount_prior = beta(1.,5.), length_constraint_gamma_prior=(30.,3.)):
        """
        theta_prior, discount_prior : Hyperparameter priors on the PYPs modeling next-character distributions
        share_across_lengths : Use the same hyperparameter set for all prefix lengths
        maximum_markov_order : Stop PYP hierarchy after prefixes of length 'maximum_markov_order' (if 'None', yields an infinite N-gram model
        length_constraint_gamma_prior : (A tuple with two elements) Parameters on the gamma distribution that acts as a prior for the length constraint of strings (if None: no length constraint)
        """
        self.share_across_lengths = share_across_lengths
        self.length_constraint = (length_constraint_gamma_prior!=None)
        self.length_constraint_gamma_prior = length_constraint_gamma_prior
        if self.length_constraint:
            self.length = gamma(length_constraint_gamma_prior[0], 1./length_constraint_gamma_prior[1]).sample()
        self.maximum_markov_order = maximum_markov_order
        if share_across_lengths:
            self.hp = {0 : PYP_Hyperparameters(theta_prior=theta_prior, discount_prior=discount_prior), None : PDF_Hyperparameters(P = lambda _ : 0., sample = lambda : '?')}
        else:
            self.hp = { None : PDF_Hyperparameters(P = lambda _ : 0., sample = lambda : '?')}
        if self.length_constraint:
            self.gamma_a, self.gamma_b = self.length_constraint_gamma_prior

    def reset(self):
        for hp_set in self.hp.values():
            hp_set.reset()
        if self.length_constraint:
            self.gamma_a, self.gamma_b = self.length_constraint_gamma_prior

    def resample_length(self):
        self.length = gamma(self.gamma_a, 1./self.gamma_b).sample()
        print "length hyperparameter = "+str(self.length)

    def cache_length(self,l):
        """
        Caches a length 'l' for resampling the length parameter
        """
        self.gamma_a += l
        self.gamma_b += 1

    def uncache_length(self,l):
        """
        Uncaches a length 'l' for resampling the length parameter
        """
        self.gamma_a -= l
        self.gamma_b += 1

        
                   
class InfiniteGram:
    """
    A class describing an infinite-gram sequence model
    """
    def __init__(self,base,hyperparameters):
        """
        base : irrelevant
        hyperparameters : a InfiniteGram_Hyperparameters object
        """
        self.initial = lambda : 0
        self.final = lambda : 1
        self.hyperparameters = hyperparameters
        self.hpyp = CacheHierarchy(model_classes = lambda pf: PDF if pf==None else PYP,
                                   backoff = lambda pf: pf[1:] if (pf!=None and len(pf)>=1) else None,
                                   hyperparameters_hash = (lambda pf : 0 if pf!=None else None) if self.hyperparameters.share_across_lengths else (lambda pf : len(pf) if pf!=None else None),
                                   hyperparameters = lambda pf: dict() if pf!=None else {'P' : (lambda _ : 0.),  'sample' : (lambda : '?')}
                                   )
        self.hpyp.hyperparameters_table = self.hyperparameters.hp
        self.T = ( lambda pf : pf ) if self.hyperparameters.maximum_markov_order==None else (lambda pf : pf[-(-1+self.hyperparameters.maximum_markov_order):])

        if globalz.PARAMS.hard_constraint_on_avg_morph_length!=None:
            self.length_histogram = dict()
            self.length_distribution = self.P_of_lengths()

    def reset(self):
        self.hyperparameters.reset()
        self.hpyp.reset()
        if globalz.PARAMS.hard_constraint_on_avg_morph_length!=None:
            self.length_histogram = dict()
            self.length_distribution = self.P_of_lengths()

    @staticmethod
    def hyperparameters_class():
        return InfiniteGram_Hyperparameters

    @staticmethod
    def resample_hyperparameters(initial,
                                 VERBOSE=True):
        for H in initial.hp:
            if H!=None:
                PYP.resample_hyperparameters(initial.hp[H], VERBOSE=VERBOSE)
        if initial.length_constraint:
                initial.resample_length()
        
    def cache(self,seq,dont_cache_length=False):
        """ Caches a sequence 'seq' """
        pf = (self.initial,)
        for x in seq:
            self.hpyp(self.T(pf)).cache(x)
            pf = pf+(x,)
        if self.hyperparameters.length_constraint and (not dont_cache_length):
            self.hyperparameters.cache_length(len(seq))
        elif globalz.PARAMS.hard_constraint_on_avg_morph_length and (not dont_cache_length):
            if len(seq) not in self.length_histogram:
                self.length_histogram[len(seq)] = 1
            else:
                self.length_histogram[len(seq)] += 1
            self.length_distribution = self.P_of_lengths()
        else:
            self.hpyp(self.T(pf)).cache(self.final)

    def uncache(self,seq):
        """ Uncaches a sequence 'seq' """
        pf = (self.initial,)
        for x in seq:
            self.hpyp(self.T(pf)).uncache(x)
            pf = pf+(x,)
        if self.hyperparameters.length_constraint:
            self.hyperparameters.uncache_length(len(seq))
        elif globalz.PARAMS.hard_constraint_on_avg_morph_length:
            self.length_histogram[len(seq)] -= 1
            self.length_distribution = self.P_of_lengths()
        else:
            self.hpyp(self.T(pf)).uncache(self.final)

    def P_of_lengths(self):
        """ In the case a hard constraint on avg morph length (globalz.PARAMS.hard_constraint_on_avg_morph_length==True), computes the distribution over possible lengths for a morph """
        """
        # First, compute the range of lengths that would keep the avg length within a distance of at most 0.5 of the imposed average length
        L = globalz.PARAMS.hard_constraint_on_avg_morph_length
        n = sum(self.length_histogram.values()) if len(self.length_histogram)>0 else 0
        sum_of_lengths = sum(map(lambda l: l*self.length_histogram[l], self.length_histogram)) if len(self.length_histogram)>0 else 0
        lmin = max(1,int(math.ceil((n+1)*(L-0.5)-sum_of_lengths)))
        lmax = max(1,int(math.floor((n+1)*(L+0.5)-sum_of_lengths)))
        """
        # Compute "target" length which will be used as the mean of the poisson distribution
        L = globalz.PARAMS.hard_constraint_on_avg_morph_length
        n = sum(self.length_histogram.values()) if len(self.length_histogram)>0 else 0
        sum_of_lengths = sum(map(lambda l: l*self.length_histogram[l], self.length_histogram)) if len(self.length_histogram)>0 else 0
        lmean = max(1,(n+1)*float(L)-sum_of_lengths)

        poisson_distance = lambda m,x: np.exp(-m)*reduce(lambda a,b: a*b, map(lambda z: m/z, range(1,x+1))) # m (mean of poisson law) must be a float, and x (tested value) must be an int

        #<DEBUG>
        print "Length histogram:",self.length_histogram
        print "Target length:",lmean
        S_ = 0.
        Z_ = 0.
        for l in self.length_histogram:
            S_ += self.length_histogram[l]*l
            Z_ += self.length_histogram[l]
        if(len(self.length_histogram)>0):
           print "AVG MORPH TYPE LENGTH:",(S_/Z_)
        #</DEBUG>

        return (lambda l: poisson_distance(lmean,l))

        """
        # Then, for each allowed length, compute the poisson probability of this length (which will then be normalized over all allowed lengths)
        unnormalized_p = dict()
        Z = 0
        for l in range(lmin, lmax+1):
            unnormalized_p[l] = self.poisson.P(l)
            Z += unnormalized_p[l]
            if l>L and unnormalized_p[l]==0.:
                break
        """

        """
        # <DEBUG>
        print "ALLOWED LENGTH DISTRIBUTION"
        for l in unnormalized_p:
            print l,":",unnormalized_p[l]/Z
        S_ = 0.
        Z_ = 0.
        for l in self.length_histogram:
            S_ += self.length_histogram[l]*l
            Z_ += self.length_histogram[l]
        if(len(self.length_histogram)>0):
           print "AVG MORPH TYPE LENGTH:",(S_/Z_)
        # </DEBUG>
        """

        """
        # Return a lambda function that represents the resulting distribution
        return (lambda l: unnormalized_p[l]/Z if l in unnormalized_p else 0.)
        """
        
    def P(self,seq):
        pf = (self.initial,)
        log_p = 0.
        for x in seq:
            px = self.hpyp(self.T(pf)).P(x)
            if px==0.:
                return 0.
            log_p += np.log(px)
            pf = pf+(x,)
        if self.hyperparameters.length_constraint:
            log_p += (-self.hyperparameters.length + len(seq)*np.log(self.hyperparameters.length) - sum(map(np.log, range(2,len(pf)))))
        elif globalz.PARAMS.hard_constraint_on_avg_morph_length:
            log_p += np.log(self.length_distribution(len(seq)))
        else:
            log_p += np.log(self.hpyp(self.T(pf)).P(self.final))
        return np.exp(log_p)

    def sample(self):
        seq = (self.initial,)
        x = 0
        if self.hyperparameters.length_constraint:
            l = poisson(self.hyperparameters.length).sample()
        elif globalz.PARAMS.hard_constraint_on_avg_morph_length:
            x = np.random.random()
            S = 0.
            l = 0
            while S<x:
                l += 1
                S += self.length_distribution(l)
        while ((self.hyperparameters.length_constraint or globalz.PARAMS.hard_constraint_on_avg_morph_length)and (len(seq)<(l+1))) or ((not self.hyperparameters.length_constraint) and x!=self.final):
            x = self.hpyp(self.T(seq)).sample()
            seq = seq+(x,)
        if self.hyperparameters.length_constraint or globalz.PARAMS.hard_constraint_on_avg_morph_length:
            return reduce(lambda a,b : a+b, seq[1:]) if len(seq)>1 else ""
        else:
            return reduce(lambda a,b : a+b, seq[1:-1]) if len(seq)>2 else ""
            

class LanguageModel:
    """
    This class describes a language model
    """
    def __init__(self, N=2, base_class=InfiniteGram, base_hyperparameters=dict(), pyp_hyperparameters=dict()):
        self.T = ( lambda pf: pf[-N+1:] ) if N>1 else ( lambda pf: tuple() )
        self.hpyp = CacheHierarchy(
            model_classes = lambda pf: (base_class if pf==None else PYP),
            backoff = lambda pf: (pf[1:] if (pf!=None and len(pf)>=1) else None),
            hyperparameters_hash = lambda pf: len(pf) if pf!=None else None,
            hyperparameters = lambda pf: base_hyperparameters if pf==None else pyp_hyperparameters
            )
        self.initial = lambda : 0
        self.final = lambda : 1
        self.N = N
        self.hpyp(tuple()).add_to_filter(self.final)

        self.info = {"Markov order of sequence model" : N,
                     "Base class" : base_class,
                     "Base hyperparameters" : base_hyperparameters,
                     "PYP hyperparameters" : pyp_hyperparameters}

    def __str__(self):
        return repr(self.info)

    def reset(self):
        self.hpyp.reset()

    def resample_hyperparameters(self):
        self.hpyp.resample_hyperparameters()

    def cache(self,seq):
        pf = (self.initial,)
        for x in seq:
            self.hpyp(self.T(pf)).cache(x)
            pf = pf+(x,)
        self.hpyp(self.T(pf)).cache(self.final)

    def uncache(self,seq):
        pf = (self.initial,)
        for x in seq:
            self.hpyp(self.T(pf)).uncache(x)
            pf = pf+(x,)
        self.hpyp(self.T(pf)).uncache(self.final)

    def P(self,seq):
        pf = (self.initial,)
        log_p = 0.
        for x in seq:
            px = self.hpyp(self.T(pf)).P(x)
            if px==0.:
                return 0.
            log_p += np.log(px)
            pf = pf+(x,)
        log_p += np.log(self.hpyp(self.T(pf)).P(self.final))
        return np.exp(log_p)

    def sample(self):
        seq = (self.initial,)
        x = 0
        while x!=self.final:
            x = self.hpyp(self.T(seq)).sample()
            seq = seq+(x,)
        return seq[1:-1] 

    @staticmethod
    def ordered_tuples(L,I,J,reverse=False):
        """
        A generator enumerating all ordered tuples of length L ranging between integers I and J
        """
        if reverse:
            if L==1:
                for k in range(I,J-1,-1):
                    yield (k,)
            else:
                for k in range(I-L+1, J-1,-1):
                    for t in LanguageModel.ordered_tuples(L-1,I,k+1,reverse=True):
                        yield (k,)+t            
        else:
            if L==1:
                for k in range(I,J+1):
                    yield (k,)
            else:
                for k in range(I+L-1, J+1):
                    for t in LanguageModel.ordered_tuples(L-1,I,k-1):
                        yield t+(k,)

    @staticmethod
    def ordered_tuples_up_to(L,I,J,reverse=False):
        """
        A generator enumerating all ordered tuples of length up to L ranging between integers I and J
        """
        for _L in range(1,L+1):
            for t in LanguageModel.ordered_tuples(_L,I,J,reverse=reverse):
                yield t

    def fetch_tuple_alpha(self,t,S):
        i = t[0]
        l = list()
        for s in range(1,len(t)):
            l.append(S[i:t[s]])
            i = t[s]
        if len(t)<self.N and t[0]==0:
            return (self.initial,)+tuple(l)
        else:
            return tuple(l)



    """
    def alpha(self,S,max_segment_length=None):
        #Computes alpha-probabilities for string S
        A = dict()
        N = len(S)

        fetch_tuple = lambda t: self.fetch_tuple_alpha(t,S)
        X = lambda x,y: (y if max_segment_length==None else min(x+max_segment_length, y))

        # Initialisation
        A[(0,)] = 1.
        for l in range(self.N-1):
            tuple_list = A.keys()
            for t in tuple_list:
                for k in range(t[-1]+1, X(t[-1],N)+1):
                    A[t+(k,)] = A[t]*self.hpyp(fetch_tuple(t)).P(S[t[-1]:k])

        # Recurrence
        tuple_list = filter(lambda t: len(t)==self.N, A.keys())
        for t in tuple_list:
            for k in range(t[-1]+1, X(t[-1],N)+1):
                A[(t+(k,))[1:]] = A[t]*self.hpyp(fetch_tuple(t)).P(S[t[-1]:k])

        return A

    def beta(self,S,max_segment_length=None):
        #Computes beta-probabilities for string S
        B = dict()
        N = len(S)

        fetch_tuple = lambda t: self.fetch_tuple_alpha(t,S)        
        X = lambda x,y: (x if max_segment_length==None else max(y-max_segment_length,x))

        #Initialisation (tuples se terminant par la position finale)
        B[(N,)] = 0. if self.N>1 else self.hpyp(tuple()).P(self.final)
        for l in range(self.N-1):
            tuple_list = B.keys()
            for t in tuple_list:
                for k in range(X(0,t[0]),t[0]):
                    B[(k,)+t] = 0. if ((k>0) and l!=(self.N-2)) else self.hpyp(fetch_tuple((k,)+t)).P(self.final)

        #Recurrence
        tuple_list = filter(lambda t: len(t)==self.N, B.keys())
        for t in tuple_list:
            for k in range(X(0,t[0]),t[0]):
                t_ = ((k,)+t)[:-1]
                B[t_] = B[t]*self.hpyp(fetch_tuple(t_)).P(S[t_[-1]:t[-1]])

        return B
    """

    def alpha(self,S,max_segment_length=None):
        memo = dict()
        fetch_tuple = lambda t: self.fetch_tuple_alpha(t,S)
        X = lambda x,y: (x if max_segment_length==None else max(y-max_segment_length,x))
        def A(t):
            if t in memo:
                return memo[t]
            else:
                if t[0]==0:
                    if len(t)==1:
                        memo[t] = 1.
                        return 1.
                    else:
                        res = A(t[:-1])*self.hpyp(fetch_tuple(t[:-1])).P(S[t[-2]:t[-1]])
                        memo[t] = res
                        return res
                else:
                    res = 0.
                    for k in range(X(0,t[0]),t[0]):
                        t_ = ((k,)+t)[:-1]
                        res += A(t_)*self.hpyp(fetch_tuple(t_)).P(S[t_[-1]:t[-1]])
                    memo[t] = res
                    return res
        return A

    def beta(self,S,max_segment_length=None):
        memo = dict()
        fetch_tuple = lambda t: self.fetch_tuple_alpha(t,S)
        X = lambda x,y: (y if max_segment_length==None else min(x+max_segment_length,y))
        L = len(S)
        def B(t):
            if t in memo:
                return memo[t]
            else:
                if t[-1]==L:
                    res = self.hpyp(fetch_tuple(t)).P(self.final)
                    memo[t] = res
                    return res
                else:
                    if len(t)==self.N:
                        res = 0.
                        for k in range(t[-1]+1,X(t[-1],L+1)):
                            t_ = (t+(k,))[1:]
                            res += B(t_)*self.hpyp(fetch_tuple(t)).P(S[t[-1]:k])
                        memo[t] = res
                        return res
                    else:
                        res = 0.
                        for k in range(t[-1]+1,X(t[-1],L+1)):
                            t_ = (t+(k,))
                            res += B(t_)*self.hpyp(fetch_tuple(t)).P(S[t[-1]:k])
                        memo[t] = res
                        return res
        return B




    def segment(self,S,sampled=True,for_eval=False,max_segment_length=None,compute_likelihood=False):
        """
        Samples a segmentation for string 'S'
        """
        splits = (0,)
        segments = tuple()
        pos = 0
        L = len(S)
        A,B = self.alpha(S,max_segment_length=max_segment_length), self.beta(S,max_segment_length=max_segment_length)

        def fetch_tuple(t):
            i = t[0]
            l = list()
            for s in range(1,len(t)):
                l.append(S[i:t[s]])
                i = t[s]
            return tuple(l)

        max_len = L if max_segment_length==None else max_segment_length
        while pos!=L:
            p = dict()
            Z = 0.
            for k in range(pos+1,min(pos+max_len+1,L+1)):
                p[k] = A(splits[-self.N:])*self.hpyp(segments[-self.N+1:] if self.N>1 else tuple()).P(S[pos:k])*B((splits+(k,))[-self.N:])
                Z += p[k]
            if Z!=0.:
                pos_next = sample(p, lambda k: p[k]/Z, normalized=True) if sampled else max(p, key = lambda k:p[k])
            else:
                #print "WARNING : a zero probability was encountered during the sampling of a segmentation. By default, a random segmentation is returned"
                for p_n in range(pos+1,L+1):
                    pos_next = p_n
                    if np.random.random()<0.3:
                        break
            splits = splits+(pos_next,)
            segments = segments+(S[pos:pos_next],)
            pos = pos_next
        return (segments, np.log(self.P(segments))) if compute_likelihood else segments

    def marginal_logP(self,S,max_segment_length=None):
        seg = self.segment(S, sampled=True, max_segment_length=max_segment_length)
        return np.log(self.P(seg))
        
        






"""
lm = LanguageModel(N=3,base_hyperparameters = {'share_across_lengths' : False,
                                           'maximum_markov_order' : 3})
text = "bonjour ceci est un test du infinite gram model j'espere que ca va bien fonctionner la procedure igram permet d'apprendre un infinite gram model sur une chaine de caracteres et la procedure open vocabulary language model permet d apprendre un modele de langue d ordre N sur une liste de mots en faisant un backoff sur un infinite gram model pour conserver un vocabulaire ouvert".split()
lm.cache(text)
"""

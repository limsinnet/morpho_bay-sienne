import globalz
import sys
import numpy as np
import eval
from loadCorpus import loadCorpus_MBO_train, loadCorpus_MBO_test, loadCorpus_TUR_train, loadCorpus_TUR_test, loadCorpus_EN_train, loadCorpus_EN_test, loadCorpus_FIN_train, loadCorpus_FIN_test, wrap_trainSet
from tools import shuffle, test_convergence, mixture, exponential, gamma
from pitmanYor import *
from pitmanYor2 import *
from tone_character_model import *
from char_ngram import *
import argparse
import codecs
import time
from info import class_model_info

if sys.stdout.encoding != 'UTF-8':
    sys.stdout = codecs.getwriter('utf-8')(sys.stdout, 'strict')
if sys.stderr.encoding != 'UTF-8':
    sys.stderr = codecs.getwriter('utf-8')(sys.stderr, 'strict')

corpuses_train = loadCorpus_EN_train, loadCorpus_TUR_train, loadCorpus_FIN_train, loadCorpus_MBO_train
corpuses_test = loadCorpus_EN_test, loadCorpus_TUR_test, loadCorpus_FIN_test, loadCorpus_MBO_test





parser = argparse.ArgumentParser(description = "PYP-SHMM for morphological segmentation")
parser.add_argument('-T','--train', help="Training corpus", type=str)
parser.add_argument('-t','--test', help="Test corpus (by default: same as train corpus. Use a float to use a sample of the train corpus. For example '-t 0.2' will use a sample of 20%% of the corpus as test set)", type=str)
parser.add_argument('--corpus_annealing', help="For corpuses where type/token counts are distinguished, annealing coefficient between type vs token sampling (ex: 0. = type sampling, 1. = token sampling)", type=float)
parser.add_argument('-N','--morph_order', help="Order of the morph sequence model (default: unigram model)", type=int)
parser.add_argument('-n','--char_order', help="Order of the character sequence model (default: infinite-gram model)", type=int)
parser.add_argument('-y','--hyperparameter_resampling', help="Resample hyperparameters every x examples", type=int)
parser.add_argument('-e','--eval_every', help="Evaluate against test every x examples", type=int)
parser.add_argument('-l','--length_constraint_prior', help="(couple of floats): parameters on the gamma prior on morpheme length. By default, will be set to 200.,40. Use '-l 0' to disable length constraint", type=str)
parser.add_argument('-C','--classes',help="Use class-based model. -C 0 : unlimited number of classes / -C n with n>0 : use a fixed number of classes", type=int)
parser.add_argument('-p','--precache_chars', help="Pre-cache 'n' samples of the corpus in character model", type=int)
parser.add_argument('-S','--save', help="Save model to path every 1000 iterations", type=str)
parser.add_argument('-L','--load', help="Load model from path (if present, all options regarding definition of model structure will be overridden)", type=str)
parser.add_argument('--tone_character_model', help="Use tone character model", action='store_true')
parser.add_argument('--charless_tone_character_model', help="Use charless tone character model", action="store_true")
parser.add_argument('--only_remember_last_tone_seen', help="Use tone character model where only the last tone seen will be remembered by the model", action="store_true")
parser.add_argument('-M','--max_segment_length', help="Maximum segment length (to speed up segmentation)", type=int)
parser.add_argument('-b','--burn_in',help="Burn-in during a certain number of iterations before starting to resample hyperparameters", type=int)
parser.add_argument('--N_test', help="The N_TEST first examples of the train corpus (as given by the option -T PATH) will be used as a test corpus. The train corpus given in PATH must therefore be a segmented corpus.", type=int)
parser.add_argument('--stop',type=str,help="Define a stop criterion:\n--stop iterationN : will stop training after N iterations\n--stop train_likelihood[value_window_ratio]: will stop when the approximate trainset likelihood has converged (ie: when all approximate trainset likelihoods of the last 15 percent of training time (and at least 100000 iterations) have not varied by more than a percentage of 'value_window_ratio' (by default: 0.05))\n--stop test_likelihood[value_window_ratio]\n--stop test_performance[value_window_ratio]")
parser.add_argument('--deadline',type=float,help="Stop training after DEADLINE hours")
parser.add_argument('--one_spelling_model_per_class', help="Use a separate spelling model for each class (only relevant when using the class-based model)", action="store_true")
parser.add_argument('--use_dirac_mixture_for_morph_theta_priors', help="The prior over the morph PYP's theta parameter will be a mixture of distributions designed to model three degrees of class 'openness'. Otherwise, an agnostic prior will be used.", action="store_true")
parser.add_argument('--hard_constraint_on_avg_morph_length', help="Use a hard constraint on average morph type length", type=float)
parser.add_argument('--simple_ngram_model', help="Use a simple ngram model learned on the train set as a spelling model", action="store_true")
parser.add_argument('--morph_theta_dirac', help="Use a dirac as hyperprior for theta in morph PYP. ex: --morph_pyp_theta_prior dirac(10.)", type=float)
parser.add_argument('--morph_discount_dirac', help="Use a dirac as hyperprior for discount in morph PYP", type=float)
parser.add_argument('--class_theta_dirac', help="Use a dirac as hyperprior for theta in class PYP", type=float)
parser.add_argument('--class_discount_dirac', help="Use a dirac as hyperprior for discount in class PYP", type=float)

globalz.PARAMS = parser.parse_args()
PARAMS = globalz.PARAMS

print PARAMS.save

if PARAMS.morph_order==None:
    PARAMS.morph_order = 2
if PARAMS.char_order==None:
    PARAMS.char_order = None

PRINT_INFO = not((PARAMS.train==None) and (PARAMS.test!=None))

STOP_CRITERION = None
if PARAMS.stop!=None:
    stop_criteria = ['iteration','train_likelihood','test_likelihood','test_performance']
    for c in stop_criteria:
        if PARAMS.stop[:len(c)]==c:
            STOP_CRITERION = c
    if STOP_CRITERION==None:
        print "Stop criterion: Parse Error"
        exit(0)
    if len(PARAMS.stop)==len(STOP_CRITERION):
        if STOP_CRITERION=='iteration':
            print "Stop criterion: Parse Error"
            exit(0)
        else:
            STOP_ARG = 0.05
    else:
        STOP_ARG = int(PARAMS.stop[len(STOP_CRITERION):]) if STOP_CRITERION=='iteration' else float(PARAMS.stop[len(STOP_CRITERION):])
    print "Stop criterion: "+STOP_CRITERION+" "+str(STOP_ARG)


if PARAMS.save!=None or PARAMS.load!=None:
    import dill

    def save(morphModel,path):
        if PRINT_INFO:
            print "Saving "+repr(morphModel)+" at "+path
        f = open(path,"wb")
        p = dill.Pickler(f)
        p.dump(morphModel)
        f.close()

    def load(path):
        if PRINT_INFO:
            print "Loading model at "+path
        f = open(path, "rb")
        p = dill.Unpickler(f)
        morphModel = p.load()
        f.close()
        return morphModel



if PARAMS.hard_constraint_on_avg_morph_length!=None and PARAMS.length_constraint_prior!=None:
    print "Inconsistency in parameters: cannot use both a hard constraint on avg morph length and a prior on a soft length constraint"
    exit()

if PARAMS.length_constraint_prior==None:
    if PARAMS.hard_constraint_on_avg_morph_length==None:
        PARAMS.length_constraint_prior = (400.,40.)
else:
    l = PARAMS.length_constraint_prior.split(',')
    l = tuple(map(float,l))
    if l==(0.,):
        PARAMS.length_constraint_prior = None
    else:
        PARAMS.length_constraint_prior = l


PARAMS.resample_table_layout_after_each_hyperparameter_resampling = True

if PRINT_INFO:
    print "PARAMS = "+str(PARAMS)

# Initialize model

charModel_class = ToneCharacterModel if (PARAMS.tone_character_model or PARAMS.charless_tone_character_model or PARAMS.only_remember_last_tone_seen) else CharNGram if PARAMS.simple_ngram_model else InfiniteGram

if PARAMS.load==None:
    base_hp = ( {'order' : PARAMS.char_order} if PARAMS.simple_ngram_model
                else 
                {'maximum_markov_order' : PARAMS.char_order,
                 'share_across_lengths' : True,
                 'length_constraint_gamma_prior' : PARAMS.length_constraint_prior } )
    if PARAMS.charless_tone_character_model:
        base_hp['no_chars'] = True
    if PARAMS.only_remember_last_tone_seen:
        base_hp['only_remember_last_tone_seen'] = True

    morph_theta_prior = exponential(1.)
    morph_discount_prior = beta(1.,5.)
    if PARAMS.morph_theta_dirac != None:
        morph_theta_prior = dirac(PARAMS.morph_theta_dirac)
    if PARAMS.morph_discount_dirac != None:
        morph_discount_prior = dirac(PARAMS.morph_discount_dirac)

    if PARAMS.use_dirac_mixture_for_morph_theta_priors:
        morph_theta_prior = mixture([dirac(1.),dirac(10.),dirac(100.),dirac(1000.)]) #mixture([gamma(5.,2.), gamma(25.,4.), gamma(100.,10.)])
        shared_theta_prior = dirac(10.) if PARAMS.morph_theta_dirac==None else dirac(PARAMS.morph_theta_dirac)
    else:
        shared_theta_prior = morph_theta_prior
    shared_discount_prior = morph_discount_prior

    if PARAMS.classes!=None:
        class_theta_prior = exponential(1.)
        class_discount_prior = beta(1.,5.)
        if PARAMS.class_theta_dirac != None:
            class_theta_prior = dirac(PARAMS.class_theta_dirac)
        if PARAMS.class_discount_dirac != None:
            class_discount_prior = dirac(PARAMS.class_discount_dirac)
        morphModel = ClassLanguageModel(N = PARAMS.morph_order,
                                        base_class = charModel_class,
                                        base_hyperparameters = base_hp,
                                        class_pyp_hyperparameters = {'theta_prior' : class_theta_prior,
                                                                     'discount_prior' : class_discount_prior},
                                        morph_pyp_hyperparameters = {'theta_prior' : morph_theta_prior,
                                                                     'discount_prior' : morph_discount_prior},
                                        shared_pyp_hyperparameters = {'theta_prior' : shared_theta_prior,
                                                                      'discount_prior' : shared_discount_prior},
                                        n_classes = PARAMS.classes,
                                        one_spelling_model_per_class = PARAMS.one_spelling_model_per_class
                                    )
        charModel = morphModel.morph_hpyp(None)
    else:
        morphModel = LanguageModel(N = PARAMS.morph_order,
                                   base_class = charModel_class,
                                   base_hyperparameters = base_hp,
                                   pyp_hyperparameters = {'theta_prior' : morph_theta_prior,
                                                          'discount_prior' : morph_discount_prior})
        charModel = morphModel.hpyp(None)
else:
    morphModel = load(PARAMS.load)
    charModel = (morphModel.hpyp(None) if morphModel.__class__==LanguageModel else morphModel.morph_hpyp(None) if morphModel.__class__==ClassLanguageModel else None)

if PRINT_INFO:
    print "morphModel = "+str(morphModel)

train,test = None,None

# Initialize train set
segmented_train = None
if PARAMS.train==None:
    train = None
else:
    if PARAMS.train.isdigit():
        annealing = 1. if PARAMS.corpus_annealing==None else PARAMS.corpus_annealing
        train, test = corpuses_train[int(PARAMS.train)](annealing=annealing), corpuses_test[int(PARAMS.train)]()
    else:
        f = codecs.open(PARAMS.train,"r",encoding="utf-8")
        lines = f.read().splitlines()
        test = map(lambda s: [reduce(lambda a,b: a+b, map(lambda c: '' if c==' ' else c,s)), [s.split()]], lines)
        lines = map(lambda s: reduce(lambda a,b: a+b, map(lambda c: '' if c==' ' else c, s)), lines)
        trainset = zip([1]*len(lines),lines)
        train = wrap_trainSet(trainset, keep_order=True)

# Initialize test set

if PARAMS.test!=None:
    if PARAMS.test.isdigit():
        test = corpuses_test[int(PARAMS.test)]()
    elif len(PARAMS.test.split("."))==2 and PARAMS.test.split(".")[0].isdigit() and PARAMS.test.split(".")[1].isdigit():
        ratio = float(PARAMS.test)
        print "A sample of "+str(ratio*100)+"% of the corpus will be used as test set"
        test = filter(lambda _: np.random.random()<ratio, test)
    else:
        f = codecs.open(PARAMS.test,"r",encoding="utf-8")
        lines = f.read().splitlines()
        test = list()
        for l in lines:
            test.append([reduce(lambda a,b : a+b, map(lambda c: '' if c==' ' else c, l)), [l.split()]])

if PARAMS.N_test!=None and PARAMS.train!=None and not PARAMS.train.isdigit():
    segmented_train = test
    test = test[:PARAMS.N_test]



if PRINT_INFO:
    if train!=None:
        print "Training set: "+str(train.token_length())+" tokens, "+str(train.type_length())+" types"
    if test!=None:
        print "Test set: "+str(len(test))+" words"


# Precache chars
if PARAMS.load==None and PARAMS.simple_ngram_model:
    print "Computing character model..."
    train_gen = train()
    for i in range(train.token_length()):
        charModel.cache(next(train_gen)[0])


if PARAMS.load==None and PARAMS.precache_chars!=None:
    print "Precaching "+str(PARAMS.precache_chars)+" samples of the corpus in character model..."
    train_gen = train()
    for i in range(PARAMS.precache_chars):
        charModel.cache(next(train_gen),dont_cache_length=True)





epochs = 0
iteration = 0
previous_segmentations = dict()
eval_records = dict()
prf_records = dict()

def print_segmentation(seg):
    s = ""
    for segment in seg:
        s += segment+" "
    # return s[:-1].encode('utf-8','ignore')
    return s[:-1]

def print_class_segmentation(seg):
    cls,mph = seg
    s = ""
    for i in range(len(cls)):
        s += mph[i]+"["+str(cls[i])+"] "
    # return s[:-1].decode('utf-8','ignore')
    return s[:-1]

print_analysis = print_class_segmentation if PARAMS.classes!=None else print_segmentation
segmenter = (lambda s: morphModel.segment(s,sampled=False,for_eval=True,max_segment_length=PARAMS.max_segment_length))


if train !=None:
    train_generator = train()
    start_time = time.time()

    likelihoods = dict(map(lambda i: (i,-np.infty),range(train.token_length())))
    approx_likelihood_records_on_train, approx_likelihood_records_on_test = dict(), dict()
    approx_performance_records_on_train, approx_performance_records_on_test = dict(), dict()

    while True:
        example, example_id = next(train_generator)

        if example_id in previous_segmentations:
            #try:
            morphModel.uncache(previous_segmentations[example_id])
            #except:
            #    pass

        segmented = morphModel.segment(example,max_segment_length=PARAMS.max_segment_length,compute_likelihood=False)

        # print (u"Epoch "+str(epochs)+u" / Iteration "+str(iteration)+u" : "+print_analysis(segmented)).encode('ascii','ignore')
        t = time.time()-start_time

        # print sout.encode('utf-8')

        morphModel.cache(segmented)

        likelihood = np.log(morphModel.P(segmented))

        sout =  u"Epoch " + str(epochs) + u" / Iteration " + str(iteration) + " / Time elapsed: "+ str(int(t/3600.))+"h"+str(int((t%3600)/60.))+"m"+str(int(t%60))+"s" + u" : " + print_analysis(segmented) + " (log-likelihood: "+str(likelihood)+")"
        print sout

        previous_segmentations[example_id] = segmented
        likelihoods[example_id] = likelihood

        iteration += 1
        # Hyperparameter resampling
        if PARAMS.hyperparameter_resampling!=None and iteration%PARAMS.hyperparameter_resampling==0 and (PARAMS.burn_in==None or iteration>=PARAMS.burn_in):
            print "<hyperparameter_resampling iteration="+str(iteration)+">"
            morphModel.resample_hyperparameters()
            print "</hyperparameter_resampling>"
            if PARAMS.resample_table_layout_after_each_hyperparameter_resampling:
                print "Resampling restaurant layouts..."
                previous_segmentation_list = previous_segmentations.values()
                shuffle(previous_segmentation_list)
                morphModel.reset()
                for seg in previous_segmentation_list:
                    morphModel.cache(seg)

        # Evaluation
        if test!=None and PARAMS.eval_every!=None and iteration%PARAMS.eval_every==0:
            p,r,f = eval.against_gold_segmentations(segmenter,
                                                    test,
                                                    VERBOSE = True)
            print "<eval iteration="+str(iteration)+">"
            print "Precision: "+str(p)+" - Recall: "+str(r)+" - F-measure: "+str(f)
            print "(Random baseline: "+str(eval.against_gold_segmentations(eval.random_baseline(test),test,VERBOSE=False))+" )"
            print "</eval>"
            eval_records[iteration] = f


        # Approximate performance and log-likelihood
        if iteration%50==0:
            print "<approximate iteration="+str(iteration)+">"
            # Approximate log-likelihood
            avg = lambda l : sum(l)/len(l) if len(l)>0 else -np.infty
            approx_train_likelihood = avg(likelihoods.values())
            approx_likelihood_records_on_train[iteration] = (approx_train_likelihood)
            print "Approximate average likelihood on train set: "+str(approx_train_likelihood)
            if PARAMS.N_test!=None and (iteration%len(train) < PARAMS.N_test):
                approx_test_likelihood = avg(map(lambda x: x[1], filter(lambda x: x[0]<PARAMS.N_test, likelihoods.items())))
                approx_likelihood_records_on_test[iteration] = (approx_test_likelihood)
                print "Approximate average likelihood on test set: "+str(approx_test_likelihood)

            # Approximate performance
            if segmented_train!=None:
                HIDs = map(lambda x: eval.compare_segmentations(x[1], segmented_train[x[0]][1][0]), previous_segmentations.items())

                H,I,D = reduce(lambda prv, hid: (prv[0]+hid[0], prv[1]+hid[1], prv[2]+hid[2]), HIDs)
                P,R,F = 100*float(H)/(H+I), 100*float(H)/(H+D), 100*2.*float(H)/(2*H+I+D)
                prf_records[iteration] = P,R,F
                print "Approximate P/R/F: "+str((P,R,F))
            print "</approximate>"


        # Info
        if iteration%100==0:
            print "<info iteration="+str(iteration)+">"
            # Segmentations rates
            n_cuts = 0
            n_chars = 0
            n_segments = 0
            n_examples = 0
            for ex_id in previous_segmentations:
                n_examples += 1
                seg = previous_segmentations[ex_id] if PARAMS.classes==None else previous_segmentations[ex_id][1]
                n_cuts += len(seg)-1
                n_chars += sum(map(lambda s: len(s), seg))
                n_segments += len(seg)
            print "Avg number of morphs per word: "+str(float(n_segments)/float(n_examples))
            print "Segmentation rate: "+str(float(n_cuts)/float(n_chars))
            print "----------"
            if PARAMS.classes!=None:
                class_model_info(morphModel)
            print "</info>"


        #Stop criteria
        if PARAMS.stop!=None or PARAMS.deadline!=None and iteration%10==0:
            stop = False
            
            if STOP_CRITERION=='iteration' and iteration>STOP_ARG:
                reason = "max number of iterations reached"
                stop = True

            if STOP_CRITERION=='train_likelihood' and test_convergence(approx_likelihood_records_on_train, min_time_window=100000, time_window_ratio=0.15, value_window_ratio=STOP_ARG):
                reason = "Trainset likelihood has converged"
                stop = True

            if STOP_CRITERION=='test_likelihood' and test_convergence(approx_likelihood_records_on_test, min_time_window=100000, time_window_ratio=0.15, value_window_ratio=STOP_ARG):
                reason = "Testset likelihood has converged"
                stop = True

            if STOP_CRITERION=='test_performance' and test_convergence(eval_records, min_time_window=(PARAMS.eval_every*20), time_window_ratio=0.15, value_window_ratio=STOP_ARG):
                reason = "Test performance has converged"
                stop = True

            if PARAMS.deadline!=None and (time.time()-start_time)/3600.>PARAMS.deadline:
                reason = "Deadline ("+str(PARAMS.deadline)+" hours) reached"
                stop = True

            if stop:
                print "Stop criterion was triggered ("+reason+")"
                if PARAMS.save!=None:
                    save(morphModel,PARAMS.save)
                break

        if iteration%len(train)==0:
            epochs += 1

        if iteration%1000==0 and PARAMS.save!=None:
            save(morphModel,PARAMS.save)


if test!=None:
    p,r,f = eval.against_gold_segmentations(segmenter, test, VERBOSE = False, print_seg=True)
    if PRINT_INFO:
        print "Precision: "+str(p)+" - Recall: "+str(r)+" - F-measure: "+str(f)
        print "(Random baseline: "+str(eval.against_gold_segmentations(eval.random_baseline(test),test))+" )"

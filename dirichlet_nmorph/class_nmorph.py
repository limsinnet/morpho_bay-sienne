"""
A nonparametric bayesian model for morphological segmentation.
"""

import sys
import codecs
import numpy as np
import eval
from loadCorpus import loadCorpus_MBO_train
from loadCorpus import loadCorpus_MBO_test
from loadCorpus import loadCorpus_TUR_train, loadCorpus_TUR_test, loadCorpus_EN_train, loadCorpus_EN_test, loadCorpus_FIN_train, loadCorpus_FIN_test
from tools import SparseDict,sample,pr,shuffle,print_once
from corpus import Alphabet, CharacterModel, MorphModel
from hpyp_class_lm import HPYP_class_LM
from entropy_split import entropy_split
from pitmanYor import *
import itertools
import argparse
if sys.stdout.encoding != 'UTF-8':
    sys.stdout = codecs.getwriter('utf-8')(sys.stdout, 'strict')
if sys.stderr.encoding != 'UTF-8':
    sys.stderr = codecs.getwriter('utf-8')(sys.stderr, 'strict')
import pickle


parser = argparse.ArgumentParser(description = "Class-based Pitman-Yor SHMM for morphological segmentation")
parser.add_argument('class_theta', help="Class sequence model concentration parameter", type=float)
parser.add_argument('shared_theta', help="Shared morph concentration parameter", type=float)
parser.add_argument('morph_theta', help="Class-conditional morph concentration parameter", type=float)
parser.add_argument('-N','--order', help="Order of the N-gram character model used as base distribution (by default, a trigram model will be used)", type=int)
parser.add_argument('-t','--trivialprecache', help="Precache trivial segmentations", action="store_true")
parser.add_argument('-e','--entropyprecache', help="Precache segmentations obtained from the entropy split heuristic", action="store_true")
parser.add_argument('-r','--randomprecache', help="Precache random segmentations", action='store_true')
parser.add_argument('-f','--path', help="Path to file: a list of sentences (resp. words) where word boundaries (resp. morph boundaries) are indicated by a whitespace and sentences (resp. words) are separated by a newline", type=str)
parser.add_argument('-y','--hyperparameters', help="Enable hyperparameter resampling: use an integer as argument to specify after how many iterations they will be resampled", type=int)
parser.add_argument('-s', '--save', help="Save model every 1000 iterations in a binary file (give path as argument)", type=str)
parser.add_argument('-l', '--load', help="Load a pickled model", type=str)
parser.add_argument('-S','--segment', help="Segments the word list given as argument (use with -l PATH to segment the word list using the model pickled at PATH).", type=str)
parser.add_argument('-v','--eval', help="Evaluate every N iterations", type=int)
parser.add_argument('-C','--charactermodel', help="Use old school character model", action='store_true')
PARAMS = parser.parse_args()


####################
# MODEL PARAMETERS #
####################
N = 2
MAX_MORPH_LENGTH = 10

####################
# MODEL DEFINITION #
####################

loadCorpus, loadTest = loadCorpus_FIN_train, loadCorpus_FIN_test


if PARAMS.path==None:
    print "Loading default corpus..."
    lexicon = loadCorpus()
else:
    if PARAMS.path.isdigit():
        corpus_id = int(PARAMS.path)
        trains = [loadCorpus_TUR_train, loadCorpus_EN_train, loadCorpus_FIN_train, loadCorpus_MBO_train]
        tests = [loadCorpus_TUR_test, loadCorpus_EN_test, loadCorpus_FIN_test, loadCorpus_MBO_test]
        loadCorpus = trains[corpus_id]
        loadTest = tests[corpus_id]
        lexicon = loadCorpus()
    else:
        print "Loading corpus at "+PARAMS.path
        f = open(PARAMS.path,'r')
        lines = f.read().splitlines()
        f.close()
        unsplit = map(lambda line: '#'+line.replace(' ','')+'#', lines)
        split = map(lambda line: [filter(lambda morph: len(morph)>0, ['#']+line.split(' ')+['#'])], lines)
        loadCorpus = lambda : unsplit
        test_corpus = list()
        for i in range(len(unsplit)):
            test_corpus.append([unsplit[i],split[i]])
        loadTest = lambda : test_corpus
        lexicon = loadCorpus()


lexicon_sample = list()
for i in range(50000):
    lexicon_sample.append(lexicon.next())

if PARAMS.order==None:
    if PARAMS.charactermodel:
        character_model = CharacterModel(lexicon_sample, N=3)
    else:
        character_model = MorphModel(lexicon_sample, loadTest(), N=3)
else:
    if PARAMS.charactermodel:
        character_model = CharacterModel(lexicon_sample, N=PARAMS.order)
    else:
        character_model = MorphModel(lexicon_sample, loadTest(), N=PARAMS.order)

if PARAMS.eval==None:
    EVAL_EVERY = 5000
else:
    EVAL_EVERY = PARAMS.eval


            
def P0_string(string):
    """
    A base probability for generating a string of characters.
    The probability is the one computed by a unigram model fitting the type lexicon.
    """
    #res = 1.
    #for c in string:
    #    res *= (char_freqs[c]/char_freqsZ)
    #return res
    return np.exp(character_model.logP(string))
    #return (string_pool[string]/string_pool_Z) if (string in string_pool) else (0.)


class ModelState:
    """
    A class describing a state of the model, ie: for each word of the lexicon, some parse of the word into a succession of morphological states
    """
    def __init__(self):
        # Initial model:
        # There is one state for all words. The i-th word is parsed as follows: 0(initial state) => 2 => 1(final state).
        #
        # Parses are represented as a couple (state_sequence, morph_sequence)
        # For example consider the following parse:
        #     # + # + ab + ge + trunk + en + #
        #     0 ->0 ->2 -> 2  ->  4 - > 3 -> 1
        # It will be represented by the following couple:
        # ( state_sequence = [0,0,2,2,4,3,1]
        #   morph_sequence = ['#','#','ab','ge','trunk','en','#'] )
        
        if PARAMS.hyperparameters==None:
            self.LM = HPYP_class_LM(N,P0_string,
                                    CLASS_THETA = dirac(PARAMS.class_theta),
                                    SHARED_THETA = dirac(PARAMS.shared_theta),
                                    MORPH_THETA = dirac(PARAMS.morph_theta))

        else:
            self.LM = HPYP_class_LM(N,P0_string,
                                    CLASS_THETA = exponential(PARAMS.class_theta),
                                    SHARED_THETA = exponential(PARAMS.shared_theta),
                                    MORPH_THETA = exponential(PARAMS.morph_theta))


        self.parses = list()

        # PRE CACHING TRIVIAL SEGMENTATIONS
        if PARAMS.trivialprecache:
            self.parses = map(lambda i: ([0, 2, 1], ['#', lexicon_sample[i][1:-1], '#']),
                              range(len(lexicon_sample)))
            print_once("TRIVIAL SEGMENTATIONS PRE CACHED")

        # PRE CACHING RANDOM SEGMENTATIONS
        if PARAMS.randomprecache:
            self.parses = list()
            for word in lexicon_sample:
                classes = [0]
                morphs = ['#']
                i = 1
                for j in range(2,len(word)-1):
                    if np.random.random()<0.41:
                        classes.append(np.random.randint(2,5))
                        morphs.append(word[i:j])
                        i = j
                classes.append(np.random.randint(2,5))
                morphs.append(word[i:j])
                classes.append(1)
                morphs.append('#')
                self.parses.append((classes,morphs))
            print_once("RANDOM SEGMENTATIONS WITH 5 MORPH CLASSES (INITIAL + FINAL + 3 OTHER CLASSES) PRE CACHED")
        

        # PRE CACHING ENTROPY SPLIT HEURISTIC
        if PARAMS.entropyprecache:
            self.parses = list()
            splits = entropy_split(lexicon_sample)
            for split in splits:
                classes = map(lambda _ : np.random.randint(2,5), split)
                classes[0] = 0
                classes[-1] = 1
                self.parses.append((classes,split))
            print_once("SEGMENTATIONS INITIALIZED WITH THE ENTROPY PEAK HEURISTIC")

        if len(self.parses)==0:
            print_once("NO SEGMENTATIONS PRE CACHED")            


        print "Caching "+str(len(self.parses))+" initial parses..."
        for parse in self.parses:
            self.LM.cache(parse)
        print "DONE"

        parses_ = dict()
        for p in self.parses:
            parses_[reduce(lambda a,b: a+b, p[1])] = p[1]
            
        self.parses = parses_


    @staticmethod
    def repr_parse(parse):
        morphs = ""
        states = ""
        state_seq, morph_seq = parse
        for i in range(len(state_seq)):
            morph = morph_seq[i]
            state = str(state_seq[i])
            L = max(len(morph),len(state))
            morph += (" "*(1+L-len(morph)))
            state += (" "*(1+L-len(state)))
            morphs += morph
            states += state
        return morphs[:-1]+'\n'+states[:-1]
        

    def save(self,path):
        f = open(path,'wb')
        p = pickle.Pickler(f)
        self.LM.pickle(p)
        f.close()

    def load(self,path):
        f = open(path,'rb')
        p = pickle.Unpickler(f)
        self.LM.unpickle(p)
        f.close()


    def Print(self,Nlines=1000):
        i = 0
        for parse in self.parses:
            try:
                print (ModelState.repr_parse(parse)+'\n').encode('utf-8')
            except:
                pass
            i += 1
            if i>Nlines:
                break


def beta_probs(word,state,
               split_ratio_forcing,
               split_ratio):
    beta = dict()

    possible_prefixes = iter([])
    # TODO : Corriger la boucle suivante lorsque le mot est trop court
    for M in range(1,min(len(word)-1,N-1)):
        possible_prefixes = itertools.chain(possible_prefixes,
                                            itertools.product([0], *map(lambda _: state.LM.class_iterator(), range(M)))
                                            )

    if (len(word)-1)>=N:
        possible_prefixes = itertools.chain(possible_prefixes,
                                            itertools.product(*map(lambda _: state.LM.class_iterator(),
                                                                   range(N-1)))
                                            )
        

    for pre in possible_prefixes:
        beta[(len(word)-1, pre)] = state.LM.Psm(1,"#",pre)
        
    for i in range(len(word)-2,1,-1):
        possible_prefixes = iter([])
        if i>=N:
            possible_prefixes = itertools.chain(possible_prefixes,
                                                itertools.product(*map(lambda _: state.LM.class_iterator(), range(N-1)))
                                                )
        for M in range(1,min(i,N-1)):
            possible_prefixes = itertools.chain(possible_prefixes,
                                                itertools.product([0], *map(lambda _: state.LM.class_iterator(), range(M)))
                                                )

        for pre in possible_prefixes:
            beta[(i,pre)] = np.array(0.)
            for j in range(i+1,min(i+MAX_MORPH_LENGTH+1,len(word))):
                for c in state.LM.class_iterator():
                    if split_ratio_forcing:
                        beta[(i,pre)] += state.LM.Psm(c, word[i:j], pre) * beta[(j,(pre+(c,))[-N+1:])] * ( (np.power(1.-split_ratio, j-i-1) * split_ratio) )
                    else:
                        beta[(i,pre)] += state.LM.Psm(c, word[i:j], pre) * beta[(j,(pre+(c,))[-N+1:])]
    return beta
                                                                                                                                                                                        

def sample_parse_DP(word, state,
                    maximize = False,
                    annealing=1.,
                    split_ratio_forcing = False,
                    split_ratio = 0.41,
                    VERBOSE=False):
    """
    Samples a parse for 'word'
    If maximize==True, the highest-probability parse will be returned, else, the parse will be sampled.
    If split_ratio_forcing==True, the hypothesis probabilities will be modified in order to include a prior probability of splitting of 'split_ratio', ie: the number of split points will have a binomial prior
    """
    if split_ratio_forcing:
        print_once("SPLIT RATIO FORCING: "+str(split_ratio))
    else:
        print_once("NO SPLIT RATIO FORCING")

    # Compute beta-probabilities
    beta = beta_probs(word,state,
                      split_ratio_forcing=split_ratio_forcing, split_ratio=split_ratio)

    i = 1
    pre = (0,)
    state_seq = [0]
    morph_seq = ['#']
    while i!=(len(word)-1):
        H = dict()
        for j in range(i+1,min(i+1+MAX_MORPH_LENGTH,len(word))):
            for c in state.LM.class_iterator():
                if split_ratio_forcing:
                    H[(j,c)] = state.LM.Psm(c, word[i:j], pre) * beta[(j,(pre+(c,))[-N+1:])] * ( (np.power(1.-split_ratio, j-i-1) * split_ratio) )
                else:
                    H[(j,c)] = state.LM.Psm(c, word[i:j], pre) * beta[(j,(pre+(c,))[-N+1:])]
        if VERBOSE:
            for (j,c) in H:
                print "P( "+str(pre)+word[i:j]+" -> "+str(c)+" ) = "+str(H[(j,c)])
        if maximize:
            (j,c) = max(H, key=lambda x:H[x])
        else:
            (j,c) = sample(iterator = H,
                           P = lambda x: H[x],
                           normalized = False,
                           annealing = annealing)

        state_seq.append(c)
        morph_seq.append(word[i:j])

        i = j
        pre = (pre+(c,))[-N+1:]

    state_seq.append(1)
    morph_seq.append('#')
    return state_seq, morph_seq


def sample_parse_beam(word, state, beam_size=30, VERBOSE=False):

    # Initialize hypotheses
    hypotheses = [[
            { 'morphs' : ['#'],
              'states' : [0],
              'pre' : (0,),
              'pos' : 1,
              'logQ' : 0.,
              'logP' : 0. }
        ]]+map(lambda _: [], range(len(word)-1))

    completed_hypotheses = list()

    # Expand hypotheses
    while sum(map(lambda h: len(h), hypotheses))>0:
        # Pick a hypothesis
        H = reduce(lambda x,y:x+y,hypotheses)[0]
        if H['pos']==len(word)-1:
            # Complete hypothesis
            new_H = dict()
            new_H['morphs'] = H['morphs']+['#']
            new_H['states'] = H['states']+[1]
            new_H['logP'] = H['logP'] + np.log(state.LM.Psm(1,'#',H['pre']))
            completed_hypotheses.append(new_H)
        else:
            # Consider all hypothesis expansions
            pos = H['pos']
            for j in range(pos+1,len(word)):
                for s in state.LM.class_iterator():
                    new_H = dict()
                    new_H['morphs'] = H['morphs']+[word[pos:j]]
                    new_H['states'] = H['states']+[s]
                    new_pre = (H['pre']+(s,))[-N+1:]
                    new_H['pre'] = new_pre
                    new_H['pos'] = j
                    new_H['logP'] = H['logP'] + np.log( state.LM.Psm(s,word[pos:j],H['pre']))
                    hypotheses[j-1].append(new_H)
        hypotheses[H['pos']-1].remove(H)

        # Pruning
        for l in range(len(word)):
            hypotheses[l] = sorted(hypotheses[l],
                                   key = lambda H: H['logP'],
                                   reverse = True)[:beam_size]

    if VERBOSE:
        for H in completed_hypotheses:
            print H

    # Sample from completed hypotheses
    H = sample(iterator = completed_hypotheses,
               P = lambda H: np.exp(H['logP']),
               normalized = False)
    return H['states'], H['morphs']                          
            
          



def gibbsSample(state,
                hp_resampling_condition = (lambda it: (1+it)%PARAMS.hyperparameters==0),
                dump_every=5000,
                eval_procedure=(lambda : 0)):
    """
    Performs Gibbs sampling on the lexicon parses to learn the model
    """
    it = 0

    while True:

        if PARAMS.save!=None and (1+it)%1000==0:
            print "Saving model to "+PARAMS.save
            state.save(PARAMS.save)

        if PARAMS.hyperparameters!=None and hp_resampling_condition(it):
            print "Resampling hyper-parameters..."
            state.LM.sample_hyperparameters()

        if (1+it)%dump_every==0:
            state.LM.dump()

        if (1+it)%EVAL_EVERY==0:
            eval_procedure()

        print "Iteration: "+str(it)
        word = lexicon.next()
        if word in state.parses:
            # A parse for this word already exists: un-sample it
            state.LM.uncache(state.parses[word])
            
        new_parse = sample_parse_DP(word,state)
        state.LM.cache(new_parse)
                             
        if word in state.parses:
            # Override the old parse
            old_parse = state.parses[word]
            state.parses[word] = new_parse
            if new_parse!=old_parse:
                try:
                    print "Old parse:"
                    print ModelState.repr_parse(old_parse)
                    print "New parse:"
                    print ModelState.repr_parse(new_parse)
                    print ""
                except:
                    print new_parse
            else:
                try:
                    print word+" - unchanged"
                except:
                    pass
        else:
            # Add new parse
            state.parses[word] = new_parse
            try:
                print ModelState.repr_parse(new_parse)
            except:
                print new_parse
            print ""
        it += 1



def sample_form():
    return reduce(lambda a,b: a+b, s.LM.sample()[1])

def segment_form(word):
    return sample_parse_DP(word,s,maximize=True)[1]

s = ModelState()


class eval_on:
    def __init__(self,test_corpus):
        self.test_corpus = test_corpus
    
    def __call__(self):
        p,r,f = eval.against_gold_segmentations(segment_form, self.test_corpus)
        print "Precision: "+str(p)+" - Recall: "+str(r)+" - F-measure: "+str(f)
        print "(Random baseline: "+str(eval.against_gold_segmentations(eval.random_baseline(test_corpus),test_corpus))+" )"


print "Sequence theta prior: ",s.LM.class_hpyp.THETA_PRIOR," / discount prior: ",s.LM.class_hpyp.DISCOUNT_PRIOR
print "Morph theta prior: ",s.LM.morph_hpyp.THETA_PRIOR, " / discount prior: ",s.LM.morph_hpyp.DISCOUNT_PRIOR
print "Shared morph theta prior: ",s.LM.shared_morph_hpyp.THETA_PRIOR, " / discount prior: ",s.LM.shared_morph_hpyp.DISCOUNT_PRIOR


if PARAMS.load!=None:
    s.load(PARAMS.load)



test_corpus = loadTest()

if PARAMS.segment!=None:
    in_path = PARAMS.segment
    out_path_best = "out_bestParse.txt"
    out_path_sampled = "out_sampledParse.txt"
    outBest_ = open(out_path_best,"w")
    outSampled_ = open(out_path_sampled,"w")
    in_ = open(in_path,"r")
    while True:
        word = str(filter(lambda x: x!=" ",in_.readline()))
        if len(word)==0:
            in_.close()
            outBest_.close()
            outSampled_.close()
            exit()
        word = '#'+word[:-1]+'#'
        best = sample_parse_DP(word,s,maximize=True)[1]
        sampled = sample_parse_DP(word,s,maximize=False)[1]
        if len(best)==3:
            best = best[1]
        else:
            best = reduce(lambda a,b: a+' '+b, best[1:-1])
            
        if len(sampled)==3:
            sampled = sampled[1]
        else:
            sampled = reduce(lambda a,b: a+' '+b, sampled[1:-1])

        outBest_.write(best+'\n')
        outSampled_.write(sampled+'\n')
        print "Best parse: "+best+" - Sampled parse: "+sampled
else:
    gibbsSample(s,
                #            eval_procedure = lambda : 0)
                eval_procedure = eval_on(test_corpus))


    

"""
A module for plotting graphs in text-mode
"""

import numpy as np


class Plot:
    def __init__(self,
                 x_size=200,y_size=50,
                 x_min=-10.,x_max=10.):
        """
        Initializes a plot in a region of x_size*y_size characters. Values will range between x_min and x_max, and the scale of y will be automatically adjusted depending on the maxima and minima of functions
        """
        
        self.x_size = x_size
        self.y_size = y_size
        self.values = list()
        self.colors = list()
        self.logprob = list()
        self.x_min = x_min
        self.x_max = x_max
        self.y_ranges = list()
        self.x_step = (float(self.x_max)-float(self.x_min))/float(self.x_size)
        self.functions = list()

    def add(self, f,
            log_probability = False,
            color='#'):
        """
        Adds a function to the plot. 'f' must be a procedure taking a float as input and returning a float
        'color' must be a single character
        Set 'log_probability' to True if f is an unnormalized log-probability distribution
        """
        self.functions.append(f)
        self.logprob.append(log_probability)
        y_min, y_max = np.inf, -np.inf
        x_step = self.x_step
        vals = np.zeros((self.x_size,))
        x = self.x_min
        for i in range(self.x_size):
            vals[i] = f(x)
            x += x_step
        y_min = min(y_min, min(vals))
        y_max = max(y_max, max(vals))

        if log_probability:
            vals -= max(vals)
            vals = np.exp(vals)
            Z = vals.sum()
            vals /= Z

        self.values.append(vals)
        self.colors.append(color)
        self.y_ranges.append((y_min, y_max))

    def resize(self, x_min, x_max):
        colors = self.colors
        functions = self.functions
        logprob = self.logprob
        self.values,self.y_ranges, self.colors, self.functions, self.logprob = list(), list(), list(), list(), list()
        self.x_min, self.x_max = x_min, x_max
        self.x_step = (float(self.x_max)-float(self.x_min))/float(self.x_size)
        
        for i in range(len(functions)):
            self.add(f=functions[i],
                     log_probability = logprob[i],
                     color = colors[i])

            
        
    def plot(self,
             y_range = None):
        """
        Prints the plot
        Set y_range to a couple (y_min, y_max) to impose a range for y. Else, it will be automatically adjusted
        """        

        matrix = map(lambda _: map(lambda _: ' ', range(self.x_size)), range(self.y_size))
        x_step = self.x_step
        x_min, x_max = self.x_min, self.x_max
        x_size,y_size = self.x_size, self.y_size
        #y_min = sum(map(lambda r: r[0], self.y_ranges))/float(len(self.y_ranges))
        #y_max = sum(map(lambda r: r[1], self.y_ranges))/float(len(self.y_ranges))


        if y_range!=None:
            y_min, y_max = y_range
        else:
            y_values = reduce(lambda a,b:a+b, map(lambda v: list(v), self.values))
            y_values = sorted(y_values)
            y_min = min(y_values)
            y_max = max(y_values)


        y_step = (y_max-y_min)/float(self.y_size)


        for _x in range(self.x_size):
            for _y_ in range(self.y_size):
                _y = y_size-_y_-1
                c = ' '
                on_Xaxis, on_Yaxis = False, False
                if (x_min+(_x*x_step))<=0. and 0.<(x_min+((_x+1)*x_step)):
                    on_Yaxis = True
                if (y_min+(_y*y_step))<=0. and 0.<(y_min+((_y+1)*y_step)):
                    on_Xaxis = True
                if on_Xaxis:
                    c = '-'
                if on_Yaxis:
                    c = '|'
                if on_Xaxis and on_Yaxis:
                    c = '+'

                for f in range(len(self.colors)):
                    vals = self.values[f]
                    if (y_min+(_y*y_step))<=vals[_x] and vals[_x]<(y_min+((_y+1)*y_step)):
                        c = self.colors[f]

                matrix[_y_][_x] = c

        for line in matrix:
            print reduce(lambda a,b:a+b, line)

                    
                

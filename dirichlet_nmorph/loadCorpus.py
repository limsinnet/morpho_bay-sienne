import json
from corpus import Alphabet
from random import random
from tools import sample, cumulative_sample
import numpy as np

def loadCorpus_MBO_train(PATH="/vol/work/loser/data/bulb/mbochi.txt"):
    """
    Mbochi data without the segmentations
    """
    f = open(PATH,'r')
    lines = f.read().splitlines()
    f.close()
    dataset = map(lambda line: '#'+line.replace(' ','')+'#', lines)
    return dataset

def loadCorpus_MBO_test(PATH="/vol/work/loser/data/bulb/mbochi.txt"):
    """
    Mbochi data with segmentations
    """
    f = open(PATH,'r')
    lines = f.read().splitlines()
    f.close()
    unsplit = map(lambda line: '#'+line.replace(' ','')+'#', lines)
    split = map(lambda line: [filter(lambda morph: len(morph)>0, ['#']+line.split(' ')+['#'])], lines)
    return zip(unsplit, split)
                         

def loadCorpus_FIN_test(PATH="/vol/work/loser/data/morphoChallenge/goldstdsample.fin",
                         SAMPLE=1.):
    """
    Finnish MorphoChallenge 2005 test set
    """
    f = open(PATH,'r')
    lines = f.read().splitlines()
    f.close()
    lines_split_between_unsegmented_and_segmented = map(lambda x: x.split('\t'),
                                                        lines)
    lines_with_split_options = map(lambda x: [x[0], x[1].split(', ')],
                                   lines_split_between_unsegmented_and_segmented)
    lines_with_split_segmentation = map(lambda x: ['#'+x[0]+'#',
                                                   map(lambda opt: ['#']+opt.split(' ')+['#'],
                                                       x[1])],
                                        lines_with_split_options)
    sampled_dataset = filter(lambda _: random()<SAMPLE,
                             lines_with_split_segmentation)
    return sampled_dataset

def loadCorpus_FIN_train(PATH="/vol/work/loser/data/morphoChallenge/wordlist.fin",
                         SAMPLE=1.):
    """
    Turkish MorphoChallenge 2005 train set
    """
    f = open(PATH,'r')
    lines = f.read().splitlines()
    f.close()
    dataset = map(lambda line: '#'+line.split(' ')[1]+'#',
                  lines)
    sampled_dataset = filter(lambda _: random()<SAMPLE, dataset)
    return sampled_dataset

def loadCorpus_TUR_test(PATH="/vol/work/loser/data/morphoChallenge/goldstdsample2005.tur",
                         SAMPLE=1.):
    """
    Turkish MorphoChallenge 2005 test set
    """
    f = open(PATH,'r')
    lines = f.read().splitlines()
    f.close()
    lines_split_between_unsegmented_and_segmented = map(lambda x: x.split('\t'),
                                                        lines)
    lines_with_split_options = map(lambda x: [x[0], x[1].split(', ')],
                                   lines_split_between_unsegmented_and_segmented)
    lines_with_split_segmentation = map(lambda x: ['#'+x[0]+'#',
                                                   map(lambda opt: ['#']+opt.split(' ')+['#'],
                                                       x[1])],
                                        lines_with_split_options)
    sampled_dataset = filter(lambda _: random()<SAMPLE,
                             lines_with_split_segmentation)
    return sampled_dataset

def loadCorpus_TUR_train(PATH="/vol/work/loser/data/morphoChallenge/wordlist2005.tur",
                         SAMPLE=1.):
    """
    Turkish MorphoChallenge 2005 train set
    """
    f = open(PATH,'r')
    lines = f.read().splitlines()
    f.close()
    dataset = map(lambda line: '#'+line.split(' ')[1]+'#',
                  lines)
    sampled_dataset = filter(lambda _: random()<SAMPLE, dataset)
    return sampled_dataset

def loadCorpus_EN_test(PATH="/vol/work/loser/data/morphoChallenge/goldstdsample2005.eng",
                         SAMPLE=1.):
    """
    English MorphoChallenge 2005 test set
    """
    f = open(PATH,'r')
    lines = f.read().splitlines()
    f.close()
    lines_split_between_unsegmented_and_segmented = map(lambda x: x.split('\t'),
                                                        lines)
    lines_with_split_options = map(lambda x: [x[0], x[1].split(', ')],
                                   lines_split_between_unsegmented_and_segmented)
    lines_with_split_segmentation = map(lambda x: ['#'+x[0]+'#',
                                                   map(lambda opt: ['#']+opt.split(' ')+['#'],
                                                       x[1])],
                                        lines_with_split_options)
    sampled_dataset = filter(lambda _: random()<SAMPLE,
                             lines_with_split_segmentation)
    return sampled_dataset

def loadCorpus_EN_train(PATH="/vol/work/loser/data/morphoChallenge/wordlist2005.eng",
                         ANNEALING=0.5):
    """
    English MorphoChallenge 2005 train set
    """
    f = open(PATH,'r')
    lines = f.read().splitlines()
    f.close()

    type_list = list()
    Z = 0.

    for line in lines:
        spl = line.split(' ')
        q = np.power(float(spl[0]), ANNEALING)
        type_list.append((Z, '#'+spl[1]+'#'))
        Z += q

    """
    type_list = sorted(type_list,
                       key = lambda x: x[0],
                       reverse = True)
    """

    while True:
        yield cumulative_sample(type_list,
                                F = lambda x: x[0]/Z)[1]



    



def loadCorpus_DE_small(PATH="/vol/work/loser/tiger/data/tiger_test.json",
               SAMPLE=1.):
    """
    Loads the corpus at PATH, and returns its lexicon
    The parameter SAMPLE is used to sample some ration of the full corpus. 1 takes the whole corpus, 0.5 half of it, etc...
    """
    corpus = json.load(open(PATH,"rt"))
    corpus = filter(lambda _: random()<=SAMPLE, corpus)
    corpus = map(lambda w: w.lower(), reduce(lambda x,y:x+y, map(lambda s: s[0],corpus)))
    corpus = filter(lambda x: x.isalpha(), corpus)
    corpus = list(set(corpus))
    lexicon = map(lambda x: '#'+x+'#', corpus)
    lexicon = sorted(lexicon,
                     key = lambda w: len(w))
    return lexicon


def loadCorpus_DE_large(PATH="/vol/work/loser/tiger/data/tiger_train.json",
               SAMPLE=1.):
    """
    Loads the corpus at PATH, and returns its lexicon
    The parameter SAMPLE is used to sample some ration of the full corpus. 1 takes the whole corpus, 0.5 half of it, etc...
    """
    corpus = json.load(open(PATH,"rt"))
    corpus = filter(lambda _: random()<=SAMPLE, corpus)
    corpus = map(lambda w: w.lower(), reduce(lambda x,y:x+y, map(lambda s: s[0],corpus)))
    corpus = filter(lambda x: x.isalpha(), corpus)
    corpus = list(set(corpus))
    lexicon = map(lambda x: '#'+x+'#', corpus)
    lexicon = sorted(lexicon,
                     key = lambda w: len(w))
    return lexicon


def loadCorpus_DE_sentences_small(PATH="/vol/work/loser/tiger/data/tiger_test.json",
               SAMPLE=1.):
    """
    Loads the corpus at PATH, and returns its lexicon
    The parameter SAMPLE is used to sample some ration of the full corpus. 1 takes the whole corpus, 0.5 half of it, etc...
    """
    corpus = json.load(open(PATH,"rt"))
    corpus = filter(lambda _: random()<=SAMPLE, corpus)
    corpus = map(lambda sentence: sentence[0], corpus) # Leave out POS information
    corpus = filter(lambda sentence: len(sentence)>7, corpus) # Only keep sentences with at least 7 words
    corpus = map(lambda sentence: map(lambda word: word.lower(), sentence), corpus) # Convert to lower-case
    corpus = map(lambda sentence: reduce(lambda x,y: x+y, sentence), corpus) # Forget word boundaries
    corpus = sorted(corpus,
                    key = lambda sentence: len(sentence)) # Sort in increasing sentence length
    corpus = filter(lambda sentence: '#' not in sentence, corpus) # Remove sentences with padding symbols
    corpus = map(lambda sentence: '#'+sentence+'#', corpus) # Add padding symbols
    return corpus


def loadCorpus_DE_sentences_large(PATH="/vol/work/loser/tiger/data/tiger_train.json",
               SAMPLE=1.):
    """
    Loads the corpus at PATH, and returns its lexicon
    The parameter SAMPLE is used to sample some ration of the full corpus. 1 takes the whole corpus, 0.5 half of it, etc...
    """
    corpus = json.load(open(PATH,"rt"))
    corpus = filter(lambda _: random()<=SAMPLE, corpus)
    corpus = map(lambda sentence: sentence[0], corpus) # Leave out POS information
    corpus = filter(lambda sentence: len(sentence)>7, corpus) # Only keep sentences with at least 7 words
    corpus = map(lambda sentence: map(lambda word: word.lower(), sentence), corpus) # Convert to lower-case
    corpus = map(lambda sentence: reduce(lambda x,y: x+y, sentence), corpus) # Forget word boundaries
    corpus = sorted(corpus,
                    key = lambda sentence: len(sentence)) # Sort in increasing sentence length
    corpus = filter(lambda sentence: '#' not in sentence, corpus) # Remove sentences with padding symbols
    corpus = map(lambda sentence: '#'+sentence+'#', corpus) # Add padding symbols
    return corpus

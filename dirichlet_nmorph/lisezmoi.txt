(Pour une description exhaustive des paramètres qu'on peut passer au script, exécuter la commande:
python class_nmorph.py -h )


Pour apprendre le modèle, lancer par exemple la commande:

python class_ngram.py 1 5 5 -f /chemin/vers/mbochi_train.txt -s model.bin

Cette commande lancera l'apprentissage du modèle sur le fichier mbochi.txt (sans critère d'arrêt, il faudra tuer le processus à la main). Tous les 1000 exemples, le modèle courant sera sauvegardé dans le fichier binaire model.bin, et tous les 5000 exemples, la précision/rappel/F-mesure sur le corpus sera évaluée. (Pour y accéder, envoyer par exemple l'output sur un log, puis taper la commande: grep "F-measure" mon_log.log )
Les nombres "1 5 5" signifient que les paramètres de concentration respectifs du modèle de classes, du modèle de morphes partagé, et du modèle de morphes conditionné à la classe, seront soumis à un a priori exponentiel de paramètres respectifs 1., 5. et 5.



Pour évaluer le modèle sur le corpus, lancer par exemple la commande:

python class_ngram.py 1 5 5 -f /chemin/vers/mbochi_train.txt -l model.bin -S /chemin/vers/mbochi_test.txt

Attention il faut fournir en argument à "-f" le chemin vers le corpus d'apprentissage.
Même si dans mbochi_test.txt, les limites de morphèmes sont présentes sous forme d'espace, le script les oubliera avant de produire sa propre segmentation. C'est-à-dire que en pratique, tu peux taper la commande:

python class_ngram.py 1 5 5 -f /chemin/vers/mbochi.txt -l model.bin -S /chemin/vers/mbochi.txt


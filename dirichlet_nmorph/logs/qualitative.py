import argparse
import re
import numpy as np

parser = argparse.ArgumentParser(description="")
parser.add_argument('path', help="Path to log", type=str)
parser.add_argument('-n','--nexamples', help="Load only n examples to avoid having to browse the whole log file", type=int)
_args = parser.parse_args()
path = _args.path
loadN = _args.nexamples

print "Opening log file..."
log = open(path,'r')
print "DONE"

# Search number of examples
_n_ex = re.compile("^.*Sample: [0-9]+\/([0-9]+)$")
N_ex = None
while True:
    m = _n_ex.match(log.readline())
    if m!=None:
        N_ex = m.group(1)
        break

print "Dataset size: ",int(N_ex)

N_ex = int(N_ex)

log.close()
log = open(path,'r')

parses = list()
morph_seq = re.compile("^\# (.+) \#$")


def print_parse(m_seq,s_seq):
    res = ""
    for i in range(len(m_seq)):
        res += m_seq[i]
        res += "["+str(s_seq[i])+"]"
    print res


print "Parsing log..."

it = 0
while (loadN==None or it<loadN):

    line = log.readline()
    if line=="":
        break

    if line=="Old parse:\n":
        log.readline()
        log.readline()
        line = log.readline()

    match = morph_seq.match(line)

    if match!=None:
        m_seq = match.group(1).split()
        try:
            s_seq = map(int, log.readline().split())[1:-1]
        except:
            continue
            
        if len(parses)<N_ex:
            parses.append((m_seq,s_seq))
        else:
            parses[it%N_ex] = (m_seq,s_seq)
        it += 1


print "Computing statistics on "+str(len(parses))+" parses"
            
seq_counts = dict()
morph_counts = dict()
for (m_seq, s_seq) in parses:
    s_seq = tuple(s_seq)
    if s_seq in seq_counts:
        seq_counts[s_seq] += 1
    else:
        seq_counts[s_seq] = 1

    for i in range(len(s_seq)):
        s,m = s_seq[i],m_seq[i]
        if s in morph_counts:
            if m in morph_counts[s]:
                morph_counts[s][m] += 1
            else:
                morph_counts[s][m] = 1
        else:
            morph_counts[s] = dict()
            morph_counts[s][m] = 1

Zseq = sum(seq_counts.values())
print "Most frequent class sequences:"
freq_seqs = sorted(seq_counts.keys(),
                   key = lambda x: seq_counts[x],
                   reverse=True)[:200]
S = 0
for seq in freq_seqs:
    S += seq_counts[seq]
    print seq,"  ","("+str(seq_counts[seq])+")","  ("+str(100.*float(S)/float(Zseq))+"% of mass)"




print " "

print "Top morphemes per class (80% of the class's mass goes to these):"
for c in morph_counts:
    freq_morphs = sorted(morph_counts[c].keys(),
                         key = lambda x: morph_counts[c][x],
                         reverse = True)

    Z = float(sum(morph_counts[c].values()))
    S = 0
    for i in range(len(freq_morphs)):
        S += morph_counts[c][freq_morphs[i]]
        if S>(0.8*Z):
            break

    print "Class "+str(c)+" : 80% of the mass is allocated to "+str(i+1)+" morphs"
    print reduce(lambda a,b: a+" "+b,
                 freq_morphs[:min(5000,i)])


print "   "






def sample(iterator, P, normalized=True, annealing=1.):
    """
    Samples an element from an iterator according to a probability distribution P
    
    Parameters
    ----------
    iterator : The iterator to sample from
    P : The probability function (takes an element from the iterator, and returns a real-valued number)
    normalized : A boolean indicating whether P is normalized on the iterator
    annealing : (If different from 1., overrides the 'normalized' argument).
                annealing==1. -> sample from the true distribution
                annealing==0. -> sample from the uniform distribution
                0. <= annealing <= 1.
    """
    if not normalized or annealing!=1.:
        Z = 0.
        for el in iterator:
            Z += np.power(P(el), annealing)
    else:
        Z = 1.
    x = np.random.random()
    y = 0.
    for el in iterator:
        y += np.power(P(el),annealing)/Z
        if y>=x:
            return el
    raise Exception("Options exhausted during sampling ! Check whether the distribution was normalized.")








for c in morph_counts:
    print "Samples from class "+str(c)+" :"
    samples = list()
    Z = float(sum(morph_counts[c].values()))
    while len(samples)<min(len(morph_counts[c]),500):
        m_ = sample(iterator = morph_counts[c],
                    P = lambda m: (0. if m in samples else morph_counts[c][m]/Z),
                    normalized = True)
        Z -= morph_counts[c][m_]
        samples.append(m_)

    print reduce(lambda a,b: a+" "+b, samples)
        
    




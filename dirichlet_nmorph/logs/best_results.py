import re
import argparse


fmeasure = re.compile("^(.*)\.log:Precision: .* - Recall: .* - F-measure: (.*)$")
parser = argparse.ArgumentParser(description = "")
parser.add_argument('path', help="Path to result file", type=str)
path = parser.parse_args().path


f = open(path,'r')
lines = f.read().splitlines()
f.close()

best = dict() # Best score for each experiment
line = dict() # Line at which best score appeared for each experiment

for i in range(len(lines)):
    m = fmeasure.match(lines[i])
    (name,score) = (m.group(1), float(m.group(2)))
    if ( name in best and best[name]<score ) or ( not name in best ):
        best[name] = score
        line[name] = i

podium = sorted(line.keys(),
                key = lambda name: best[name],
                reverse = True)

for name in podium:
    print lines[line[name]]

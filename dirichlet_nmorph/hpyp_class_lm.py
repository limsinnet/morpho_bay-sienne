"""
A module implementing a class-based hierarchical Pitman-Yor language model
"""

from pitmanYor import *

class HPYP_class_LM:
    """
    A class describing a class-based hierarchical Dirichlet language model
    Class->morph generation is itself a hierarchical PYP, where the class-conditional distribution backs off to a class-independent distribution shared among classes, that itself backs off to a base distribution over character strings
    """
    def __init__(self,N,P0,
                 CLASS_THETA = dirac(5.),
                 SHARED_THETA = dirac(5.),
                 MORPH_THETA = dirac(5.)):
        """
        Parameters
        ----------
        N : the order of the class sequence model at the highest level of the hierarchy
        CLASS_THETA, CLASS_DISCOUNT : the parameters for the Pitman-Yor class sequence model
        LEX_THETA, LEX_DISCOUNT : each class independently generates morphs according to a Pitman-Yor process whose parameters are LEX_THETA, LEX_DISCOUNT
        P0 : the base distribution over character strings
        """
        self.N = N
        self.P0 = lambda form, _: P0(form)

        SCALE = 50.

        """ Class sequence HPYP """
        self.class_bottom_dist = Index_Base_Distribution()
        self.class_hpyp = PitmanYorProcess(ASSIGN_HP = lambda conditioning: len(conditioning),  # conditional distributions with same context length share the same hyperparameters
                                           THETA_PRIOR = CLASS_THETA,
                                           BASE = None,
                                           BASE_CONDITIONING = lambda cond : (None if len(cond)==0 else cond[1:]))
        self.class_hpyp.BASE = ( lambda cond : (self.class_bottom_dist if len(cond)==0 else self.class_hpyp) )

        """ Morph generation HPYP """
        self.morph_bottom_dist = PYP_wrapper(self.P0)
        self.shared_morph_hpyp = PitmanYorProcess(BASE = lambda _: self.morph_bottom_dist,
                                                  THETA_PRIOR = SHARED_THETA,
                                                  BASE_CONDITIONING = lambda _: None)
        self.morph_hpyp = PitmanYorProcess(ASSIGN_HP = lambda conditioning: conditioning,
                                           THETA_PRIOR = MORPH_THETA,
                                           BASE = lambda _: self.shared_morph_hpyp,
                                           BASE_CONDITIONING = lambda _: None)

    def class_iterator(self):
        return self.class_bottom_dist.index_iterator(reserved = set([0,1]))

    def dump(self):
        print "Class sequence model:"
        self.class_hpyp.dump()
        print "Class-conditional morph model:"
        self.morph_hpyp.dump()
        print "Class-independent morph model:"
        self.shared_morph_hpyp.dump()

    def pickle(self,pickler):
        self.class_hpyp.pickle(pickler)
        self.class_bottom_dist.pickle(pickler)
        self.shared_morph_hpyp.pickle(pickler)
        self.morph_hpyp.pickle(pickler)

    def unpickle(self,pickler):
        self.class_hpyp.unpickle(pickler)
        self.class_bottom_dist.unpickle(pickler)
        self.shared_morph_hpyp.unpickle(pickler)
        self.morph_hpyp.unpickle(pickler)
        

    def cache(self,parse):
        """
        Adds a parse to the cache.
        NB: Parses are represented as a couple (state_sequence, morph_sequence) where 'state_sequence' is a sequence of integers and morph_sequence is a sequence of strings
        """
        state_seq, morph_seq = parse
        
        for i in range(0,len(state_seq)):
            self.class_hpyp.cache(state_seq[i], tuple(state_seq[i-self.N+1:i]))
            self.morph_hpyp.cache(morph_seq[i], state_seq[i])

    def uncache(self,parse):
        """
        Removes a parse from the cache.
        """
        state_seq, morph_seq = parse
        
        for i in range(0,len(state_seq)):
            self.class_hpyp.uncache(state_seq[i], conditioning = tuple(state_seq[i-self.N+1:i]))
            self.morph_hpyp.uncache(morph_seq[i], conditioning = state_seq[i])


    def sample(self):
        """
        Samples a parse according to the current model
        """
        pre = (0,)
        morph_seq = ['#']
        class_seq = [0]
        
        c = 0
        while c!=1:
            while True:
                c = self.class_hpyp.sample(conditioning = pre)
                if c!=0:
                    break
            m = self.morph_hpyp.sample(conditioning = c)
            class_seq.append(c)
            morph_seq.append(m)
            pre = tuple(class_seq[-self.N+1:])

        return class_seq, morph_seq


    def Pstate(self,state,predecessors=(0,)):
        """
        Computes the probability of 'state' following predecessors according to the current model.
        """
        return self.class_hpyp.P(state, conditioning = predecessors)

    def Psm(self,state,morph,predecessors=(0,)):
        """
        Computes the probability of generating (class,morph) in the parse conditional to the preceding class sequence.
        
        Parameters
        ----------
        state : generated state
        morph : generated morph
        predecessors : preceding class sequence (should not be of length greater than N-1)
        """
        return self.morph_hpyp.P(morph, conditioning = state) * self.class_hpyp.P(state, conditioning = predecessors)


    def sample_hyperparameters(self):
        """
        Resamples all hyper-parameters of the model according to the priors specified at initialization
        """
        print "Resampling hyperparameters for class-sequence HPYP (one hyperparameter set by context length)..."
        self.class_hpyp.sample_all_hyperparameters()
        print "Resampling shared morph pool hyperparameters..."
        self.shared_morph_hpyp.sample_all_hyperparameters()
        print "Resampling hyperparameters for class-conditional morph-generation HPYP (one hyperparameter set by class)..."
        self.morph_hpyp.sample_all_hyperparameters()



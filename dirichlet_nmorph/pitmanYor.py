"""
Some tools for implementing hierarchical Pitman-Yor processes
"""

import numpy as np
from tools import sample, slice_sample, slice_sample_step, beta, uniform, gamma, exponential, dirac
import itertools








loggamma = lambda z: ( 0.5*(np.log(2*np.pi) - np.log(z)) + z*(np.log(    z+(1./((12.*z)-(1./(10.*z))) )  ) - 1.) )  # (approximation de Gergo Nemes)




class PYP_wrapper:
    """
    A class to wrap a standard probability distribution function into a PitmanYorProcess interface, to use as the base distribution of a PitmanYorProcess object
    """
    def __init__(self,pdf,
                 sample = lambda conditioning : '?'):
        """
        pdf : The probability distribution function to wrap. Should take two arguments: pdf(item,conditioning)
        sample : A procedure to sample from that pdf (takes a single argument : a possible conditioner)
        """
        self.pdf = pdf
        self.sample = sample

    def P(self,item,conditioning = None):
        return self.pdf(item, conditioning)

    def cache(self,item,conditioning = None):
        pass

    def uncache(self,item,conditioning = None):
        pass

    def pickle(self,pickler):
        return

    def unpickle(self,pickler):
        return


class Index_Base_Distribution:
    """
    A base distribution that generates indices, exposing a 'PitmanYorProcess' interface
    """
    def __init__(self):
        self.used_slots = dict()
        self.next_ID = 0

    def P(self, item, conditioning=None):
        return 1./(len(self.used_slots)+1.)

    def cache(self,item,conditioning=None):
        if item in self.used_slots:
            self.used_slots[item] += 1.
        else:
            self.used_slots[item] = 1.
            if item==self.next_ID:
                x = 0
                while x in self.used_slots:
                    x += 1
                self.next_ID = x

    def uncache(self,item,conditioning=None):
        self.used_slots[item] -= 1.
        if self.used_slots[item]==0.:
            self.used_slots.pop(item)
            self.next_ID = item

    def sample(self,conditioning=None):
        return sample(iterator = self.used_slots.keys()+[self.next_ID],
                      P = lambda _: 1./(1.+len(self.used_slots)))

    def index_iterator(self, reserved=set()):
        """ Iterates over current indices """
        for i in self.used_slots:
            if i not in reserved:
                yield i
        yield self.next_ID

    def pickle(self,pickler):
        pickler.dump(self.used_slots)
        pickler.dump(self.next_ID)
        return

    def unpickle(self,pickler):
        self.used_slots = pickler.load()
        self.next_ID = pickler.load()


class PitmanYorProcess:
    def __init__(self,
                 BASE,
                 BASE_CONDITIONING = lambda _ : None,
                 THETA_PRIOR = exponential(scale=1000.),
                 DISCOUNT_PRIOR = beta(alpha=1.,beta=5.),
                 ASSIGN_HP = lambda conditioning : None
                 ):
        """
        THETA_PRIOR : the prior distribution over the concentration parameter.
        DISCOUNT_PRIOR : the prior distribution over the discount parameter
         - Note: Prior distribution are specified by an object with two methods. A method sample(void)->float that returns sample from the distribution, and a method unnormalized_logP(float)->float that is the unnormalized log-density function of the distribution
        ASSIGN_HP : Maps a conditioner to an identifier object. Whenever conditioners are mapped to the same identifier object, their conditional distributions will share the same hyperparameters.

        BASE : the base distribution. Must be a function taking an object (as conditioning) and returning a PitmanYorProcess object. To use a standard probability distribution as the base distribution, you need to wrap it into a 'PitmanYorProcess' interface using the 'PYP_wrapper' class
               set as string "self" to point towards oneself
        BASE_CONDITIONING : (a function). This function determines how to condition the base distribution when drawing a sample from it. For example, say we draw a sample from the top distribution, conditioned on object 'X'. Then either the top distribution will perform a pool draw, either it will perform a base draw. In the latter case, the draw will be obtained from the base distribution, by conditioning it on the object BASE_CONDITIONING(X)
       """
        self.THETA_PRIOR, self.DISCOUNT_PRIOR, self.BASE, self.BASE_CONDITIONING = THETA_PRIOR, DISCOUNT_PRIOR, BASE, BASE_CONDITIONING
        self.THETAS, self.DISCOUNTS = dict(), dict()
        self.ASSIGN_HP = ASSIGN_HP

        def _THETA(cond):
            id = self.ASSIGN_HP(cond)
            if id in self.THETAS:
                return self.THETAS[id]
            else:
                self.THETAS[id] = self.THETA_PRIOR.sample()
                return self.THETAS[id]

        def _DISCOUNT(cond):
            id = self.ASSIGN_HP(cond)
            if id in self.DISCOUNTS:
                return self.DISCOUNTS[id]
            else:
                self.DISCOUNTS[id] = self.DISCOUNT_PRIOR.sample()
                return self.DISCOUNTS[id]
            
        self.THETA = _THETA
        self.DISCOUNT = _DISCOUNT

        if self.BASE=="self":
            self.BASE = self

        self.customers_per_table = dict()
        self.total_customers = dict()
        self.table_labels = dict()
        self.label_tables = dict()

    def pickle(self,pickler):
        pickler.dump(self.THETAS)
        pickler.dump(self.DISCOUNTS)
        pickler.dump(self.customers_per_table)
        pickler.dump(self.total_customers)
        pickler.dump(self.table_labels)
        pickler.dump(self.label_tables)

    def unpickle(self,pickler):
        self.THETAS = pickler.load()
        self.DISCOUNTS = pickler.load()
        self.customers_per_table = pickler.load()
        self.total_customers = pickler.load()
        self.table_labels = pickler.load()
        self.label_tables = pickler.load()

    def dump(self):
        print "THETAS = ",self.THETAS
        print "DISCOUNTS = ",self.DISCOUNTS
        for cond in self.customers_per_table:
            try:
                print "Samples conditioned on "+str(cond)+" :"
                s = ""
                for i in range(1000):
                    s += str(self.sample(conditioning=cond))+" "
                print s
                print ""
            except:
                pass



    def resample_histogram(self):
        """
        Resamples the table assignment.
        (Use when after setting new hyperparameters, the table assignment must be resampled to reflect the new concentration and discount
        """
        # Collect observations
        observations = list()
        for conditioning in self.customers_per_table:
            for table in self.customers_per_table[conditioning]:
                label = self.table_labels[conditioning][table]
                for i in range(int(self.customers_per_table[conditioning][table])):
                    observations.append((conditioning, label))

        # Shuffle observations
        for i in range(len(observations)-1):
            j = np.random.randint(i,len(observations))
            a = observations[i]
            b = observations[j]
            observations[j] = a
            observations[i] = b

        # Reset histogram
        self.customers_per_table = dict()
        self.total_customers = dict()
        self.table_labels = dict()
        self.label_tables = dict()
        
        # Recache everything
        for (conditioning, label) in observations:
            self.cache(label, conditioning=conditioning)
                    
    """
    def logQ_conditioner(self, theta, discount, conditioning=None):
        #loggamma = lambda n: 0.5*np.log(2*3.14)+0.5*np.log(n)+n*(np.log(n)-1.)
        K = len(self.customers_per_table[conditioning])
        N = self.total_customers[conditioning]
        return (
            loggamma(theta/discount + K) - loggamma(theta/discount)
            - loggamma(theta+N) + loggamma(theta+1.)
            + K*np.log(discount)
            + sum(map(lambda table: loggamma(self.customers_per_table[conditioning][table]-discount)-loggamma(1.-discount) + np.log(self.BASE(conditioning).P(self.table_labels[conditioning][table],conditioning=self.BASE_CONDITIONING(conditioning))),
                      self.customers_per_table[conditioning]))
            )
    """


    def logQ_conditioner(self,theta,discount,conditioning=None): # Fast computation of posterior unnormalized log-probability using an approximation (defined in function 'loggamma')
        K = len(self.customers_per_table[conditioning])
        N = self.total_customers[conditioning]
        _logQ = 0.
        for t in self.customers_per_table[conditioning]:
            _logQ += np.log( self.BASE(conditioning).P(self.table_labels[conditioning][t], conditioning=self.BASE_CONDITIONING(conditioning)) )
            _logQ += ( loggamma(self.customers_per_table[conditioning][t]-discount) - loggamma(1.-discount) )

        _logQ += ( loggamma(theta) - loggamma(theta+N) )
        _logQ += ( K*np.log(discount) + loggamma(K + theta/discount) - loggamma(theta/discount) )
        return _logQ


    def logQ_conditioner_exact(self,theta,discount,conditioning=None): # Exact (but slow) computation of posterior unnormalized log-probability
        K = 0
        N = 0
        _logQ = 0.
        for table in self.customers_per_table[conditioning]:
            _logQ += np.log(theta+K*discount) - np.log(theta+N) + np.log(self.BASE(conditioning).P(self.table_labels[conditioning][table], conditioning=self.BASE_CONDITIONING(conditioning)))
            K += 1
            N += 1
            for n_k in range(1,int(self.customers_per_table[conditioning][table])):
                _logQ += np.log(n_k-discount)-np.log(theta+N)
                N += 1
        return _logQ



    def logQ(self, theta_discount, conditioner_id=None):
        """
        Computes the unnormalized posterior log-probability of the hyperparameters associated to all conditioners with ID 'conditioner_id' considering the current caches of these conditioners
        """
        t_prior, d_prior = self.THETA_PRIOR, self.DISCOUNT_PRIOR
        conditioners = set()
        for C in self.customers_per_table:
            if self.ASSIGN_HP(C)==conditioner_id:
                conditioners.add(C)

        #print "conditioners=",conditioners

        T,D = theta_discount
        if T<=0. or D<=0. or D>=1.:
            return (-np.infty)

        return sum(map(lambda _conditioning: ( self.logQ_conditioner(theta=T, discount=D,
                                                                    conditioning=_conditioning) 
                                               + (t_prior.unnormalized_logP(T) + d_prior.unnormalized_logP(D))
                                              ),
                       conditioners))



    def sample_hyperparameters_old(self, conditioner_id=None, chain_length=5, VERBOSE=True):
        """
        Resamples the hyper-parameters of the process using the priors specified at the initialization of the object. The sampling method used is slice sampling initialized to the current value of each parameter
        """
        if VERBOSE:
            print "Sampling theta for conditioner ID: "+str(conditioner_id)
            print "Current theta = "+str(self.THETAS[conditioner_id])

        is_empty = True
        for C in self.customers_per_table:
            if self.ASSIGN_HP(C)==conditioner_id:
                is_empty = False
                break
        if is_empty:
            return

        self.THETAS[conditioner_id] = slice_sample(logQ = lambda th: self.logQ((th, self.DISCOUNTS[conditioner_id]), conditioner_id = conditioner_id),
                                                   w = 1.,
                                                   initial = dirac(self.THETAS[conditioner_id]),
                                                   n = chain_length,
                                                   policy = 'doubling',
                                                   VERBOSE = False)
        if VERBOSE:
            print "Resampled theta = "+str(self.THETAS[conditioner_id])

        
        if VERBOSE:
            print "Sampling discount for conditioner ID: "+str(conditioner_id)
            print "Current discount = "+str(self.DISCOUNTS[conditioner_id])
        self.DISCOUNTS[conditioner_id] = slice_sample(logQ = lambda di: self.logQ((self.THETAS[conditioner_id], di), conditioner_id = conditioner_id),
                                                      w = 0.1,
                                                      initial = dirac(self.DISCOUNTS[conditioner_id]),
                                                      n = chain_length,
                                                      policy = 'stepping_out',
                                                      VERBOSE = False)
        if VERBOSE:
            print "Resampled discount = "+str(self.DISCOUNTS[conditioner_id])
        
                              
        return self.THETAS[conditioner_id], self.DISCOUNTS[conditioner_id]


    def sample_hyperparameters(self, conditioner_id=None, chain_length=5, VERBOSE=True):
        """
        Resamples the hyper-parameters of the process using the priors specified at the initialization of the object. The sampling method used is slice sampling initialized to the current value of each parameter
        """

        is_empty = True
        for C in self.customers_per_table:
            if self.ASSIGN_HP(C)==conditioner_id:
                is_empty = False
                break
        if is_empty:
            return

        if VERBOSE:
            print "Resampling hyperparameters for conditioner hash: "+str(conditioner_id)
            print "theta, discount = "+str((self.THETAS[conditioner_id], self.DISCOUNTS[conditioner_id]))


        for it in range(chain_length):
            self.THETAS[conditioner_id] = slice_sample_step(logQ = lambda th: self.logQ((th, self.DISCOUNTS[conditioner_id]), conditioner_id = conditioner_id),
                                                            x_= self.THETAS[conditioner_id],
                                                            w = 1.,
                                                            policy = "doubling")
            self.DISCOUNTS[conditioner_id] = slice_sample_step(logQ = lambda di: self.logQ((self.THETAS[conditioner_id], di), conditioner_id = conditioner_id),
                                                               x_ = self.DISCOUNTS[conditioner_id],
                                                               w = 0.1,
                                                               policy = "stepping_out")
            if VERBOSE:
                print "theta, discount = "+str((self.THETAS[conditioner_id], self.DISCOUNTS[conditioner_id]))

        return self.THETAS[conditioner_id], self.DISCOUNTS[conditioner_id]
    


    def sample_all_hyperparameters(self,VERBOSE=True):
        """
        Resamples all the hyper-parameters (ie across all conditional distributions)
        """
        # First, create a mapping from conditioner IDs to sets of conditioners (ie: find how many hyperparameters there are to resample)
        for C_id in self.THETAS:
            self.sample_hyperparameters(conditioner_id = C_id, VERBOSE=VERBOSE)
        self.resample_histogram()
            

    def tables_counts_countsZ_baseDrawsItem_baseDraws_D_T(self,item,conditioning=None):
        counts = 0
        countsZ = 0
        baseDraws_item = 0
        baseDraws = 0
        tables = set()
        if conditioning in self.label_tables:
            if item in self.label_tables[conditioning]:
                tables = self.label_tables[conditioning][item]
                counts = sum(map(lambda table_id: self.customers_per_table[conditioning][table_id], tables))
                baseDraws_item = len(tables)
                
        if conditioning in self.total_customers:
            countsZ = self.total_customers[conditioning]
            baseDraws = len(self.customers_per_table[conditioning])

        D = self.DISCOUNT(conditioning)
        T = self.THETA(conditioning)
        return tables, counts, countsZ, baseDraws_item, baseDraws, D, T


    def P(self,item,conditioning = None):
        """
        Computes the probability of sampling 'item' conditioned on 'conditioning' under the current model
        """

        """
        tables, counts, countsZ, baseDraws_item, baseDraws, D, T = self.tables_counts_countsZ_baseDrawsItem_baseDraws_D_T(item,conditioning=conditioning)
        return (counts - baseDraws_item*D + (T + D*baseDraws)*self.BASE(conditioning).P(item,conditioning=self.BASE_CONDITIONING(conditioning)))/(countsZ + T)
        """
        p = 0.
        theta = self.THETA(conditioning)
        discount = self.DISCOUNT(conditioning)
        if (conditioning in self.label_tables):
            N = sum(map(lambda k: self.customers_per_table[conditioning][k], self.customers_per_table[conditioning]))
            K = len(self.customers_per_table[conditioning])
            if (item in self.label_tables[conditioning]):
                for k in self.label_tables[conditioning][item]:
                    p += (self.customers_per_table[conditioning][k] - discount)/(theta + N)
            p += ((theta + K*discount)/(theta + N))*self.BASE(conditioning).P(item, conditioning= self.BASE_CONDITIONING(conditioning))
            return p
        else:
            return self.BASE(conditioning).P(item, conditioning=self.BASE_CONDITIONING(conditioning))
                        

        

    def cache(self,item,conditioning = None):
        """
        Caches the observation 'item' into the Pitman-Yor process conditioned on 'conditioning'
        """

        P = dict()
        Z = 0.
        
        D = self.DISCOUNT(conditioning)
        T = self.THETA(conditioning)

        tables = set()
        if conditioning in self.label_tables:
            if item in self.label_tables[conditioning]:
                tables = self.label_tables[conditioning][item]

        for table in tables:
            p = self.customers_per_table[conditioning][table] - D
            P[table] = p
            Z += p

        n_tables = len(self.customers_per_table[conditioning]) if (conditioning in self.customers_per_table) else 0.

        p = (T + D*n_tables)*self.BASE(conditioning).P(item,conditioning=self.BASE_CONDITIONING(conditioning))

        P['new'] = p
        Z += p

        table = sample(P, lambda x: P[x]/Z, normalized=True)
        
        if table=="new":
            if conditioning in self.customers_per_table:
                id_ = max(self.customers_per_table[conditioning])+1 if len(self.customers_per_table[conditioning])>0 else 1
                if item not in self.label_tables[conditioning]:
                    self.label_tables[conditioning][item] = set()
            else:
                self.customers_per_table[conditioning] = dict()
                self.total_customers[conditioning] = 0.
                self.table_labels[conditioning] = dict()
                self.label_tables[conditioning] = dict()
                self.label_tables[conditioning][item] = set()
                id_ = 0
            self.customers_per_table[conditioning][id_] = 1.
            self.total_customers[conditioning] += 1.
            self.table_labels[conditioning][id_] = item
            self.label_tables[conditioning][item].add(id_)

            self.BASE(conditioning).cache(item, conditioning=self.BASE_CONDITIONING(conditioning)) # retropropagate the sampling of 'item' to the base distribution
        else:
            self.customers_per_table[conditioning][table] += 1.
            self.total_customers[conditioning] += 1.
            
            

    def uncache(self,item,conditioning=None):
        """
        Removes observation 'item' conditioned on 'conditioning' from the cache
        """
        P = dict()
        Z = 0.
        
        D = self.DISCOUNT(conditioning)
        T = self.THETA(conditioning)

        tables = set()
        if conditioning in self.label_tables:
            if item in self.label_tables[conditioning]:
                tables = self.label_tables[conditioning][item]

        for table in tables:
            p = self.customers_per_table[conditioning][table] - D
            P[table] = p
            Z += p

        table = sample(P, lambda x: P[x]/Z, normalized=True)

        self.customers_per_table[conditioning][table] -= 1.
        self.total_customers[conditioning] -= 1.
        if self.customers_per_table[conditioning][table] == 0.:
            self.customers_per_table[conditioning].pop(table)
            self.table_labels[conditioning].pop(table)
            self.label_tables[conditioning][item].remove(table)
            self.BASE(conditioning).uncache(item, conditioning = self.BASE_CONDITIONING(conditioning))


            
    def sample(self,conditioning=None):
        """
        Returns a sample from the distribution conditioned on 'conditioning'
        """
        P = dict()
        Z = 0.
        
        D = self.DISCOUNT(conditioning)
        T = self.THETA(conditioning)

        if conditioning in self.customers_per_table:
            for t in self.customers_per_table[conditioning]:
                p = self.customers_per_table[conditioning][t] - D
                P[t] = p
                Z += p

        p = (T + D*len(self.customers_per_table[conditioning])) if conditioning in self.customers_per_table else T
        P['new'] = p
        Z += p

        table = sample(P, lambda x: P[x]/Z, normalized=True)
        if table=='new':
            return self.BASE(conditioning).sample(self.BASE_CONDITIONING(conditioning))
        else:
            return self.table_labels[conditioning][table]


    def P_formula(self,conditioning=None):
        """
        Returns a string containing the mathematical formula for the probability of the current cache depending on the hyperparameters.
        The concentration hyperparameter is x, and the discount hyperparameter is y.
        """
        N = 0
        K = 0
        numerator, denominator = "",""
        for t in self.customers_per_table[conditioning]:
            numerator += "(x+"+str(K)+"*y)*"
            denominator += "("+str(N)+"+x)*"
            N += 1
            K += 1
            for c in range(1,int(self.customers_per_table[conditioning][t])):
                numerator += "("+str(c)+"-y)*"
                denominator += "("+str(N)+"+x)*"
                N += 1
        return "("+numerator[:-1]+")/("+denominator[:-1]+")"


        """
        base_draw = False
        if conditioning in self.counts:
            pool_Z = self.countsZ[conditioning] - D*self.baseDraws[conditioning]
            base_Z = D*self.baseDraws[conditioning]
            base_draw = (np.random.random() < (base_Z/(pool_Z + base_Z)))
        else:
            base_draw = True

        if base_draw:
            return self.BASE(conditioning).sample(conditioning=self.BASE_CONDITIONING(conditioning))
        else:
            return sample(iterator = self.counts[conditioning],
                          P = lambda item: (self.counts[conditioning][item]-D*self.baseDraws_item[conditioning][item])/(self.countsZ[conditioning] - D*self.baseDraws[conditioning])
                          )
                          
        """

        


def test(THETA = 50., DISCOUNT = 0.1,
          N = 1000,
          VERBOSE = False):
    """
    Runs a PYP-CRP (table labels are uniform random integers from 0 to 999)
    """
    customers_per_table = dict()
    table_labels = dict()
    base = PYP_wrapper(pdf = (lambda x,_: 1./1000),
                       sample = (lambda _ : np.random.randint(0,1000)))
    pyp = PitmanYorProcess(BASE = lambda _: base,
                           BASE_CONDITIONING = lambda _: None)
    pyp.DISCOUNTS[None] = DISCOUNT
    pyp.THETAS[None] = THETA
    for i in range(N):
        x = pyp.sample()
        pyp.cache(x)
        if VERBOSE:
            print x

    return pyp






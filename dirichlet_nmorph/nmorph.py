"""
A nonparametric bayesian model for morphological segmentation.

A state of a model is defined by some segmentation of the lexicon into morphs.
"""
import sys
import codecs
import numpy as np
import eval
from loadCorpus import loadCorpus_TUR_train as loadCorpus
from tools import SparseDict,sample,pr,shuffle
from corpus import Alphabet, CharacterModel
from hpyp_noclass_lm import HPYP_LM
import itertools

if sys.stdout.encoding != 'UTF-8':
    sys.stdout = codecs.getwriter('utf-8')(sys.stdout, 'strict')
if sys.stderr.encoding != 'UTF-8':
    sys.stderr = codecs.getwriter('utf-8')(sys.stderr, 'strict')

####################
# MODEL PARAMETERS #
####################
N = 2
THETA_SCALE = 1.

####################
# MODEL DEFINITION #
####################

print "Loading corpus..."

lexicon = loadCorpus()
#lexicon = map(lambda x: x[0], lexicon)
shuffle(lexicon)
#alphabet = Alphabet([lexicon])
character_model = CharacterModel(lexicon)


            
def P0_string(string):
    """
    A base probability for generating a string of characters.
    The probability is the one computed by a unigram model fitting the type lexicon.
    """
    #res = 1.
    #for c in string:
    #    res *= (char_freqs[c]/char_freqsZ)
    #return res
    return np.exp(character_model.logP(string))
    #return (string_pool[string]/string_pool_Z) if (string in string_pool) else (0.)


class ModelState:
    """
    A class describing a state of the model, ie: for each word of the lexicon, some parse of the word into a succession of morphological states
    """
    def __init__(self):
        # Initial model:
        # There is one state for all words. The i-th word is parsed as follows: 0(initial state) => 2 => 1(final state).
        #
        # Parses are represented as a couple (state_sequence, morph_sequence)
        # For example consider the following parse:
        #     # + # + ab + ge + trunk + en + #
        #     0 ->0 ->2 -> 2  ->  4 - > 3 -> 1
        # It will be represented by the following couple:
        # ( state_sequence = [0,0,2,2,4,3,1]
        #   morph_sequence = ['#','#','ab','ge','trunk','en','#'] )



        self.LM = HPYP_LM(N,P0_string,THETA_SCALE=THETA_SCALE, split_ratio=0.41)

        """
        self.parses = map(lambda i: ['#', lexicon[i][1:-1], '#'],
                          range(len(lexicon)))
        """

        self.parses = list()
        
        for parse in self.parses:
            self.LM.cache(parse)

        """
        for word in lexicon:
            for i in range(1,len(word)-1):
                for j in range(i+1, len(word)):
                    self.LM.morph_hpyp.cache(word[i:j], conditioning=tuple())
        """




    @staticmethod
    def repr_parse(parse):
        return reduce(lambda x,y: x+' '+y, parse)


    def Print(self,Nlines=1000):
        for i in range(min(len(self.parses),Nlines)):
            print (ModelState.repr_parse(self.parses[i])+'\n').encode('utf-8')

          

def gibbsSample(state,
                dump_every=20000,
                eval_every=10000,
                eval_procedure=(lambda : 0),
                resample_hyperparameters_every=5000):
    """
    Performs Gibbs sampling on the lexicon parses to learn the model
    """
    it = 0

    if resample_hyperparameters_every=='epoch':
        resample_hyperparameters_every = len(lexicon)

    while True:

        if (1+it)%resample_hyperparameters_every==0:
            print "Resampling hyper-parameters..."
            state.LM.sample_hyperparameters()

        if (1+it)%dump_every==0:
            state.LM.dump()

        if (1+it)%eval_every==0:
            eval_procedure()

        print "Epoch: "+str(1+(it//len(lexicon)))+" - Sample: "+str(1+(it%len(lexicon)))+"/"+str(len(lexicon))
        wordID = it%len(lexicon)
        word = lexicon[wordID]
        if wordID<len(state.parses):
            # A parse for this word already exists: un-sample it
            state.LM.uncache(state.parses[wordID])
            
        new_parse = state.LM.sample_parse(word)
        state.LM.cache(new_parse)
                             
        if wordID<len(state.parses):
            # Override the old parse
            old_parse = state.parses[wordID]
            state.parses[wordID] = new_parse
            if new_parse!=old_parse:
                print "Old parse:"
                print ModelState.repr_parse(old_parse)
                print "New parse:"
                print ModelState.repr_parse(new_parse)
                print ""
            else:
                print word+" - unchanged"
        else:
            # Add new parse
            state.parses.append(new_parse)
            print ModelState.repr_parse(new_parse)
            print ""
        it += 1


def segment_form(word):
    return s.LM.sample_parse(word,maximize=True)

s = ModelState()


class eval_on:
    def __init__(self,test_corpus):
        self.test_corpus = test_corpus
    
    def __call__(self):
        p,r,f = eval.against_gold_segmentations(segment_form, self.test_corpus)
        print "Precision: "+str(p)+" - Recall: "+str(r)+" - F-measure: "+str(f)
        print "(Random baseline: "+str(eval.against_gold_segmentations(eval.random_baseline(test_corpus),test_corpus))+" )"



print "Class-less HPYP morph model on Turkish 2005 MorphoChallenge"
print "N="+str(N)
print "Theta prior:",s.LM.morph_hpyp.THETA_PRIOR," / Discount prior:",s.LM.morph_hpyp.DISCOUNT_PRIOR

import loadCorpus
test_corpus = loadCorpus.loadCorpus_TUR_test()
gibbsSample(s,
#            eval_procedure = lambda : 0)
            eval_procedure = eval_on(test_corpus))

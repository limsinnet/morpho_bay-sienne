import numpy as np

"""
A module to apply the entropy peak heuristic to split words into morphemes
"""


def entropy_split(lexicon,
                  threshold = 0.8,
                  N=3):
    """
    Parameters
    ----------
    lexicon : A lexicon of word forms
    N : The order of the character language model that will be used to compute character entropy

    Returns : A list of split forms
    """

    print "--- Entropy splitting --------------------"


    # Acquire a character language model
    LM = dict()
    revLM = dict()
    for word in lexicon:
        for i in range(1,len(word)):
            pre = word[i-N+1:i]
            suf = word[i:i+N-1]
            c_ = word[i-1]
            c = word[i]

            if pre in LM:
                if c in LM[pre]:
                    LM[pre][c] += 1.
                else:
                    LM[pre][c] = 1.
            else:
                LM[pre] = dict()
                LM[pre][c] = 1.

            if suf in revLM:
                if c_ in revLM[suf]:
                    revLM[suf][c_] += 1.
                else:
                    revLM[suf][c_] = 1.
            else:
                revLM[suf] = dict()
                revLM[suf][c_] = 1.
    
    # Compute the entropy of all distributions conditioned on a predecessor
    H = dict()
    for pre in LM:
        p = np.array(LM[pre].values())
        p /= p.sum()
        H[pre] = -(p*np.log(p)).sum()
    revH = dict()
    for suf in revLM:
        p = np.array(revLM[suf].values())
        p /= p.sum()
        revH[suf] = -(p*np.log(p)).sum()

    # Compute threshold
    entropies = list()
    h_sum = 0.
    h_max = 0.
    for word in lexicon:
        for i in range(2,len(word)-1):
            h = (H[word[i-N+1:i]] + revH[word[i:i+N-1]])
            entropies.append(h)
            h_sum += h
            h_max = max(h_max,h)
    entropies = sorted(entropies, reverse=True)
    
    h = 0.
    I = 0
    for i in range(len(entropies)):
        h += entropies[i]
        if h>(h_sum*(1.-threshold)):
            I = i
            break
    Hmin = entropies[I]

    print "Threshold:",Hmin
    print "Maximal entropy:",h_max
    print "------------------------------------------"
    
    # Split words
    split = list()
    for word in lexicon:
        split_form = ['#']
        i = 1
        for j in range(2,len(word)-1):
            if ( H[word[j-N+1:j]] + revH[word[j:j+N-1]] )>Hmin:
                split_form.append(word[i:j])
                i = j
        split_form.append(word[i:len(word)-1])
        split_form.append('#')
        split.append(split_form)

    return split
        

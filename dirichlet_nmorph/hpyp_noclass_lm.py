"""
A module implementing a class-based hierarchical Pitman-Yor language model
"""

from pitmanYor import *

class HPYP_LM:
    """
    A class describing a class-less hierarchical Dirichlet language model
    """
    def __init__(self,N,P0,
                 THETA_SCALE = 1.,
                 split_ratio = 0.41
                 ):
        """
        Parameters
        ----------
        N : the order of the class sequence model at the highest level of the hierarchy
        P0 : the base distribution over character strings
        """
        self.N = N
        self.P0 = lambda form, _: P0(form)

        self.morph_bottom_dist = PYP_wrapper(self.P0)
        self.morph_hpyp = PitmanYorProcess(ASSIGN_HP = lambda conditioning: len(conditioning),
                                           THETA_PRIOR = exponential(scale=THETA_SCALE),
                                           BASE_CONDITIONING = lambda cond : (None if len(cond)==0 else cond[1:]),
                                           BASE = None)
        self.morph_hpyp.BASE = ( lambda cond: (self.morph_bottom_dist if len(cond)==0 else self.morph_hpyp) )
        self.split_ratio = split_ratio


    def dump(self):
        morph_lexicon = self.morph_hpyp.counts[tuple()]
        print str(len(morph_lexicon))+" morphs collected:"
        print morph_lexicon.keys()

    def cache(self,morph_seq):
        """
        Adds a parse to the cache.
        NB: Parses are represented as a list morph_sequence where morph_sequence is a sequence of strings
        """
        for i in range(0,len(morph_seq)):
            self.morph_hpyp.cache(morph_seq[i], tuple(morph_seq[i-self.N+1:i]))

    def uncache(self,morph_seq):
        """
        Removes a parse from the cache.
        """        
        for i in range(0,len(morph_seq)):
            self.morph_hpyp.uncache(morph_seq[i], tuple(morph_seq[i-self.N+1:i]))

    def Pmorph(self,morph,predecessors=tuple()):
        """
        Computes the probability of 'morph' following 'predecessors' according to the current model
        """
        return self.morph_hpyp.P(morph, conditioning = predecessors)

    def sample_hyperparameters(self):
        """
        Resamples all hyper-parameters of the model according to the priors specified at initialization
        """
        print "Resampling hyperparameters..."
        self.morph_hpyp.sample_all_hyperparameters()

    @staticmethod
    def _ordered_tuple_iterator(I,J,N):
        """
        Enumerates all (strictly) ordered (increasing) tuples of length N from integer I to integer J
        """
        if (J-I)<N:
            return
        elif N==1:
            yield (I,J)
        else:
            for k in range(I+1,J-N+2):
                for x in HPYP_LM._ordered_tuple_iterator(k,J,N-1):
                    yield (I,)+x
    
    @staticmethod
    def _tuple_to_ngram(word,t):
        """
        Converts a tuple of split points to a list of adjacent substrings of 'word'
        """
        res = tuple()
        for i in range(len(t)-1):
            res += (word[t[i]:t[i+1]],)
        return res
        
    @staticmethod
    def _predecessor_iterator(J,N):
        """
        Enumerates all possible predecessors (split point tuples) of at most 'N' splits leading up to position 'J'
        """
        if J==1:
            yield (0,1)
            return
        for M in range(1,N):
            for x in HPYP_LM._ordered_tuple_iterator(1,J,M):
                yield (0,)+x
        for I in range(1,J-N+1):
            for x in HPYP_LM._ordered_tuple_iterator(I,J,N):
                yield x



    def _beta_probs(self,word):
        beta = dict()
        for pre in HPYP_LM._predecessor_iterator(len(word)-1,self.N-1):
            beta[pre] = self.Pmorph('#',HPYP_LM._tuple_to_ngram(word,pre))
        for K in range(len(word)-2,0,-1):
            for pre in HPYP_LM._predecessor_iterator(K,self.N-1):
                beta[pre] = np.array(0.)
                for J in range(K+1,len(word)):
                    beta[pre] += self.Pmorph(word[K:J], HPYP_LM._tuple_to_ngram(word,pre))*beta[(pre+(J,))[-self.N:]]*self.split_ratio*np.power(1.-self.split_ratio,J-K-1)
        return beta
        
    def sample_parse(self,word,maximize=False):
        """
        Samples a parse for 'word'
        If maximize==True, the highest-probability parse will be returned, else, the parse will be sampled.
        """
        
        beta = self._beta_probs(word)
        i = 1
        pre = (0,1)
        morph_seq = ['#']
        while i!=(len(word)-1):
            H = dict()
            for j in range(i+1,len(word)):
                H[j] = self.Pmorph(word[i:j],pre)*beta[(pre+(j,))[-self.N:]]*self.split_ratio*np.power(1.-self.split_ratio,j-i-1)
            if maximize:
                j = max(H,key=lambda x:H[x])
            else:
                j = sample(iterator = H,
                           P = lambda x: H[x],
                           normalized = False)
            morph_seq.append(word[i:j])
            i = j
            pre = (pre+(j,))[-self.N:]

        morph_seq.append('#')
        return morph_seq
                
        



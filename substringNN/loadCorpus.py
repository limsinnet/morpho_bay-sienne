import json
from corpus import Alphabet
from random import random

def loadCorpus(PATH="/vol/work/loser/tiger/data/tiger_train.json",
               SAMPLE=1.):
    """
    Loads the corpus at PATH, and returns a tuple (lexicon<set>, alphabet<Alphabet>)
    The parameter SAMPLE is used to sample some ratio of the full corpus. 1 takes the whole corpus, 0.5 half of it, etc...
    """
    corpus = json.load(open(PATH,"rt"))
    corpus = filter(lambda _: random()<=SAMPLE, corpus)
    return map(lambda w: '#'+w.lower()+'#', reduce(lambda x,y:x+y, map(lambda s: s[0],corpus)))

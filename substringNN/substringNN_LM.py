import theano as th
import theano.tensor as T
import numpy as np
from loadCorpus import loadCorpus
from substringTools import SubstringStats
from random import randint
import pickle
import tools
from corpus import CharacterModel

# HYPER-PARAMETERS
# ----------
EMBEDDING_DIM = 40
HIDDEN_DIM = 100
NONLINEARITY = lambda x: T.maximum(0.,x) #lambda x: T.tanh(x) + 0.05*x
N_FEATURES = 10000
GRAD_STEP = th.shared(np.array(0.001))
N_CONTEXT = 5


# Initialization
print "Initializing corpus and substring statistics..."
corpus = loadCorpus()
character_model = CharacterModel(corpus) #character_model = CharacterModel(map(lambda x: x[1:-1], corpus))
substringStats = SubstringStats(corpus, N=N_FEATURES, VERBOSE=False, load="pickles/substringStats_train")
lexicon = substringStats.lexicon
print "Initializing neural network..."

n_features = len(substringStats)


feature_embeddings = th.shared(0.01*np.random.randn(n_features, EMBEDDING_DIM))
input_to_hidden = th.shared(0.01*np.random.randn(N_CONTEXT*EMBEDDING_DIM,HIDDEN_DIM))
hidden_to_out = th.shared(0.01*np.random.randn(HIDDEN_DIM,n_features))
out_bias = th.shared(0.01*np.random.randn(n_features))


context_features = map(lambda _: th.shared(np.array([0,1,2], dtype='int16')), range(N_CONTEXT)) # Feature bags of the context words. context_features[m] is a int16 1-dimensional shared variable listing the feature IDs present in the m-th context word
input_embeddings = map(lambda m: feature_embeddings[context_features[m]].sum(axis=0), range(N_CONTEXT)) # A list of the context word embeddings
input_layer = T.concatenate(input_embeddings)
hidden_layer = NONLINEARITY( T.dot(input_layer,input_to_hidden) )
logistic = lambda x: 1./(1.+T.exp(-x))
output_layer = logistic( T.dot(hidden_layer, hidden_to_out)+out_bias )
target = th.shared(0.5+np.zeros((n_features,)))
character_model_logP = th.shared(np.array(0.))
x_ent = -T.sum(target*T.log(output_layer) + (1.-target)*T.log(1.-output_layer)) - character_model_logP

params = feature_embeddings, input_to_hidden, hidden_to_out, out_bias
d_params = T.grad(x_ent, params)

compute_output = th.function(inputs=[], outputs=output_layer)
error = th.function(inputs=[], outputs=x_ent)
backprop = th.function(inputs=[],
                       outputs=x_ent,
                       updates = map(lambda i: (params[i], params[i]-GRAD_STEP*d_params[i]), range(len(params)))
                       )



# Learning
def set_context(context):
    """
    Sets the context: 'context' is a list of strings
    """
    for m in range(N_CONTEXT):
        context_features[m].set_value( np.array( list(substringStats.getWordFeatures(context[m])), dtype='int16' ) )


def set_output_form(form):
    """
    Sets the target vector so as to capture the substring features present in 'form'

    Parameters
    ----------
    form : A string
    """
    bag = substringStats.getWordFeatures(form)
    target_vec = np.zeros((n_features,))
    for i in bag:
        target_vec[i] = 1.
    target.set_value(target_vec)
    character_model_logP.set_value(character_model.logP(form))

def train(AVG_OVER=1):
    it = 0
    accumulated_error = 0.
    while True:
        pos = randint(0,len(lexicon)-N_CONTEXT-1)
        context = corpus[pos:pos+N_CONTEXT]
        word = corpus[pos+N_CONTEXT]
        set_context(context)
        set_output_form(word)
        accumulated_error += backprop()
        it += 1
        if it%AVG_OVER==0:
            print "Iteration "+str(it)+" - Error: "+str(accumulated_error/AVG_OVER)
            accumulated_error = 0.


def logP(form):
    """
    Computes the log-probability of emitting a given form, under the current shared-variable settings.
    """
    set_output_form(form)
    return -error()


def find_most_probable_form(pool=lexicon):
    """
    Finds the most probable word form among those in 'pool', given the current input settings.

    Parameters
    ----------
    pool : A finite iterable over strings
    """
    return max(pool, key = logP)



# Pickling procedures
# -------------------
def save(PATH):
    with open(PATH,'wb') as f:
        p = pickle.Pickler(f)
        for i in params:
            p.dump(i.get_value())
        f.close()

def load(PATH):
    with open(PATH,'rb') as f:
        p = pickle.Unpickler(f)
        for i in params:
            i.set_value(p.load())
        f.close()


# Word reconstitution search procedures
# -------------------------------------
# The procedures TRUNCATE, EXTEND and INSERT_CHAR each return a dictionary of proposals in string space in order to perform Monte Carlo Markov Chain sampling.
# The keys of the dictionary are string proposals, and its values are the associated log-probabilities.

def TRUNCATE(form, max_truncation=5):
    """
    Returns a dictionary of proposals corresponding to all substrings of 'form' where at most 'max_truncation' characters are removed.
    """
    proposals = dict()
    form_ = form[1:-1]
    for i in range(0, min(len(form_)+1, max_truncation)):
        for j in range(max(0,len(form_)-max_truncation+i), len(form_)+1):
            new_form = '#'+form_[i:j]+'#'
            proposals[new_form] = logP(new_form)
    return proposals

def EXTEND(form,n_best=200):
    output = compute_output()
    best = set(sorted(range(output.shape[0]),
                      key = lambda i: output[i],
                      reverse = True)[:n_best])
    proposals = dict()
    for ss_id in best:
        ss = substringStats.ID_to_substring(ss_id)
        for pos in range(len(form)):
            # We try to paste 'ss' at position 'pos' in 'form'.
            # If possible, some prefix of 'ss' will be merged with some suffix of form[:pos]
            # and some suffix of 'ss' will be merged with some prefix of form[pos:]
            
            for i_ in range(len(ss)):
                if ss[:i_]==form[pos-i_:pos]:
                    i = i_
            rr = ss[i_:]
            for j_ in range(len(rr)+1):
                if rr[len(rr)-j_:]==form[pos:pos+j_]:
                    j = j_
            new_form = form[:pos-i]+ss+form[pos+j:]

            if '#' in new_form[1:-1]:
                continue

            new_logP = logP(new_form)
            proposals[new_form] = new_logP
    return proposals
                    
        
def INSERT_CHAR(form, n_samples_for_each_pos=5):
    """
    Tries to insert a character in 'form' according to a character-wise language model.
    At each position in the form, 'n_samples_for_each_pos' new forms are returned where a character is inserted at the considered position.
    """
    form_ = form[1:-1]
    proposals = list()
    for pos in range(0,len(form_)+1):
        possibilities = character_model.insert_char(form_,pos)
        possibilities = sorted(possibilities,
                               key = lambda x: possibilities[x],
                               reverse = True)[:n_samples_for_each_pos]
        possibilities = map(lambda x: '#'+x+'#', possibilities)
        proposals += zip(possibilities, map(logP, possibilities))
    return dict(proposals)


        
def sampleForm(n_steps=100, n_particles=1, VERBOSE=True):
    """
    Performs MCMC to reconstitute a word form.
    Elements of the search space are strings.
    """
    initial = lambda : "##"
    def transition(form):
        proposals = TRUNCATE(form).items()+EXTEND(form).items()+INSERT_CHAR(form).items()
        M = max(map(lambda x: x[1], proposals))
        return tools.sample(proposals,
                            P = lambda x: np.exp(x[1]-M),
                            normalized = False)[0]
    return tools.MCMC(initial, transition, logP,
                      n_steps=n_steps, n_particles=n_particles, VERBOSE=VERBOSE)
                      

def beamSearch(n_steps=100, beam_size=10, VERBOSE=True):
    """
    Performs beam search to reconstitute a word form.
    At each iteration, given a potential solution, neighbors of this potential solution are explored (according to a local search scheme).
    Only the 'beam_size'-best neighbors are kept for the next iteration.
    """
    beam = ["##"]
    for step in range(n_steps):
        print step, beam
        proposals = dict(reduce(lambda x,y: x+y,
                                map(lambda X: TRUNCATE(X).items()+EXTEND(X).items()+INSERT_CHAR(X).items(),
                                    beam))
                         )
        beam = sorted(proposals,
                      key = lambda X: proposals[X],
                      reverse = True)[:beam_size]
    return beam
        











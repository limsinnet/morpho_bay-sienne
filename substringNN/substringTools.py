import numpy as np
from loadCorpus import loadCorpus
from corpus import Alphabet
import pickle

class SubstringStats:
    """
    A class collecting substring statistics for computing N-most articulating substrings
    """
    def __init__(self,corpus,min_length=2,max_length=10,N=10000,
                 load = None,
                 VERBOSE=True):
        """
        Initializes substring statistics.

        Parameters
        ----------
        corpus : A list of words, in the order as they appear in the corpus. Words must be lower-case and padded with one character left and right.
        min_length, max_length : Minimum and maximum length of collected substrings
        N : Take into account only the N-most articulating substrings
        """
        self.corpus = corpus
        self.lexicon = list(set(self.corpus))
        self.alphabet = Alphabet([self.lexicon])
        self.min_length = min_length
        self.max_length = max_length
        self.N = N
        substringSet = set()

        self.TO_SAVE = ["N_best","reverse_N_best"]
        if load!=None:
            self.load(load)
            return


        # Collect and index substrings
        if VERBOSE:
            print "Indexing substrings..."
        for word in self.lexicon:
            for i in range(0,len(word)-min_length+1):
                for j in range(i+min_length,i+max_length+1):
                    if j>len(word):
                        break
                    else:
                        substringSet.add(word[i:j])
        self.substringList = list(substringSet)
        self.reverseSubstringList = dict()
        for i in range(len(self.substringList)):
            self.reverseSubstringList[self.substringList[i]] = i

        # Initialize predecessor and successor matrices.
        # The successor matrix is a matrix such that for every substring with ID 'ss', and for every character with ID 'c' successor[ss,c] is the number of times 'c' appears after 'ss' in the corpus.
        # Same goes for the predecessor matrix
        # NOTE: whitespaces are not counted, else it would penalize too much prefixes and suffixes
        if VERBOSE:
            print "Initializing predecessor and successor matrices..."
        self.predecessor = np.zeros((len(self.substringList), len(self.alphabet)), dtype='float32')
        self.successor = np.zeros((len(self.substringList), len(self.alphabet)), dtype='float32')
        self.unigramDistribution = np.zeros((len(self.alphabet),), dtype='float32')
        for word in self.lexicon:
            for char in word:
                if char=='#':
                    continue
                else:
                    self.unigramDistribution[self.alphabet.char_to_id(char)] += 1.
        self.unigramDistribution /= np.sum(self.unigramDistribution)
        
        # Count predecessors/successors
        for w in range(len(self.corpus)):
            word = self.corpus[w]
            for i in range(0,len(word)-min_length+1):
                for j in range(i+min_length, i+max_length+1):
                    if j>len(word):
                        break
                    else:
                        ss = self.reverseSubstringList[word[i:j]]
                        if i==0:
                            if w==0:
                                self.predecessor[ss] += self.unigramDistribution
                            else:
                                self.predecessor[ss,self.alphabet.char_to_id(self.corpus[w-1][-2])] += 1.
                        else:
                            self.predecessor[ss,self.alphabet.char_to_id(word[i-1])] += 1.
                        if j==len(word):
                            if w==(len(self.corpus)-1):
                                self.successor[ss] += self.unigramDistribution
                            else:
                                self.successor[ss,self.alphabet.char_to_id(self.corpus[w+1][1])] += 1.
                        else:
                            self.successor[ss,self.alphabet.char_to_id(word[j])] += 1.
        self.predecessor /= np.sum(self.predecessor, axis=1, keepdims=True)
        self.successor /= np.sum(self.successor, axis=1, keepdims=True)
                            
        # Compute articulation score (ie: predecessor entropy + successor entropy)
        if VERBOSE:
            print "Computing articulation scores..."

        def entropy(it):
            H = 0.
            for p in it:
                if p==0.:
                    continue
                else:
                    H -= p*np.log(p)
            return H

        self.articulation_score = np.zeros((len(self.substringList),), dtype='float32')
        for ss in range(len(self.substringList)):
            self.articulation_score[ss] = entropy(self.predecessor[ss]) + entropy(self.successor[ss])

        # Sorting by articulation score
        if VERBOSE:
            print "Sorting substrings by articulation score..."
        self.sorted_indices = sorted(range(len(self.substringList)),
                                     key= lambda ss: self.articulation_score[ss],
                                     reverse=True)
                                           
        self.N_best = map(lambda index_: self.substringList[index_], self.sorted_indices)[:self.N]
        self.reverse_N_best = dict()
        for i in range(len(self.N_best)):
            self.reverse_N_best[self.N_best[i]] = i




    def save(self,path):
        with open(path,'wb') as f:
            p = pickle.Pickler(f)
            for var in self.TO_SAVE:
                p.dump(self.__dict__[var])
            f.close()

    def load(self,path):
        with open(path,'rb') as f:
            p = pickle.Unpickler(f)
            for var in self.TO_SAVE:
                self.__dict__[var] = p.load()
            f.close()


    def __len__(self):
        """
        Returns number of collected articulating substrings
        """
        return len(self.N_best)+1

    def substring_to_ID(self,substring):
        if substring in self.reverse_N_best:
            return self.reverse_N_best[substring]
        else:
            return len(self.N_best)

    def ID_to_substring(self,ID):
        if ID<len(self.N_best):
            return self.N_best[ID]
        else:
            return None


    def N_most_articulating(self,N=20000):
        """
        Returns a list of the N most articulating substrings, sorted by decreasing articulation score.
        """
        return map(lambda ss: self.substringList[ss], self.sorted_indices[:N])

    def articulationScore(self,substring):
        """
        Returns the articulation score of a substring
        """
        return self.articulation_score[self.reverseSubstringList[substring]]

    def getWordFeatures(self,word):
        """
        Returns a bag of features (set of integers) corresponding to the N-most articulating substrings that are also present in 'word_'.
        'word' must be padded
        """
        bag = set()
        for i in range(0, len(word)-self.min_length+1):
            for j in range(i+self.min_length, i+self.max_length+1):
                if j>len(word):
                    break
                else:
                    bag.add(self.substring_to_ID(word[i:j]))
        if self.N in bag:
            bag.remove(self.N)
        return bag







        
    
                

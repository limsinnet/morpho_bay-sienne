"""
A module including classes and procedures for loading and treating corpuses.
"""

import random
import numpy as np
import tools

class Alphabet:
    """
    A class for gathering and encoding an alphabet
    """
    def __init__(self,txt,
                 MAX_CHARS=50, NORMALIZATION=lambda c: c.lower()):
        """
        Initializes an alphabet
        
        Parameters
        ----------
        txt : A list of sentences, each sentence being a list of words
        MAX_CHARS : How many different characters are allowed ? Any of the characters less frequent than the MAX_CHAR-most frequent character will be mapped to this character.
        NORMALIZATION : A function for normalizing character strings (by default: lower-casing)
        """
        self.normalize = NORMALIZATION
        chars = dict()
        for sentence in txt:
            for word in sentence:
                for char_ in word:
                    char = self.normalize(char_)
                    if char in chars:
                        chars[char] += 1
                    else:
                        chars[char] = 1
        self.alphabet = ['#']+map(lambda y: y[0], sorted(chars.items(), key = lambda x: -x[1])[:MAX_CHARS])
        self.alphabet_rev = dict()
        for char in chars:
            self.alphabet_rev[char] = MAX_CHARS-1
        for i in range(1,len(self.alphabet)):
            self.alphabet_rev[self.alphabet[i]] = i

    def __len__(self):
        return len(self.alphabet)

    def char_to_id(self,char):
        return self.alphabet_rev[char] if char in self.alphabet_rev else (len(self.alphabet)-1)

    def id_to_char(self,id):
        return self.alphabet[id]
            
    def encode(self,txt):
        """
        Encodes a character string 'txt' into a sequence of integers
        """
        return map(lambda c: self.alphabet_rev[c] if c in self.alphabet_rev else (len(self.alphabet)-1),
                   map(self.normalize,txt))

    def decode(self,txt):
        """
        Decodes a list of integers to a character string
        """
        if len(txt)==0:
            return ""
        else:
            return reduce(lambda x,y: x+y, map(lambda i: self.alphabet[i], txt))


class CharacterModel:
    """
    A class describing a character-wise language model
    """
    def __init__(self,corpus,alphabet=None,N=3):
        """
        Parameters
        ----------
        corpus : A list of words
        alphabet : An 'Alphabet' object determining the encoding used. If 'None', an alphabet is locally created
        N : The order of the model
        """
        if alphabet==None:
            self.alphabet = Alphabet([corpus])
        else:
            self.alphabet = alphabet
        self.corpus = map(lambda x: map(lambda _: 0, range(N-1))+self.alphabet.encode(x)+[0], corpus)
        self.P = dict()
        self.N = N
        
        for word in self.corpus:
            for i in range(len(word)):
                prev = tuple(word[i-N+1:i])
                if prev not in self.P:
                    self.P[prev] = np.array(map(lambda _: 0., range(len(self.alphabet))))
                self.P[prev][word[i]] += 1.

        for prev in self.P:
            self.P[prev] /= self.P[prev].sum()

    def logP(self,string_):
        """
        Computes the log-probability of 'string_' according to the model
        """
        s = map(lambda _: 0, range(self.N-1))+self.alphabet.encode(string_)+[0]
        res = np.array(0.)
        for i in range(self.N-1, len(s)):
            prev = tuple(s[i-self.N+1:i])
            if prev not in self.P:
                return np.log(0.)
            else:
                res += np.log(self.P[prev][s[i]])
        return res

    def insert_char(self,string_,pos):
        """
        Tries to insert a character in 'string_' at position 'pos', and returns a dictionary describing the probability distribution over all possibilities
        """
        s = map(lambda _: 0, range(self.N-1))+self.alphabet.encode(string_)+[0]
        p = pos+self.N-1
        proposals = map(lambda i: s[:p]+[i]+s[p:], range(1,len(self.alphabet)))

        def recompute_P(proposal):
            res = 1.
            for i in range(self.N):
                if p+i==len(proposal):
                    break
                prev = tuple(proposal[p-self.N+1+i:p+i])
                if prev in self.P:
                    res *= self.P[prev][proposal[p+i]]
                else:
                    return np.array(0.)
            return res
        proposals_P = np.array(map(recompute_P, proposals))
        proposals_P /= proposals_P.sum()
        return dict(zip(map(lambda x: self.alphabet.decode(x)[self.N-1:-1],proposals),proposals_P))

    def sample(self):
        """
        Samples a word from the character language model
        """
        res = map(lambda _: 0, range(self.N-1))
        while True:
            prev = tuple(res[-self.N+1:])
            if prev not in self.P:
                return self.sample()
            else:
                res += tools.sample(range(len(self.alphabet)), 
                                    P = lambda x: self.P[prev][x],
                                    normalized = True)
                if res[-1]==0:
                    return reduce(lambda x,y: x+y,
                                  map(lambda x: self.alphabet.ID_to_char(x), res)[self.N-1:-1])




class Lexicon:
    """
    A class for gathering and encoding a lexicon of words
    """
    def __init__(self,txt,
                 MAX_WORDS=20000, NORMALIZATION=lambda w: w.lower()):
        """
        Initializes a lexicon
        
        Parameters
        ----------
        txt : A list of sentences, each sentence being a list of words
        MAX_WORDS : The out-of-vocabulary threshold
        NORMALIZATION : A function for normalizing character string (by default: lower-casing)
        """
        self.normalize = NORMALIZATION
        words = dict()
        for sentence in txt:
            for word_ in sentence:
                word = self.normalize(word_)
                if word in words:
                    words[word] += 1
                else:
                    words[word] = 1
        self.lexicon = ['<s>','<unk>']+map(lambda y: y[0], sorted(words.items(), key = lambda x: -x[1])[:MAX_WORDS])
        self.lexicon_rev = dict()
        for word in words:
            self.lexicon_rev[word] = 1
        for i in range(2,len(self.lexicon)):
            self.lexicon_rev[self.lexicon[i]] = i

    def __len__(self):
        return len(self.lexicon)
            
    def encode(self,txt):
        """
        Encodes a word list 'txt' into a sequence of integers
        """
        return map(lambda c: self.lexicon_rev[c] if c in self.lexicon_rev else 1,
                   map(self.normalize,txt))

    def decode(self,txt):
        """
        Decodes a list of integers to a character string
        """
        return map(lambda i: self.lexicon[i], txt)


        
        
class FrequentSubstrings:
    """
    A class to convert character strings to a sparse feature representation where each feature indicates the presence of some frequent substring in the string, where substring frequency is
    evaluated on some corpus.
    """
    def __init__(self,text,alphabet=None,
                 ADD_BOUNDARY_MARKUPS=True,
                 MIN_LENGTH=2, MAX_LENGTH=5,
                 MAX_FEATURES=5000):
        """
        Parameters
        ----------
        text : A corpus given as a list of lists of words.
        alphabet : The encoding scheme (an 'Alphabet' instance). If 'None', the alphabet is automatically created based on 'text'.
        ADD_BOUNDARY_MARKUPS : Pad the words left and right with 'zero' symbols ?
        MIN_LENGTH : Minimum length of a feature substring
        MAX_LENGTH : Maximum length of a feature substring
        MAX_FEATURES : Maximum number of features
        """
        if alphabet==None:
            self.alphabet = Alphabet(text)
        else:
            self.alphabet = alphabet

        self.MIN_LENGTH = MIN_LENGTH
        self.MAX_LENGTH = MAX_LENGTH
        substrings = dict()
        for sentence in text:
            for word_ in sentence:
                boundary_markup = [0] if ADD_BOUNDARY_MARKUPS else []
                word = boundary_markup+self.alphabet.encode(word_)+boundary_markup
                for l in range(MIN_LENGTH,MAX_LENGTH+1):
                    for i in range(0,len(word)-l+1):
                        ss = tuple(word[i:i+l])
                        if ss in substrings:
                            substrings[ss] += 1
                        else:
                            substrings[ss] = 1
        self.features = map(lambda x: x[0], sorted(substrings.items(), key = lambda y: -y[1])[:MAX_FEATURES])
        self.features_rev = dict()
        for i in range(len(self.features)):
            self.features_rev[self.features[i]] = i

    def __len__(self):
        """
        Returns the length of the feature vectors
        """
        return len(self.features)

    def encode(self,s):
        """
        Converts a string to its bag-of-features representation
        """
        bag = set()
        for l in range(self.MIN_LENGTH,self.MAX_LENGTH+1):
            for i in range(0,len(s)-l+1):
                ss = tuple(s[i:i+l])
                if ss in self.features_rev:
                    bag.add(self.features_rev[ss])
                    print self.alphabet.decode(ss)
        return bag



class BiCorpus:
    """
    A bilingual corpus
    """
    def __init__(self,sourceTxt,targetTxt):
        self.sTxt = sourceTxt
        self.tTxt = targetTxt
        self.preprocess()

    def preprocess(self):
        # Gather character and word encodings
        self.sLexicon = Lexicon(self.sTxt)
        self.sAlphabet = Alphabet(self.sTxt)
        self.tLexicon = Lexicon(self.tTxt)
        self.tAlphabet = Alphabet(self.tTxt)



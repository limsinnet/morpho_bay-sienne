# coding=utf-8

import random
import numpy as np

def randomU01():
    return random.random()

def randomInt(a,b):
    return random.randint(a,b)

def sample(iterator, P, normalized=True):
    """
    Samples an element from an iterator according to a probability distribution P
    
    Parameters
    ----------
    iterator : The iterator to sample from
    P : The probability function (takes an element from the iterator, and returns a real-valued number)
    normalized : A boolean indicating whether P is normalized on the iterator
    """
    if not normalized:
        Z = 0.
        for el in iterator:
            Z += P(el)
    else:
        Z = 1.
    x = np.random.random()
    y = 0.
    for el in iterator:
        y += P(el)/Z
        if y>=x:
            return el
    raise Exception("Options exhausted during sampling ! Check whether the distribution was normalized.")


def MHcheck(acceptance_logratio):
    """
    Performs a Metropolis-Hastings acceptance check given the difference between the log-probabilities of the source and target states.
    Returns True if the target state has passed the test, else False.
    """
    if acceptance_logratio>0:
        return True
    else:
        acceptance_ratio = np.exp(acceptance_logratio)
        return ( random.random()<=acceptance_ratio )
    

def MCMC(initial, transition, logQ, n_steps=100, n_particles=10,
         VERBOSE = True):
    """
    Performs Monte Carlo Markov Chain sampling.

    Parameters
    ----------
    initial : A function that takes no arguments and returns a random element of some domain X (the search space).
    transition : A function that takes an element of X and returns a random element of X.
    logQ : A function that takes an element of X and returns its unnormalized log-probability
    n_steps, n_particles : Self-explanatory
    """
    particles = map(lambda _: initial(), range(n_particles))
    particles_logQ = map(logQ, particles)
    for iteration in range(n_steps):
        if VERBOSE:
            print iteration, particles
        new_particles_before_MH = map(transition, particles)
        logQ_new_particles_before_MH = map(logQ, new_particles_before_MH)
        for i in range(n_particles):
            if MHcheck(logQ_new_particles_before_MH[i]-particles_logQ[i]):
                particles[i] = new_particles_before_MH[i]
                particles_logQ[i] = logQ_new_particles_before_MH[i]
    return particles[max(range(n_particles),
                         key = lambda i: particles_logQ[i])]


def metropolisSampling(proposal, logQ, steps=200):
    """
    Performs Metropolis sampling.
    
    Parameters
    ----------
    proposal : A function that takes no arguments and returns an element of some domain X.
    logQ : An unnormalized log-density function on the domain X.
    steps : Number of steps to run the algorithm for
    """
    state = proposal()
    state_logQ = logQ(state)
    for i in range(steps):
        newState = proposal()
        newState_logQ = logQ(newState)
        if MHcheck(newState_logQ-state_logQ):
            state = newState
            state_logQ = newState_logQ
    return state


def shuffle(L):
    """
    Shuffles a list. (Careful: overrides the list !)
    """
    for i in range(len(L)):
        k = np.random.randint(i,len(L))
        (L[i], L[k]) = (L[k], L[i])


def pr(text,val):
    print text+": "+str(val)
    return val



